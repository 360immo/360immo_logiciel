package test.java;

import main.java.scan.Plan;
import main.java.scan.PlanAnalysis;
import org.junit.Assert;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.CvType;
import org.opencv.core.Scalar;
import org.opencv.core.*;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

/**
 * Created by nicol on 11/10/2016.
 */
public class JAnalyseTest {

    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME); // ne pas toucher !!
    }

    @Test
    public void borderTest() {

        Plan plan = new Plan("src/test/resources/plan_test_1.png"); // Permet de choisir une image
        BufferedImage res = plan.borderDetect(); // Detecte les bordures de celle ci.
        try {
            ImageIO.write(res, "png", new File("src/test/res/plan_test_1_res.png"));
        } catch (IOException e) {
            e.printStackTrace();
            Assert.assertTrue(false);
        }
        Assert.assertTrue(true);
    }

    @Test
    public void detectTest() {

        Plan plan = new Plan("src/test/resources/plan_test_1.png"); // Permet de choisir une image
        BufferedImage res = plan.borderDetect(); // Detecte les bordures de celle ci.

        try {
            ImageIO.write(res, "png", new File("src/test/res/plan_test_1_res.png"));
        } catch (IOException e) {
            e.printStackTrace();
            Assert.assertTrue(false);
        }

        PlanAnalysis a = new PlanAnalysis(plan);

        a.analyse();

        BufferedImage res2 = Plan.toBufferedImage(a.get_plans().get(0).get_imgPlan());

        try {
            ImageIO.write(res2, "png", new File("src/test/res/plan_test_detect_res.png"));
        } catch (IOException e) {
            e.printStackTrace();
            Assert.assertTrue(false);
        }
        Assert.assertTrue(true);
    }

    @Test
    public void openCvTest() {

        System.out.println("Welcome to OpenCV " + Core.VERSION);
        Mat m = new Mat(5, 10, CvType.CV_8UC1, new Scalar(0));
        System.out.println("OpenCV Mat: " + m);
        Mat mr1 = m.row(1);
        mr1.setTo(new Scalar(1));
        Mat mc5 = m.col(5);
        mc5.setTo(new Scalar(5));
        System.out.println("OpenCV Mat data:\n" + m.dump());
    }

    //Useless mais je le garde de coter
    @Test
    public void openCvBorderDetectTest() {
        // reading image
        Mat image = Highgui.imread("src/test/resources/plan_test_1.png");

        Imgproc.cvtColor(image, image, Imgproc.COLOR_BGR2GRAY);
        // thresholding the image to make a binary image
        Imgproc.threshold(image,image, 127,255,0);

        // finding the contours
        ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(image, contours, hierarchy, Imgproc.RETR_TREE,Imgproc.CHAIN_APPROX_SIMPLE);

        // finding best bounding rectangle for a contour whose distance is closer to the image center that other ones
        Imgproc.drawContours(image, contours, -1, new Scalar(255,255,255));
        Highgui.imwrite("src/test/res/result.png", image);
    }
}
