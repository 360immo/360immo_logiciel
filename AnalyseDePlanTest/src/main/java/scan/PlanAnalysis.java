package main.java.scan;

import com.sun.org.apache.bcel.internal.generic.DMUL;
import main.java.scan.data.Room;
import org.opencv.calib3d.Calib3d;
import org.opencv.core.*;
import org.opencv.features2d.*;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 * Created by nicol on 11/10/2016.
 * Where plan analyse will process
 */

public class PlanAnalysis {
    private List<Plan> _plans = new ArrayList();
    private List<Room> _rooms;

    // Constructor

    public PlanAnalysis() {
    }

    public PlanAnalysis(Plan plan) {
        this._rooms = null;
        if (plan.get_imgBorder() != null) {
            this._plans.add(plan);
        }

    }

    public void add_plan(Plan plan) {
        this._rooms = null;
        if (plan.get_imgBorder() != null) {
            this._plans.add(plan);
        }

    }

    private void _cornerDetection(Plan plan) {
        try {
            // Read ressources images.
            Mat img_corner = Highgui.imread("src/main/resources/image/src_border_type1.png", Highgui.CV_LOAD_IMAGE_COLOR );
            if(img_corner.empty())
                throw new Exception("src_border_type1.png missing or cannot be load");

            //-- Step 1: Detect the keypoints using SURF Detector

            // Vector containing main key point of each image
            MatOfKeyPoint keypoints_corner = new MatOfKeyPoint();
            MatOfKeyPoint keypoints_plan = new MatOfKeyPoint();

            //Init SURF detector
            FeatureDetector surf = FeatureDetector.create(FeatureDetector.SURF);
            surf.detect(img_corner, keypoints_corner);
            surf.detect(plan.get_imgBorder(), keypoints_plan);

            System.out.println("-- type" + img_corner.type() + " | " + plan.get_imgBorder().type());

            System.out.println("-- key_corner lenght" + keypoints_corner.size());
            System.out.println("-- key_plan lenght" + keypoints_plan.size());

            //-- Draw keypoints
            Mat img_keypoints_1; Mat img_keypoints_2;

            Features2d.drawKeypoints( img_corner, keypoints_corner, img_corner, new Scalar(100, 200, 100), Features2d.DRAW_OVER_OUTIMG );
            Features2d.drawKeypoints( plan.get_imgBorder(), keypoints_plan, plan.get_imgPlan(), new Scalar(200, 100, 150), Features2d.DRAW_OVER_OUTIMG );

            //-- write img

            Highgui.imwrite("src/test/res/src_border_type1.png", img_corner );

            //-- Step 2: Calculate descriptors (feature vectors)
            DescriptorExtractor extractor = DescriptorExtractor.create(DescriptorExtractor.SURF);

            Mat descriptors_object = new Mat();
            Mat descriptors_scene = new Mat();

            extractor.compute( img_corner, keypoints_plan, descriptors_object );
            extractor.compute( plan.get_imgBorder(), keypoints_plan, descriptors_scene );

            //-- Step 3: Matching descriptor vectors using FLANN matcher
            DescriptorMatcher matcher = DescriptorMatcher.create(DescriptorMatcher.FLANNBASED);

            MatOfDMatch matches = new MatOfDMatch();

            matcher.match( descriptors_object, descriptors_scene, matches );

            //-- Quick calculation of max and min distances between keypoints
            double max_dist = 0;
            double min_dist = 100;

            DMatch[] a_matches = matches.toArray();

            System.out.println("-- matches lenght : " + a_matches.length );

            for( int i = 0; i < descriptors_object.rows(); i++ )
            { double dist = a_matches[i].distance;
                if( dist < min_dist ) min_dist = dist;
                if( dist > max_dist ) max_dist = dist;
            }

            System.out.println("-- Max dist : " + max_dist );
            System.out.println("-- Min dist : " + min_dist );

            //-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist
            Vector<DMatch> gm = new Vector<>();

            for( int i = 0; i < descriptors_object.rows(); i++ )
            { if( a_matches[i].distance < 3*min_dist )
            { gm.add( a_matches[i]); }
            }

            MatOfDMatch good_matches = new MatOfDMatch();
            good_matches.fromList(gm);

            Mat img_matches = new Mat();
            MatOfByte mob = new MatOfByte();
            Features2d.drawMatches( img_corner, keypoints_plan, plan.get_imgBorder(), keypoints_plan,
                    good_matches, img_matches, new Scalar(-1), new Scalar(-1), mob , Features2d.NOT_DRAW_SINGLE_POINTS );

            //-- Localize the object
            Vector<Point> v_obj = new Vector<>();
            Vector<Point> v_scene = new Vector<>();
            MatOfPoint2f obj  = new MatOfPoint2f();
            MatOfPoint2f scene = new MatOfPoint2f();

            for( int i = 0; i < good_matches.rows(); i++ )
            {
                //-- Get the keypoints from the good matches
                v_obj.add(keypoints_corner.toArray()[ good_matches.toArray()[i].queryIdx ].pt);
                v_scene.add(keypoints_plan.toArray()[ good_matches.toArray()[i].trainIdx ].pt);
            }

            obj.fromList(v_obj);
            scene.fromList(v_scene);

            Mat H = Calib3d.findHomography( obj, scene);

            //-- Get the corners from the image_1 ( the object to be "detected" )
            Vector<Point> v_obj_corners = new Vector<>(4);
            v_obj_corners.set(0,new Point(0,0));
            v_obj_corners.set(1, new Point( img_corner.cols(), 0 ));
            v_obj_corners.set(2,new Point( img_corner.cols(), img_corner.rows() ));
            v_obj_corners.set(3, new Point( 0, img_corner.rows() ));

            MatOfPoint2f obj_corners = new MatOfPoint2f();
            obj_corners.fromList(v_obj_corners);
            MatOfPoint2f scene_corners = new MatOfPoint2f();

            Core.perspectiveTransform( obj_corners, scene_corners, H);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Call for each Plan, return a list a Room object
    private List<Room> _getRoomFromPlan(Plan plan) {
        ArrayList rooms = new ArrayList();

        _cornerDetection(plan);

        /*Mat lines = new Mat();

        Imgproc.HoughLinesP(plan.get_imgBorder(), lines,1, Math.PI / 180,50, 10, 10);

        for(int i = 0; i < lines.cols(); i = i + 2) {
            double[] val = lines.get(0, i);
            Core.line(plan.get_imgPlan(), new Point(val[0], val[1]), new Point(val[2], val[3]), new Scalar(0, 0, 255), 2);
        }*/

        return rooms;
    }

    // Launch the analyse
    public List<Room> analyse() {
        this._rooms = new ArrayList();
        Iterator var1 = this._plans.iterator();

        while (var1.hasNext()) {
            Plan p = (Plan) var1.next();
            List res = this._getRoomFromPlan(p);
            if (res != null && !res.isEmpty()) {
                this._rooms.addAll(res);
            }
        }

        return this._rooms;
    }

    public List<Plan> get_plans() {
        return this._plans;
    }

    public List<Room> get_rooms() {
        return this._rooms;
    }
}
