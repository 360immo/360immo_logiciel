package main.java.scan.data;

/**
 * Created by nicol on 11/10/2016.
 */

abstract class Link {
    protected Point _pos;

    Link() {
    }

    abstract Room nextRoom();

    public Point getPos() {
        return this._pos;
    }
}
