package main.java.scan.data;

/**
 * Created by nicol on 11/10/2016.
 */

public class Point {
    public int x;
    public int y;
    public int z;

    public Point(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
