package main.java.scan.data;

/**
 * Created by nicol on 11/10/2016.
 */
public class Door extends Link {
    private Room _nextRoom;

    public Door(Point pos, Room room) {
        this._pos = pos;
        this._nextRoom = room;
    }

    public Room nextRoom() {
        return this._nextRoom;
    }
}

