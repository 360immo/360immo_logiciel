package main.java.scan.data;

/**
 * Created by nicol on 11/10/2016.
 */
public class Stair extends Link {
    private int _nextStage;
    private Room _nextRoom;

    public Stair(Point pos, Room room, int nextStage) {
        this._pos = pos;
        this._nextRoom = room;
        this._nextStage = nextStage;
    }

    public Room nextRoom() {
        return this._nextRoom;
    }
}