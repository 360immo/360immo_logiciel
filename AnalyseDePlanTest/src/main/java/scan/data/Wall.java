package main.java.scan.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicol on 11/10/2016.
 */

public class Wall {
    private List<Point> _l_point;

    public Wall(List<Point> l_point) {
        this._l_point = l_point;
    }

    public Wall(Point p) {
        this._l_point = new ArrayList();
        this._l_point.add(p);
    }

    public Wall() {
        this._l_point = new ArrayList();
    }

    public void addPoint(Point p) {
        this._l_point.add(p);
    }

    public List<Point> get_l_point() {
        return this._l_point;
    }
}