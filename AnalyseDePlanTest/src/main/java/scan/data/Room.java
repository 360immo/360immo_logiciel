package main.java.scan.data;

/**
 * Created by nicol on 11/10/2016.
 */


import java.util.ArrayList;
import java.util.List;

public class Room {
    public String _name;
    private Point _pos;
    public List<Wall> l_wall = new ArrayList();
    public List<Link> l_Link = new ArrayList();

    public Room() {
    }

    public Point get_pos() {
        return this._pos;
    }
}