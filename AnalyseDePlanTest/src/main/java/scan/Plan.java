package main.java.scan;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Paths;


/**
 * Created by nicol on 11/10/2016.
 */

public class Plan {
    private String _imgPath;
    private Mat _imgPlan;
    private Mat _imgBorder;

    public Plan(Mat img, String path) {
        this._imgPath = path;
        this._imgPlan = img;
        this._imgBorder = null;
    }

    public Plan(String path) {
        this._imgPath = path;
        this._imgBorder = null;

        try {
            BufferedImage img = ImageIO.read(Files.newInputStream(Paths.get(path, new String[0]), new OpenOption[0]));
            _imgPlan = toMat(img);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static Plan Plan() {
        Plan plan = null;
        JFileChooser fc = new JFileChooser();
        FileNameExtensionFilter imagesFilter = new FileNameExtensionFilter("Images", new String[]{"bmp", "gif", "jpg", "jpeg", "png"});
        fc.setFileFilter(imagesFilter);
        fc.setDialogTitle("Chose an Image");
        int returnVal = fc.showOpenDialog(null);
        System.out.println(returnVal);
        if (returnVal == 0) {
            File file = fc.getSelectedFile();

            try {
                BufferedImage img = ImageIO.read(file);
                Mat ex = toMat(img);
                plan = new Plan(ex, file.getPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Open command cancelled by user.\n");
        }

        return plan;
    }

    private static Mat toMat(BufferedImage image) {
        byte[] data = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        Mat mat = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC3);
        mat.put(0, 0, data);
        return mat;
    }

    public static BufferedImage toBufferedImage(Mat m) {

        int type = BufferedImage.TYPE_BYTE_GRAY;
        if (m.channels() > 1) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = m.channels() * m.cols() * m.rows();
        byte[] b = new byte[bufferSize];
        m.get(0, 0, b); // get all the pixels
        BufferedImage image = new BufferedImage(m.cols(), m.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(b, 0, targetPixels, 0, b.length);
        return image;
    }

    public BufferedImage borderDetect() {
        Mat src_gray = new Mat();
        Mat detected_edges = new Mat();

        _imgBorder = new Mat();
        _imgBorder.create(_imgPlan.size(), _imgPlan.type());

        // convertie l'image en noir et blanc
        Imgproc.cvtColor(_imgPlan, src_gray, Imgproc.COLOR_RGB2GRAY);

        Imgproc.blur(src_gray, detected_edges, new Size(3, 3));

        //Canny edge detector
        Imgproc.Canny(detected_edges, _imgBorder, 60, 60 * 3);

        return toBufferedImage(_imgBorder);
    }

    public Mat get_imgPlan() {
        return this._imgPlan;
    }

    public Mat get_imgBorder() {
        return this._imgBorder;
    }

    public String get_imgPath() {
        return this._imgPath;
    }
}
