package EIProject;

/**
 * Created by ylies on 4/02/17.
 */

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public class WebPage extends Application {

    @Override
    public void start(Stage stage) {
        stage.setTitle("Webpage and terminal output");
        stage.setWidth(850);
        stage.setHeight(600);
        Scene scene = new Scene(new Group());
        VBox root = new VBox();
        final WebView browser = new WebView();
        final WebEngine webEngine = browser.getEngine();
        webEngine.load("http://google.com");

        TextArea output = new TextArea();
        output.setEditable(false);
        System.setOut(new PrintStream(new OutputStream() {

            @Override
            public void write(int b) throws IOException {
                output.appendText("" + ((char)b));
            }

            @Override
            public void write(byte[] b) throws IOException {
                output.appendText(new String(b));
            }

            @Override
            public void write(byte[] b, int off, int len) throws IOException {
                output.appendText(new String(b, off, len));
            }
        }));

        root.getChildren().addAll(browser, output);
        scene.setRoot(root);
        stage.setScene(scene);
        System.out.print("AVANT\n");
        stage.show();
        System.out.print("APRES\n");
    }

    public static void main(String[] args) {
        launch(args);
    }
}
