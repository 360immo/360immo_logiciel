package EIProject.Controller;

import EIProject.Main;
import EIProject.Model.FXTools.PhotoPreview;
import EIProject.Model.FXTools.ProgressForm;
import EIProject.Model.Graphic.AGraphic;
import EIProject.Model.Graphic.Numerisation.Room.ARoom;
import EIProject.Model.Graphic.Numerisation.Room.RoomPolygon;
import EIProject.Model.Graphic.Numerisation.Room.RoomRectangle;
import EIProject.Model.Graphic.Numerisation.Symbol.ASymbol;
import EIProject.Model.Graphic.Numerisation.Symbol.Door;
import EIProject.Model.Graphic.Numerisation.Symbol.Stairs;
import EIProject.Model.Graphic.VisiteVirtuelle.Link;
import EIProject.Model.Graphic.VisiteVirtuelle.Photo;
import EIProject.Model.Level;
import EIProject.Model.Plan;
import EIProject.Model.Preview.IPreview;
import EIProject.Model.Preview.LinkPreview;
import EIProject.Model.Preview.RoomPolyPreview;
import EIProject.Model.Preview.RoomRectPreview;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.effect.Effect;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.transform.Scale;
import javafx.stage.*;
import sun.util.resources.cldr.ar.CalendarData_ar_LB;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.List;

public class InterfaceMain {

    public enum Tools {
        DRAWMODE, DRAWPOLYMODE, DOORMODE, STAIRSMODE, PHOTOMODE, LINKMODE, DELMODE
    }

    static public InterfaceMain instance;

    private Tools _currentTool;

    private String _savedPlanName = "";

    static private String _savePath = "";

    private Plan _plan = null;

    private Canvas _eventCanvas;

    private ArrayList<AGraphic> _l_select;

    private IPreview _preview;

    private Scene _settingsScene;
    private Stage _settingsStage;

    @FXML
    private TabPane _tab;
    @FXML
    private TextArea _txtAreaHelp;
    @FXML
    private Pane _paneHelper;
    @FXML
    private Button _btnNew;
    @FXML
    private Button _btnOpen;
    @FXML
    private Button _btnSave;
    @FXML
    private Button _btnPreview;
    @FXML
    private Button _btnExport;
    @FXML
    private Button _btnClose;
    @FXML
    private Button _btnSettings;
    @FXML
    private Button _btnAbout;
    @FXML
    private Button _btnLogout;
    @FXML
    private Button _btnSquare;
    @FXML
    private Button _btnPoly;
    @FXML
    private Button _btnDoor;
    @FXML
    private Button _btnStairs;
    @FXML
    private Button _btnPhoto;
    @FXML
    private Button _btnLink;
    @FXML
    private Button _btnLinkAuto;
    @FXML
    private Button _btnEditVisit;
    @FXML
    private Button _btnNext;
    private List<Button> _lButton = null;
    private List<String> _lStringHelp = null;
    Boolean helperLevel = true;

    private Effect effectLighting;
    private Effect effectBasic;

    @FXML
    private Pane _drawingPane;
    @FXML
    private ScrollPane _scroll;

    private Stage _PhotoAddon;

    public Group getRoot() {
        return root;
    }

    private Group root;

    public Scene getS() {
        return s;
    }

    private Scene s;

    private int _scrollValue = 100;

    @FXML
    protected void initialize() {
        if (instance == null) {
            instance = this;
        }

        _l_select = new ArrayList<>();

        effectLighting = _btnOpen.getEffect();
        effectBasic = _btnNew.getEffect();
        _btnOpen.setEffect(effectBasic);

        //TODO Implement Software Properties - Helper On/Off  (CheckBox)
        if (Main.getHelper())
            initHelper();
        else
            _paneHelper.setVisible(false);

    }

    @FXML
    public void loadPlan() {
        FileChooser fcLoad = new FileChooser();
        fcLoad.setTitle("Load a previously saved plan");
        fcLoad.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Edited plan", "*.360PLAN")
        );
        String path;
        if (_savePath.equals(""))
            path = System.getProperty("user.home");
        else
            path = _savePath;
        fcLoad.setInitialDirectory(
                new File(path)
        );

        File file = fcLoad.showOpenDialog(Main.getPrimaryStage());
        doLoading(file);
    }

    private void doLoading(File file) {
        int staticCount = 0;
        int staticSymbol = 0;
        int staticPhoto = 0;
        int staticLevel = 0;
        int levelBegin = 0;

        if (file != null) {
            // load plan
            try {
                JAXBContext context = JAXBContext.newInstance(Plan.class);
                Unmarshaller um = context.createUnmarshaller();
                _plan = (Plan) um.unmarshal(new FileReader(file.getAbsolutePath()));

                Integer count = 0;
                Level tmp_level = _plan.get_m_levels().get(count);
                if (tmp_level != null)
                    levelBegin = tmp_level.id;
                while (tmp_level != null)
                {
                    if (tmp_level.id > staticLevel)
                        staticLevel = tmp_level.id;
                    for (AGraphic tmp : tmp_level.get_l_item())
                    {
                        if (tmp instanceof RoomRectangle)
                            ((RoomRectangle) tmp).loadShape();
                        if (tmp instanceof RoomPolygon)
                            ((RoomPolygon) tmp).loadShape();
                        if (tmp instanceof Door)
                            ((Door) tmp).loadShape(tmp_level.get_l_item());
                        if (tmp instanceof Stairs)
                            ((Stairs) tmp).loadShape(_plan.get_m_levels(), tmp_level);
                        if (tmp instanceof Photo)
                            ((Photo) tmp).loadShape(tmp_level.get_l_item());
                        if (tmp.get_id() > staticCount)
                            staticCount = tmp.get_id();
                        if (tmp instanceof ASymbol && ((ASymbol) tmp).get_sId() > staticSymbol)
                            staticSymbol = ((ASymbol) tmp).get_sId();
                        if (tmp instanceof Photo && ((Photo) tmp).get_pId() > staticPhoto)
                            staticPhoto = ((Photo) tmp).get_pId();
                    }
                    count += 1;
                    tmp_level = _plan.get_m_levels().get(count);
                }

                count = 0;
                tmp_level = _plan.get_m_levels().get(count);
                while (tmp_level != null)
                {
                    if (tmp_level.id > staticLevel)
                        staticLevel = tmp_level.id;
                    for (AGraphic tmp : tmp_level.get_l_item())
                    {
                        if (tmp instanceof Link)
                            ((Link) tmp).loadShape(_plan.get_m_levels(), tmp_level);
                    }
                    count += 1;
                    tmp_level = _plan.get_m_levels().get(count);
                }

                InterfaceLevels.instance.reset();
                InterfaceLevels.instance.addLevels();

                String str_manip = file.getAbsolutePath();
                int cut = str_manip.lastIndexOf(System.getProperty("file.separator"));
                str_manip = str_manip.substring(0, cut);
                _savePath = str_manip;

                _savedPlanName = file.getName();

                AGraphic.set_s_countId(staticCount);
                ASymbol.set_s_symbolId(staticSymbol);
                Photo.set_s_photoId(staticPhoto);
                Level.set_s_id(staticLevel);

                //System.out.println(_plan.get_picFolder());
                if (!_plan.get_picFolder().isEmpty()) {
                    File load_picPath = new File(_plan.get_picFolder());
                    InterfaceAssets.instance.loadPhotoDirectory(load_picPath);
                }
                fillDrawingStage();
                InterfaceLevels.instance.changeLevel(levelBegin);

                // Add to the recently opened files array
                Main.addRecentlyOpened(file.getAbsolutePath());
            } catch (javax.xml.bind.JAXBException | java.io.FileNotFoundException e) {
                InterfaceError._error = 2;
                Main.showInterfaceError();
                System.out.println("Loading error :");
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void newPlan() {
        FileChooser fcNew = new FileChooser();
        fcNew.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.jpg", "*.png"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
        );
        String path;
        if (_savePath.equals(""))
            path = System.getProperty("user.home");
        else
            path = _savePath;
        fcNew.setInitialDirectory(
                new File(path)
        );

        File file = fcNew.showOpenDialog(Main.getPrimaryStage());
        if (file != null) {
            _plan = new Plan("NewPlan", file.getAbsolutePath());
            InterfaceLevels.instance.reset();
            InterfaceLevels.instance.addLevels();
            _savedPlanName = "";
            fillDrawingStage();
            drawingMode();
        }
    }

    @FXML
    public void savePlan() {
        if (_plan != null) {

            ProgressForm pForm = new ProgressForm();

            Task<Void> task = new Task<Void>() {
                @Override
                public Void call() throws InterruptedException {

                    Integer count = 0;
                    Level tmp_level = _plan.get_m_levels().get(count);
                    while (_plan.get_m_levels().get(count) != null) {
                        for (AGraphic tmp : tmp_level.get_l_item()) {
                            if (tmp instanceof RoomRectangle)
                                ((RoomRectangle) tmp).saveShape();
                            if (tmp instanceof RoomPolygon)
                                ((RoomPolygon) tmp).saveShape();
                            if (tmp instanceof Door)
                                ((Door) tmp).saveShape();
                            if (tmp instanceof Stairs)
                                ((Stairs) tmp).saveShape();
                            if (tmp instanceof Photo)
                                ((Photo) tmp).saveShape();
                            if (tmp instanceof Link)
                                ((Link) tmp).saveShape();
                        }
                        count += 1;
                        updateProgress(count / _plan.get_m_levels().size() * 100, 100);  // Update progress
                        tmp_level = _plan.get_m_levels().get(count);
                    }
                    return null;
                }
            };

            // binds progress of progress bars to progress of task:
            pForm.activateProgressBar(task, "Saving");

            // in real life this method would get the result of the task
            // and update the UI based on its value:
            task.setOnSucceeded(event -> {
                try {
                    // create JAXB context and instantiate marshaller
                    JAXBContext context = JAXBContext.newInstance(Plan.class);
                    Marshaller m = context.createMarshaller();
                    m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

                    // Write to System.out
                    m.marshal(_plan, System.out);

                    // Write to File
                    String path;
                    if (_savePath.equals(""))
                        path = System.getProperty("user.home");
                    else
                        path = _savePath;

                    // Check if there is a plan name and if file exists on disk
                    File f = new File(path + System.getProperty("file.separator") + _savedPlanName);

                    if (!_savedPlanName.equals("") && f.exists() && !f.isDirectory()) {
                        m.marshal(_plan, new File(path + System.getProperty("file.separator") + _savedPlanName));
                    } else {
                        FileChooser fcSave = new FileChooser();
                        fcSave.setTitle("Save the current plan");
                        fcSave.getExtensionFilters().addAll(
                                new FileChooser.ExtensionFilter("Edited plan", "*.360PLAN")
                        );
                        fcSave.setInitialDirectory(
                                new File(path)
                        );
                        File file = fcSave.showSaveDialog(Main.getPrimaryStage());
                        if (file != null) {
                            m.marshal(_plan, new File(file.getAbsolutePath()));
                            _savedPlanName = file.getName();
                            String str_manip = file.getAbsolutePath();
                            int cut = str_manip.lastIndexOf(System.getProperty("file.separator"));
                            str_manip = str_manip.substring(0, cut);
                            _savePath = str_manip;

                            // Add to the recently opened files array
                            Main.addRecentlyOpened(file.getAbsolutePath());
                        }
                    }
                } catch (javax.xml.bind.JAXBException e) {
                    InterfaceError._error = 3;
                    Main.showInterfaceError();
                    System.out.println("Saving error :");
                    e.printStackTrace();
                }
                pForm.getDialogStage().close();
            });

            pForm.getDialogStage().show();

            Thread thread = new Thread(task);
            thread.start();

        }
    }

    @FXML
    public void closePlan() {
        if (_plan != null) {
            InterfaceAssets.instance.closePhotoDirectory();
            InterfaceLevels.instance.reset();
            _plan = null;
            _drawingPane.getChildren().clear();
            _savedPlanName = "";
            InterfaceLevels.instance.reset();
            Photo.resetPhotoId();
            ARoom.reset_s_countId();
        }
    }

    @FXML
    public void previewPlan() {
        if (_plan != null && _plan.get_firstPhoto() != null) {
            new PhotoPreview(_plan.get_firstPhoto());
            _PhotoAddon = new Stage();

            Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
            _PhotoAddon.setX((primScreenBounds.getMinX() + primScreenBounds.getWidth()) - 800);
            _PhotoAddon.setY((primScreenBounds.getMinY() + primScreenBounds.getHeight()) - 600);
            _PhotoAddon.setWidth(800);
            _PhotoAddon.setHeight(600);

            _PhotoAddon.setOnCloseRequest(new EventHandler<WindowEvent>() {
                public void handle(WindowEvent we) {
                    InterfaceLevels.instance.changeLevel(InterfaceMain.instance.get_plan().get_currLevel().id);
                }
            });

            root = new Group();
            s = new Scene(root, 1000, 700, Color.BLACK);
            InterfaceMain.instance.fillDrawingStageMatthieuTest();
            InterfaceMain.instance.get_plan().get_currLevel().drawLevelMatthieuTest();

            _PhotoAddon.setScene(s);
            _PhotoAddon.show();

        }
        }

    // Drawing mode
    @FXML
    public void drawingMode() {
        if (_plan != null) {
            if (_preview == null || _preview.getClass() != RoomRectPreview.class) {
                if (_preview != null)
                    _preview.stop();
                _preview = new RoomRectPreview(_eventCanvas);
                _preview.launch();
            }

            _currentTool = Tools.DRAWMODE;
        }
    }

    public void drawPolyMode() {
        if (_plan != null) {
            if (_preview == null || _preview.getClass() != RoomPolyPreview.class) {
                if (_preview != null)
                    _preview.stop();
                _preview = new RoomPolyPreview(_eventCanvas);
                _preview.launch();
            }

            _currentTool = Tools.DRAWPOLYMODE;
        }
    }

    @FXML
    public void doorPlacement() {
        _currentTool = Tools.DOORMODE;
    }

    @FXML
    public void stairsPlacement() {
        _currentTool = Tools.STAIRSMODE;
    }

    @FXML
    public void autoLink() {
        if (_plan != null)
            _plan.get_currLevel().generateLinkAuto();
    }

    @FXML
    public void photoPlacement() {
        _currentTool = Tools.PHOTOMODE;
    }

    @FXML
    public void linkPlacement() {
        if (_plan != null) {
            if (_preview == null || _preview.getClass() != LinkPreview.class) {
                if (_preview != null)
                    _preview.stop();
                _preview = new LinkPreview(_drawingPane);
                _preview.launch();
            }

            _currentTool = Tools.LINKMODE;
        }
    }

    @FXML
    public void editVisit() {
        if (InterfaceLogin.getUser() != null)
            Main.showInterfaceManageVisit();
        else
            Main.showInterfaceLogin();
    }

    @FXML
    public void export() throws FileNotFoundException {
        if (InterfaceLogin.getUser() != null)
            if (_plan != null) {
                Main.showInterfaceExport();
            }
            else {
                InterfaceError._error = 1;
                Main.showInterfaceError();
            }
        else
            Main.showInterfaceLogin();
    }

    @FXML
    public void logout() {
        if (InterfaceLogin.getUser() != null) {
            File lic = new File("License.lic");
            if (lic.delete())
                InterfaceLogin.setUser(null);
        }
        else {
            InterfaceError._error = 4;
            Main.showInterfaceError();
        }
    }

    @FXML
    private void about() { Main.showInterfaceLogin(); }

    @FXML
    private void openSettings() {
        // open new window
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("UIResources", new Locale(Main.getLanguage()));
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("view/InterfaceSettings.fxml"), bundle);
            AnchorPane pane = loader.load();
            InterfaceSettings change = loader.getController();
            change.chooseLanguage();
            _settingsScene = new Scene(pane);
            _settingsStage = new Stage();
            _settingsStage.setTitle("Settings");
            _settingsStage.setScene(_settingsScene);
            _settingsStage.setResizable(false);
            if (!_settingsStage.isShowing())
                _settingsStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //InterfaceSettings.instance.chooseLanguage();
    }

    @FXML
    private void recentFilesList() {
        ContextMenu contextMenu = new ContextMenu();

        // get recent files
        ArrayList<String> recentFiles = Main.getRecentFiles();
        for (String iter : recentFiles) {
            String str_manip = iter;
            int cut = str_manip.lastIndexOf(System.getProperty("file.separator"));
            str_manip = str_manip.substring(cut + 1, str_manip.length() - 8);

            MenuItem next = new MenuItem(str_manip);
            next.setOnAction(new EventHandler<ActionEvent>() {
                public void handle(ActionEvent e) {
                    File load = new File(iter);
                    doLoading(load);
                }
            });
            contextMenu.getItems().add(next);
        }
        if (recentFiles.size() > 0)
            contextMenu.show(_drawingPane, 120, 120);
    }

    //Init drawing zone
    void fillDrawingStage() {
        // fills drawing window
        try {
            Image pic = new Image(new File(_plan.get_currLevel().get_picture()).toURI().toURL().toExternalForm());

            _drawingPane.getChildren().clear();
            _drawingPane.setPrefHeight(pic.getHeight());
            _drawingPane.setPrefWidth(pic.getWidth());

            if (_preview != null) {
                if (_preview instanceof LinkPreview && ((LinkPreview)_preview).getLinkedStair() != null) {

                } else {
                    _preview.stop();
                    _preview = null;
                }
            }

            addBackground(pic);
            addGrid(pic);
            addCanvas(pic);
        } catch (java.net.MalformedURLException e) {
            e.printStackTrace();
        }
    }



    //Init drawing zone
    void fillDrawingStageMatthieuTest() {
        try {
            Image pic = new Image(new File(_plan.get_currLevel().get_picture()).toURI().toURL().toExternalForm());
            getRoot().getChildren().clear();
            addBackgroundMatthieuTest(pic);
        } catch (java.net.MalformedURLException e) {
            e.printStackTrace();
        }
    }


    private void addBackground(Image pic) {
        // add the background image
        ImageView bg = new ImageView();
        bg.setImage(pic);
        //bg.setOpacity(0.5);                     //Opacité. A remettre si besoin.
        _drawingPane.getChildren().add(bg);
    }


    private void addBackgroundMatthieuTest(Image pic) {
        // add the background image
        ImageView bg = new ImageView();
        bg.setImage(pic);
        //bg.setOpacity(0.5);                     //Opacité. A remettre si besoin.
        getRoot().getChildren().add(bg);
    }


    private void addGrid(Image pic) {
        // add the drawing grid
        Canvas canvas = new Canvas(pic.getWidth(), pic.getHeight());
        GraphicsContext gc = canvas.getGraphicsContext2D();
        _drawingPane.getChildren().add(canvas);

        gc.setFill(Color.gray(0, 0.2));

        for (int y = 0; y < pic.getHeight(); y += 25) {
            for (int x = 0; x < pic.getWidth(); x += 25) {
                gc.fillOval(x - 1.5, y - 1.5, 3, 3);
            }
        }
    }

    private EventHandler<KeyEvent> keyHandler = new EventHandler<KeyEvent>() {
        @Override
        public void handle(KeyEvent event) {
            if (event.getEventType() == KeyEvent.KEY_PRESSED) {
                if (event.getCode().equals(KeyCode.DELETE) || event.getCode().equals(KeyCode.INSERT)) {
                    Tools tmp = _currentTool;
                    _currentTool = Tools.DELMODE;
                    for(AGraphic g: _l_select) {
                        g.remove();
                    }
                    _currentTool = tmp;
                    InspectorController.instance.close();
                } else if (event.getCode().equals(KeyCode.L)){
                    linkPlacement();
                    if (_l_select.size() > 1) {
                        for (AGraphic g : _l_select) {
                            if (g.getClass() != Photo.class)
                                return;
                        }
                        ArrayList<Photo> l_photo = Plan.shortListByClass(_l_select, new Photo(), null);
                        _plan.get_currLevel().generateLinkSelect(l_photo);
                    }
                } else if  (event.getCode().equals(KeyCode.R)){
                    drawingMode();
                } else if  (event.getCode().equals(KeyCode.C)) {
                    drawPolyMode();
                } else if  (event.getCode().equals(KeyCode.D)){
                    doorPlacement();
                } else if  (event.getCode().equals(KeyCode.S)){
                    stairsPlacement();
                } else if  (event.getCode().equals(KeyCode.P)){
                    photoPlacement();
                }
            }
        }
    };

    private EventHandler<ScrollEvent> scrollEvent = new EventHandler<ScrollEvent>() {
        @Override
        public void handle(ScrollEvent event) {
            if (event.isControlDown()) {
                double zoomFactor = 1.05;
                double deltaY = event.getDeltaY();
                _scrollValue -= 5;
                if (deltaY < 0) {
                    zoomFactor = 2.0 - zoomFactor;
                    _scrollValue += 10;
                }
                System.out.println(zoomFactor);

                Scale newScale = new Scale();
                newScale.setX( _scroll.getScaleX() * zoomFactor );
                newScale.setY( _scroll.getScaleY() * zoomFactor );
                _drawingPane.getTransforms().add(newScale);

                event.consume();
            }
        }
    };

    private void addCanvas(Image pic) {
        _eventCanvas = new Canvas(pic.getWidth(), pic.getHeight());

        _drawingPane.getScene().setOnKeyPressed(keyHandler);
        _drawingPane.setOnScroll(scrollEvent);
        _drawingPane.addEventFilter(ScrollEvent.SCROLL,new EventHandler<ScrollEvent>() {
            @Override
            public void handle(ScrollEvent event) {
                if (event.getDeltaX() != 0) {
                    if (event.isControlDown())
                        event.consume();
                }
            }
        });

        _drawingPane.getChildren().add(_eventCanvas);
    }

    @FXML
    private void goMenu() throws IOException {
        Main.showInterfaceMenu();
    }

    public Tools get_curentTool() {
        return _currentTool;
    }


    //SelectManipulation
    public void addToSelect(AGraphic g) {
        if (!_l_select.contains(g))
            _l_select.add(g);
    }

    public void resetSelect() {
        for (AGraphic g: _l_select) {
            g.unselect();
        }
        _l_select.clear();
    }

    private void initHelper() {
        _paneHelper.setVisible(true);
        _btnNext.setEffect(effectLighting);
        _lButton = new ArrayList<Button>();
        _lButton.add(_btnNext);
        _lButton.add(_btnNew);
        _lButton.add(_btnOpen);
        _lButton.add(_btnSave);
        _lButton.add(_btnPreview);
        _lButton.add(_btnExport);
        _lButton.add(_btnClose);
        _lButton.add(_btnSettings);
        _lButton.add(_btnAbout);
        _lButton.add(_btnLogout);
        _lButton.add(_btnSquare);
        _lButton.add(_btnPoly);
        _lButton.add(_btnDoor);
        _lButton.add(_btnStairs);
        _lButton.add(_btnPhoto);
        _lButton.add(_btnLink);
        _lButton.add(_btnLinkAuto);
        _lButton.add(_btnEditVisit);
        _lButton.forEach(Button->Button.setDisable(true));
        _lButton.get(0).setDisable(false);
        _lStringHelp = new ArrayList<String>();
        if (Main.getLanguage().equals("fr")) {
            _lStringHelp.add("Bonjour et Bienvenue dans la visite guidée du logiciel 360IMMO !\n" +
                    "Cette visite va vous expliquez briévement les différentes fonctionnalité a votre disposition. Cliquez sur le bouton suivant ! ");
            _lStringHelp.add("Le bouton Nouveau vous permez de commencer la création d'une visite virtuelle.\n" +
                    "Ouvrez le l'image du plan associé a la visite.");
            _lStringHelp.add("Le bouton Ouvrir vous permez de continuer l'édition d'une visite virtuelle." +
                    "Ouvrez la visite que vous souhaitez édité grace a l'assistant, et reprenez ou vous vous étiez arrété.");
            _lStringHelp.add("Le bouton Enregistrer sous vous permez de continuer l'édition d'une visite virtuelle.\n" +
                    "Sauvegarder la visite en lui choissisant un nom !.");
            _lStringHelp.add("Le bouton Aperçu vous permez d'avoir un apreçu de la visite virtuelle en cours d'édition.\n" +
                    "L'aperçu est une fonction disponible en réalité virtuelle.");
            _lStringHelp.add("Le bouton Exporter vous permez de rendre la visite virtuelle en cours d'édition visible sur " +
                    "la page web dédié a vos création. Visible par vos visiteurs");
            _lStringHelp.add("Le bouton Fermer vous permez de fermer la visite virtuelle en cours d'édition.\n" +
                    "Pensez a sauvegarder !");
            _lStringHelp.add("Le bouton Paramètre vous permez d'acceder au paramètres du logiciel 360IMMO.\n" +
                    "choix de langues, Affichage de l'aide.");
            _lStringHelp.add("Le bouton A Propos vous permez d'obtenir des information sur votre compte utilisateur 360IMMO.\n" +
                    "Aide, License, Adresse web de votre page dédié au visite");
            _lStringHelp.add("Le bouton Déconnexion vous permez de déconnecté votre compte 360IMMO.");
            _lStringHelp.add("Le bouton Rectangle permet de dessiner une salle rectangulaire sur le plan.\n" +
                    "par un simple glisser déposer, dessiner une salle de votre visite !");
            _lStringHelp.add("Le bouton Polygone permet de dessiner une salle polygonale sur le plan.\n" +
                    "par un simple glisser déposer, dessiner une salle de votre visite !");
            _lStringHelp.add("Le bouton Porte permet de definir l'emplacement d'une porte sur le plan.\n" +
                    "d'un simple clic ! définisser une porte a votre salle ! ");
            _lStringHelp.add("Le bouton Escalier permet de definir l'emplacement d'un escalier sur le plan.\n" +
                    "d'un simple clic ! poser un escalier dans votre salle !");
            _lStringHelp.add("Le bouton Photo permet de definir l'emplacement d'une photo dans une salle du plan.\n" +
                    "d'un simple clic ! choissisez l'emplacement de vos photo ! ");
            _lStringHelp.add("Le bouton Lien permet de crée des liens entre les emplacment photo.\n" +
                    "cela vous permettra de changer de salle dans la visite virtuelle ! ");
            _lStringHelp.add("Le bouton Liens auto permet relier les emplacement photo entre eux automatiquement.\n" +
                    "gagnez du temps avec cette option ! ");
            _lStringHelp.add("Le bouton Gérer les visites ouvre le panneau d'administration de vos visite virtuelle actuellement sur votre page web");
        } else {
            _lStringHelp.add("Hello and Welcome to the features tour of software 360IMMO !\n" +
                    "This visit will explain you briefly the various features at your disposal. Click the next button!");
            _lStringHelp.add("The News button allows you to start creating a virtual visit.\n" +
                    "Open the image of the plan associated with the visit.");
            _lStringHelp.add("The Open button allows you to continue editing a virtual tour." +
                    "open the visit you want edit by the assistant, and resume where you stopped.");
            _lStringHelp.add("The Save button allows you yo continue editing a virtual visit.\n" +
                    "Save virtual visit by choosing a name !.");
            _lStringHelp.add("The Preview button allows you to preview the virtual visit in editing.\n" +
                    "Preview is available in virtual reality.");
            _lStringHelp.add("The Export button allows you yo make the virtual visit in editing visible on " +
                    "the web page dedicated to your creation, visible to your visitors/customers");
            _lStringHelp.add("The Close button allows you to close the virtual visit in editing.\n" +
                    "Remember to save!");
            _lStringHelp.add("The Settings button allows you to access settings of 360IMMo software.\n" +
                    "Choose languages, Display help.");
            _lStringHelp.add("The About button allows you yo get information about your 360IMMO user account.\n" +
                    "Help, License, Web address of your dedicated page");
            _lStringHelp.add("The Logout button allows you to disconnect your 360IMMO account.");
            _lStringHelp.add("The Rectangle button allows you to draw a rectangular room on the plan.\n" +
                    "simple drag and drop, draw a room for your visit !");
            _lStringHelp.add("The Polygon button allows you to draw a polygonal room on the plan.\n" +
                    "simple drag and drop, draw a room for your visit !");
            _lStringHelp.add("The Door button is used to define the location of a door on the plan.\n" +
                    "with a simple click! define a door to your room!");
            _lStringHelp.add("The Stair button is used to define the location of a staircase on the plan.\n" +
                    "with a simple click! put a staircase in your room!");
            _lStringHelp.add ("The Photo button is used to define the location of a photo in a room of the plan.\n" +
                    "with a simple click! choose the location of your photos!");
            _lStringHelp.add ("The Link button lets you create links between photo locations.\n" +
                    "This will allow you to change rooms in the virtual visit!");
            _lStringHelp.add ("The Auto Link button allows you to link photo locations between them automatically.\n" +
                    "save time with this option!");
            _lStringHelp.add ("The Edit Visit button opens the administration panel of your virtual visit currently on your web page");
        }
        _txtAreaHelp.setText(_lStringHelp.get(0));
    }

    @FXML
    public void helper() {
        SingleSelectionModel<Tab> SelectionModel = _tab.getSelectionModel();
        int x = 0;
        if (!helperLevel) {
            _paneHelper.setVisible(false);
            for (Button a_lButton : _lButton)
                a_lButton.setDisable(false);
            InterfaceLevels.instance.helper();
        }
        else if (!_lButton.isEmpty()) {
            while (_lButton.get(x).isDisable())
                x++;
            _lButton.get(x).setEffect(effectBasic);
            _lButton.get(x).setDisable(true);
            if (x == 8)
                SelectionModel.select(1);
            if (x + 1 < _lButton.size()) {
                _lButton.get(x + 1).setEffect(effectLighting);
                _lButton.get(x + 1).setDisable(false);
                if (x + 1 < _lStringHelp.size())
                    _txtAreaHelp.setText(_lStringHelp.get(x + 1));
                else
                    _txtAreaHelp.setText("Help for button " + _lButton.get(x+1).getText() + " NYI! cf TODO Language Class");
            }
            else if (helperLevel){
                SelectionModel.select(0);
                if (Main.getLanguage().equals("fr")) {
                    _txtAreaHelp.setText("Le bouton Ajouter Etage vous permez d'ajouter un niveau a la visite virtuelle en cours d'édition." +
                            "pour cela il vous faudra ouvrir l'image du plan associé au niveau souhaité.");
                }
                else {
                    _txtAreaHelp.setText("The Add Level button allows you to add a level to the virtual visit in editing." +
                            "for this you will need to open the image of the plan associated to the desired level.");
                }
                helperLevel = false;
                InterfaceLevels.instance.helperLevel();
            }
            if (_lButton.get(x).equals(_btnNext)) {
                _lButton.get(x).setDisable(false);
                _lButton.remove(x);
                _lStringHelp.remove(x);
            }

        }
    }

    public ArrayList<AGraphic> get_l_select() {
        return _l_select;
    }

    public Plan get_plan() {
        return _plan;
    }

    public Pane get_drawingPane() {
        return _drawingPane;
    }

    public IPreview get_preview() {
        return _preview;
    }

    public double get_scrollValue() {
        return _scrollValue;
    }

    public Effect getEffectLighting() {
        return effectLighting;
    }
}
