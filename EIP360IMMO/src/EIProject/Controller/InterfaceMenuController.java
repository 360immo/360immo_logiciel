package EIProject.Controller;

import EIProject.Main;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

import java.io.IOException;

public class InterfaceMenuController {

    private Main main;
    @FXML private Button AccountBtn;

    @FXML
    private void goMainScene() throws IOException {
        main.initInterface();
        main.showInterfaceAsset();
        main.showInterfaceLevel();
        main.showInterfacePanel();
    }


    @FXML
    private void goAccountScene() throws IOException {
        main.showAccountMenu();
    }

    @FXML
    private void goCloseProgram() throws IOException {
        main.myClosed();
    }





}
