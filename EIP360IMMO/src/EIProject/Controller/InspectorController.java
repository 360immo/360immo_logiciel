package EIProject.Controller;

import EIProject.Controller.Inspector.*;
import EIProject.Main;
import EIProject.Model.Graphic.AGraphic;
import EIProject.Model.Graphic.Numerisation.Room.ARoom;
import EIProject.Model.Graphic.Numerisation.Symbol.Door;
import EIProject.Model.Graphic.Numerisation.Symbol.Stairs;
import EIProject.Model.Graphic.VisiteVirtuelle.Link;
import EIProject.Model.Graphic.VisiteVirtuelle.Photo;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * 09/05/2017
 * @author Nicolas Legendre
 * @version 1.0
 *          Controller pour la partie panel interface (panel de gauche du logiciel)
 */
public class InspectorController {

    static public InspectorController instance;

    private AGraphic _target;
    private IInspector _current;

    //Inspector ref
    private RoomInspector _i_room;
    private PhotoInspector _i_photo;
    private LinkInspector _i_link;
    private DoorInspector _i_door;
    private StairInspector _i_stair;

    @FXML
    private AnchorPane _pane;

    @FXML
    protected void initialize() {
        if (instance == null) {
            instance = this;
        }
        System.out.println("interface inspector");

        // Load RoomInspector
        ResourceBundle bundle = ResourceBundle.getBundle("UIResources", new Locale(Main.getLanguage()));
        FXMLLoader loader = new FXMLLoader(Main.class.getResource("view/Inspector/RoomInspectorView.fxml"), bundle);
        try {
            loader.load();
            _i_room = loader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Load PhotoInspector
        FXMLLoader loader_p = new FXMLLoader(Main.class.getResource("view/Inspector/PhotoInspectorView.fxml"), bundle);
        try {
            loader_p.load();
            _i_photo = loader_p.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Load LinkInspector
        FXMLLoader loader_l = new FXMLLoader(Main.class.getResource("view/Inspector/LinkInspectorView.fxml"), bundle);
        try {
            loader_l.load();
            _i_link = loader_l.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Load DoorInspector
        FXMLLoader loader_d = new FXMLLoader(Main.class.getResource("view/Inspector/DoorInspectorView.fxml"), bundle);
        try {
            loader_d.load();
            _i_door = loader_d.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Load StairsInspector
        FXMLLoader loader_s = new FXMLLoader(Main.class.getResource("view/Inspector/StairInspectorView.fxml"), bundle);
        try {
            loader_s.load();
            _i_stair = loader_s.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    // this function is call on each user click to change the inspector
    public void setInspecteurTarget(AGraphic target) {
        Node node = null;

        if (target != null && target != _target) {
            if (_current != null) {
                _current.hide();
                _pane.getChildren().remove(_current);
            }

            if (target instanceof ARoom) {
                _current = _i_room;
            } else if (target instanceof Photo) {
                _current = _i_photo;
            } else if (target instanceof Link) {
                _current = _i_link;
            } else if (target instanceof Door) {
                _current = _i_door;
            }else if (target instanceof Stairs) {
                _current = _i_stair;
            }
            node = _current.setTarget(target);
            if (node != null && !_pane.getChildren().contains(node))
                _pane.getChildren().add(node);
        }

        _target = target;
        _current.show();
    }

    public void close() {
        if (_current != null)
            _current.hide();
    }
}
