package EIProject.Controller;

import EIProject.Main;
import EIProject.Model.FXTools.ProgressForm;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Pane;
import javafx.stage.DirectoryChooser;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.HashMap;

public class InterfaceAssets {

    static public InterfaceAssets instance;

    @FXML private Label Steplabel;
    @FXML private ListView Loadphoto;
    @FXML
    private Button _btnAddFiles;
    @FXML
    private Pane _paneHelper;
    @FXML
    private TextArea _txtAreaHelp;

    private ObservableList<File> _ol_file;

    private HashMap<File, Image> _h_images;


    static final String[] EXTENSIONS = new String[]{
            "png", "jpg"
    };

    static final FilenameFilter IMAGE_FILTER = new FilenameFilter() {

        @Override
        public boolean accept(final File dir, final String name) {
            for (final String ext : EXTENSIONS) {
                if (name.endsWith("." + ext)) {
                    return (true);
                }
            }
            return (false);
        }
    };

    @FXML
    public void openDirectoryChooser() {
        if (InterfaceMain.instance.get_plan() != null) {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            File selectedDirectory =
                    directoryChooser.showDialog(null);

            if (selectedDirectory != null) {
                _ol_file = FXCollections.observableArrayList(selectedDirectory.listFiles(IMAGE_FILTER));
                InterfaceMain.instance.get_plan().set_picFolder(selectedDirectory.getAbsolutePath());
            }
            openTitledPane();
        }
    }

    public void initialize() {
        if (instance == null)
            instance = this;

        _paneHelper.setVisible(false);
        //TODO Implement Software Properties - Helper On/Off  (CheckBox)
        if (Main.getHelper())
            _btnAddFiles.setDisable(true);
    }

    public void helperAsset() {
        _paneHelper.setVisible(true);
        _btnAddFiles.setDisable(false);
        _btnAddFiles.setEffect(InterfaceMain.instance.getEffectLighting());
        if (Main.getLanguage().equals("fr")) {
            _txtAreaHelp.setText("Le bouton Import Photos vous permez d'importé les photo necessaire a la création de votre visite virtuelle." +
                    "les photos doivent être présente dans un seul et même dossier.");
        }
        else {
            _txtAreaHelp.setText("The Import Pictues button allows you yo import the photos needed to create your virtual visit." +
                    "the photos must be present in the same directory.");
        }

    }

    public void helper() {
        _btnAddFiles.setEffect(null);
        _paneHelper.setVisible(false);
    }

    public void loadPhotoDirectory(File to_load) {
        _ol_file = FXCollections.observableArrayList(to_load.listFiles(IMAGE_FILTER));
        openTitledPane();
    }

    public void closePhotoDirectory() {
        _ol_file = FXCollections.observableArrayList();
        InterfaceMain.instance.get_plan().set_picFolder("");
        openTitledPane();
    }

    private final ObjectProperty<ListCell<File>> dragSource = new SimpleObjectProperty<>();

    @FXML
    public void openTitledPane() {

        ProgressForm pForm = new ProgressForm();

        Task<Void> task = new Task<Void>() {
            @Override
            public Void call() throws InterruptedException {
                _h_images = new HashMap<>();
                int i = 0;
                for (File file : _ol_file) {
                    try {
                        _h_images.put(file, SwingFXUtils.toFXImage(ImageIO.read(file), null));
                        updateProgress(++i, _ol_file.size());
                    } catch (IOException ex) {
                    }
                }
                return null;
            }
        };

        // binds progress of progress bars to progress of task:
        pForm.activateProgressBar(task, "Loading...");

        // in real life this method would get the result of the task
        // and update the UI based on its value:
        task.setOnSucceeded(event -> {
            //Config ListView
            Loadphoto.setItems(_ol_file);
            Loadphoto.setOrientation(Orientation.HORIZONTAL);


            //Config ListCell
            Loadphoto.setCellFactory(param -> {
                ListCell<File> cell = new ListCell<File>() {

                    @Override
                    public void updateItem(File file, boolean empty) {
                        super.updateItem(file, empty);
                        setContentDisplay(ContentDisplay.TOP);
                        prefWidthProperty().set(120);
                        setMaxWidth(120);
                        setStyle("-fx-alignment: center");

                        if (empty) {
                            setText(null);
                            setGraphic(null);
                        } else {
                            Image i = _h_images.get(file);


                            ImageView imageViews = new ImageView();
                            imageViews.setImage(i);
                            imageViews.setFitWidth(50);
                            imageViews.setPreserveRatio(true);
                            setText(file.getName());
                            setGraphic(imageViews);
                        }
                    }
                };

                cell.setOnDragDetected(e -> {
                    if (!cell.isEmpty()) {
                        Dragboard db = cell.startDragAndDrop(TransferMode.COPY);
                        ClipboardContent cc = new ClipboardContent();
                        cc.putString(cell.getItem().getAbsolutePath());
                        db.setContent(cc);
                        dragSource.set(cell);
                        cell.setCursor(Cursor.OPEN_HAND);
                    }
                });

                return cell;
            });


            pForm.getDialogStage().close();
        });

        pForm.getDialogStage().show();

        Thread thread = new Thread(task);
        thread.start();
    }
}
