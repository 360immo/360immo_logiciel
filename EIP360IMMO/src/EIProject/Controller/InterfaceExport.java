package EIProject.Controller;

import EIProject.Main;
import EIProject.Model.Export.Export;
import EIProject.Model.Graphic.VisiteVirtuelle.Photo;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

/**
 * Created by cyril on 6/23/2017.
 */
public class InterfaceExport {

    @FXML
    private AnchorPane  _AnchorExportInProgress;
    @FXML
    private AnchorPane  _AnchorExportMain;
    @FXML
    private AnchorPane _AnchorExportError;
    @FXML
    private AnchorPane _AnchorExportSuccess;
    @FXML
    private AnchorPane _AnchorExportDoublon;
    @FXML
    private AnchorPane _AnchorExportRename;
    @FXML
    private TextField _textRename;
    @FXML
    private Label _labErrorRename;


    public void closeExport() {
        Main.closeInterfaceExport();
    }

    public synchronized void startExport() throws FileNotFoundException, ExecutionException, InterruptedException {
        _AnchorExportInProgress.setVisible(true);
        _AnchorExportMain.setVisible(false);
        _AnchorExportError.setVisible(false);
        _AnchorExportSuccess.setVisible(false);
        _AnchorExportDoublon.setVisible(false);
        _AnchorExportRename.setVisible(false);
        Task<Integer> task = new Task<Integer>() {
            @Override
            public Integer call() throws FileNotFoundException {
                ArrayList<Photo> l_photo = InterfaceMain.instance.get_plan().getAllPlanPhoto();
                Export ExportJson = new Export();
                Integer ErrorUpload =  ExportJson.ExportPhotos(l_photo, false);
                Platform.runLater(() -> {
                    if (ErrorUpload == 0) {
                        _AnchorExportSuccess.setVisible(true);
                        _AnchorExportInProgress.setVisible(false);
                    } else if (ErrorUpload == 1){
                        _AnchorExportError.setVisible(true);
                        _AnchorExportInProgress.setVisible(false);
                    } else if (ErrorUpload == 2){
                        _AnchorExportDoublon.setVisible(true);
                        _AnchorExportInProgress.setVisible(false);
                    }
                });
                return ErrorUpload;
            }
        };
        new Thread(task).start();
    }

    public synchronized void startUpdate() throws FileNotFoundException, ExecutionException, InterruptedException {
        _AnchorExportInProgress.setVisible(true);
        _AnchorExportMain.setVisible(false);
        _AnchorExportError.setVisible(false);
        _AnchorExportSuccess.setVisible(false);
        _AnchorExportDoublon.setVisible(false);
        _AnchorExportRename.setVisible(false);
        Task<Integer> task = new Task<Integer>() {
            @Override
            public Integer call() throws FileNotFoundException {
                ArrayList<Photo> l_photo = InterfaceMain.instance.get_plan().getAllPlanPhoto();
                Export ExportJson = new Export();
                Integer ErrorUpload =  ExportJson.ExportPhotos(l_photo, true);
                Platform.runLater(() -> {
                    if (ErrorUpload == 0) {
                        _AnchorExportSuccess.setVisible(true);
                        _AnchorExportInProgress.setVisible(false);
                    } else if (ErrorUpload == 1){
                        _AnchorExportError.setVisible(true);
                        _AnchorExportInProgress.setVisible(false);
                    }
                });
                return ErrorUpload;
            }
        };
        new Thread(task).start();
    }

    public void interfaceRename() {
        _AnchorExportRename.setVisible(true);
        _AnchorExportInProgress.setVisible(false);
        _AnchorExportMain.setVisible(false);
        _AnchorExportError.setVisible(false);
        _AnchorExportSuccess.setVisible(false);
        _AnchorExportDoublon.setVisible(false);
    }

    public void startRename() throws FileNotFoundException, ExecutionException, InterruptedException {
        _AnchorExportInProgress.setVisible(true);
        _AnchorExportMain.setVisible(false);
        _AnchorExportError.setVisible(false);
        _AnchorExportSuccess.setVisible(false);
        _AnchorExportDoublon.setVisible(false);
        _AnchorExportRename.setVisible(false);
        if(!_textRename.getText().isEmpty())
        {
            if(!_textRename.getText().equals(InterfaceMain.instance.get_plan().get_name())) {
                InterfaceMain.instance.get_plan().set_name(_textRename.getText());
                startExport();
            }
            else {
                _textRename.setText("");
                _labErrorRename.setVisible(true);
            }
        }
        else {
            _textRename.setText("");
            _labErrorRename.setVisible(true);
        }
    }
}