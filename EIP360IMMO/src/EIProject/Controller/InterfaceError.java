package EIProject.Controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class InterfaceError {

    @FXML
    private Label _labError;
    @FXML
    private Label _labError2;
    @FXML
    private Label _labErrorNumber;
    static int _error = 0;

    /*
    Interface Error.
    Allow management of error box/popup easy !
    around 85-90 character by label
    choose a number for your error ;) not yet use...

    use _labError.setText() for set label 1
    use _labError2.setText() for set label 2

    write in your method
    InterfaceError._error = [errorNumber];
    Main.showInterfaceError();
     */
    @FXML
    private void initialize() {
        _labError.setVisible(false);
        _labError2.setVisible(false);
        _labError.setText("");
        _labError2.setText("");
        if (_error == 1)
            _labError.setText("The Export functionnality can only be used if a plan is open.");
        else if (_error == 2) {
            _labError.setText("File selected is not supported by 360IMMO software.");
            _labError2.setText("Wrong format or corrupted file.");
        } else if (_error == 3)
            _labError.setText("Save of visit fail, Please try again.");
        else if (_error == 4)
            _labError.setText("You need login first !");

        if (_labError.getText() != null)
            _labError.setVisible(true);
        if (_labError2.getText() != null)
            _labError2.setVisible(true);
        _labErrorNumber.setText("Error" + _error);
    }
}
