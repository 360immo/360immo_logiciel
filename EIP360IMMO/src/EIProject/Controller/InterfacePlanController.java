package EIProject.Controller;

import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class InterfacePlanController {

    @FXML private ImageView myimgview;
    @FXML private Label Steplabel;

    private static int i;



    @FXML public ScrollPane _rootPane;
    @FXML public AnchorPane _controlPane;
    @FXML public StackPane _drawingPane;

    @FXML
    public void LoadImgtoapp() throws IOException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("360IMMO Files", "*.360immo"),
                new FileChooser.ExtensionFilter("Text Files", "*.txt"),
                new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"),
                new FileChooser.ExtensionFilter("Audio Files", "*.wav", "*.mp3", "*.aac"),
                new FileChooser.ExtensionFilter("All Files", "*.*"));
        File file = fileChooser.showOpenDialog(null);

        if (file != null) {
            try {
                BufferedImage bufferedImage = ImageIO.read(file);
                WritableImage image = SwingFXUtils.toFXImage(bufferedImage, null);
                myimgview.setImage(image);
                i = 1;
            } catch (IOException ex) {
                Logger.getLogger(InterfacePlanController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public int getI(){
        return i;
    }
}
