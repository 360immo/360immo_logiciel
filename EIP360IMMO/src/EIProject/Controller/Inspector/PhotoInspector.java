package EIProject.Controller.Inspector;

import EIProject.Controller.InterfaceMain;
import EIProject.Model.FXTools.PhotoPreview;
import EIProject.Model.Graphic.AGraphic;
import EIProject.Model.Graphic.VisiteVirtuelle.Photo;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.VBox;

import java.io.File;


/**
 * Created by nicol on 09/05/2017.
 */
public class PhotoInspector implements IInspector {

    Photo _photo;

    @FXML
    VBox _pane;
    @FXML
    TextField _name;
    @FXML
    Label _room;
    @FXML
    TextArea _description;
    @FXML
    CheckBox _isFirstPhoto;
    @FXML
    CheckBox _autoRot;
    @FXML
    TextField _path;
    @FXML
    ImageView _preview;

    private ChangeListener _checkFirstListener = new ChangeListener<Boolean>() {
        public void changed(ObservableValue<? extends Boolean> ov, Boolean old_val, Boolean new_val) {
            if (_isFirstPhoto.isSelected())
                InterfaceMain.instance.get_plan().set_firstPhoto(_photo);
            else
                InterfaceMain.instance.get_plan().set_firstPhoto(null);
        }
    };


    private EventHandler<DragEvent> dragHandler = new EventHandler<DragEvent>() {
        public void handle(DragEvent event) {

            if (event.getEventType() == DragEvent.DRAG_ENTERED) {
                event.acceptTransferModes(TransferMode.COPY);
            } else if (event.getEventType() == DragEvent.DRAG_EXITED) {
            } else if (event.getEventType() == DragEvent.DRAG_DROPPED) {
                /* data dropped */
                /* if there is a string data on dragboard, stock it in string */
                boolean success = false;
                if (event.getDragboard().hasString()) {
                    _photo.set_path(event.getDragboard().getString());
                    success = true;
                    updatePhoto();
                }
                /* let the source know whether the string was successfully
                 * transferred and used */
                event.setDropCompleted(success);
            } else if (event.getEventType() == DragEvent.DRAG_OVER) {
                event.acceptTransferModes(TransferMode.COPY);
            }
            event.consume();
        }
    };

    @FXML
    protected void initialize() {
        _pane.setVisible(false);

        _name.textProperty().addListener((obs, oldText, newText) -> {
            _photo.set_name(newText);
        });

        _description.textProperty().addListener((obs, oldText, newText) -> {
            _photo.set_description(newText);
        });

        _autoRot.selectedProperty().addListener((obs, oldText, newText) -> {
            _photo.set_autoRot(_autoRot.isSelected());
        });

        _path.setOnDragEntered(dragHandler);
        _path.setOnDragExited(dragHandler);
        _path.setOnDragDropped(dragHandler);
        _path.setOnDragDone(dragHandler);
        _path.setOnDragOver(dragHandler);
    }

    @Override
    public Node setTarget(AGraphic target) {
        if (target instanceof Photo) {

            _photo = (Photo) target;

            _name.setText(_photo.get_name());
            _description.setText(_photo.get_description());
            _room.setText("Room : " + _photo.get_room().get_name());
            _autoRot.setSelected(_photo.get_autoRot());
            _pane.setVisible(true);

            _isFirstPhoto.selectedProperty().removeListener(_checkFirstListener);

            if (InterfaceMain.instance.get_plan().get_firstPhoto() == _photo) {
                _isFirstPhoto.setSelected(true);
            } else
                _isFirstPhoto.setSelected(false);

            _isFirstPhoto.selectedProperty().addListener(_checkFirstListener);


            updatePhoto();


            return _pane;
        } else
            return null;
    }

    @Override
    public void hide() {
        _pane.setVisible(false);
    }
    @Override
    public void show() {
        _pane.setVisible(true);
    }

    private void updatePhoto() {
        String path = _photo.get_path();
        if (path != null && !path.isEmpty()) {
            File imageFile = new File(path);
            _path.setText(imageFile.getName());

            Image img = new Image(imageFile.toURI().toString());
            _preview.setImage(img);
        } else {
            _path.setText("");
            _preview.setImage(null);
        }
    }

    @FXML
    void imgClick() {
        if (_photo.get_path() != null && !_photo.get_path().isEmpty())
            new PhotoPreview(_photo);
    }
}
