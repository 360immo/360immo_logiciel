package EIProject.Controller.Inspector;

import EIProject.Model.Graphic.AGraphic;
import EIProject.Model.Graphic.Numerisation.Room.ARoom;
import EIProject.Model.Graphic.VisiteVirtuelle.Link;
import EIProject.Model.Graphic.VisiteVirtuelle.Photo;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 * Created by nicol on 09/05/2017.
 */
public class LinkInspector implements IInspector {

    Link _link;

    @FXML
    VBox _pane;

    @FXML
    Label _photo1;
    @FXML
    Label _photo2;

    @FXML
    protected void initialize() {
        _pane.setVisible(false);
    }

    @Override
    public Node setTarget(AGraphic target) {
        if (target instanceof Link) {
            _link = (Link) target;

            Photo p[] = _link.get_l_photos();

            _photo1.setText(p[0].get_name());
            _photo2.setText(p[1].get_name());

            _pane.setVisible(true);
            return _pane;
        } else
            return null;
    }

    @Override
    public void hide() {
        _pane.setVisible(false);
    }
    @Override
    public void show() {
        _pane.setVisible(true);
    }
}
