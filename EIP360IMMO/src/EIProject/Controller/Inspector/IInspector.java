package EIProject.Controller.Inspector;

import EIProject.Model.Graphic.AGraphic;
import javafx.scene.Node;

/**
 * Created by nicol on 09/05/2017.
 */
public interface IInspector {
    Node setTarget(AGraphic target);
    void hide();
    void show();
}
