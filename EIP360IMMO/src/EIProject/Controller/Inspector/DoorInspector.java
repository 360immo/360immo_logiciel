package EIProject.Controller.Inspector;

import EIProject.Model.Graphic.AGraphic;
import EIProject.Model.Graphic.Numerisation.Symbol.Door;
import EIProject.Model.Graphic.VisiteVirtuelle.Link;
import EIProject.Model.Graphic.VisiteVirtuelle.Photo;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 * Created by nicol on 09/05/2017.
 */
public class DoorInspector implements IInspector {

    Door _door;

    @FXML
    VBox _pane;

    @FXML
    Label _room;


    @FXML
    protected void initialize() {
        _pane.setVisible(false);
    }

    @Override
    public Node setTarget(AGraphic target) {
        if (target instanceof Door) {
            _door = (Door) target;

            _room.setText(_door.get_room().get_name());

            _pane.setVisible(true);
            return _pane;
        } else
            return null;
    }

    @Override
    public void hide() {
        _pane.setVisible(false);
    }
    @Override
    public void show() {
        _pane.setVisible(true);
    }
}
