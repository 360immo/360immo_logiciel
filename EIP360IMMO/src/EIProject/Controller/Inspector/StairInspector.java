package EIProject.Controller.Inspector;

import EIProject.Model.Graphic.AGraphic;
import EIProject.Model.Graphic.Numerisation.Symbol.Door;
import EIProject.Model.Graphic.Numerisation.Symbol.Stairs;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by burgat on 09/05/2017.
 */
public class StairInspector implements IInspector {

    static HashMap<String, Stairs> _sl_stairs = new HashMap<>();

    Stairs _stair;

    @FXML
    VBox _pane;

    @FXML
    Label _room;

    @FXML
    Label _level;

    @FXML
    ComboBox _comboBox;


    @FXML
    protected void initialize() {
        _pane.setVisible(false);

        _comboBox.setOnAction((e) -> {
            if (_comboBox.getSelectionModel().getSelectedItem() != null) {
                Stairs linked = _sl_stairs.get(_comboBox.getSelectionModel().getSelectedItem().toString());
                _stair.set_linkedStair(linked);
                linked.set_linkedStair(_stair);
            }
        });
    }

    public static void add_stairs(Stairs toadd) {
        if (!_sl_stairs.containsValue(toadd))
            _sl_stairs.put(toadd.get_level().get_name() + ". Stairs ID: " + toadd.get_id(), toadd);
    }

    public static void remove_stairs(Stairs toremove) {
        if (_sl_stairs.containsValue(toremove))
            _sl_stairs.remove(toremove.get_level().get_name() + ". Stairs ID: " + toremove.get_id());
    }

    @Override
    public Node setTarget(AGraphic target) {
        if (target instanceof Stairs) {
            _stair = (Stairs) target;

            _room.setText(_stair.get_room().get_name());

            _comboBox.getItems().clear();
            for ( String key : _sl_stairs.keySet() ) {
                if (_sl_stairs.get(key).get_level() != _stair.get_level()) {
                    _comboBox.getItems().add(key);
                    if (_stair.get_linkedStair() != null && _sl_stairs.get(key) != null && _sl_stairs.get(key).get_id() == _stair.get_linkedStair().get_id())
                        _comboBox.getSelectionModel().select(key);
                }
            }
            _pane.setVisible(true);
            return _pane;
        } else
            return null;
    }

    @Override
    public void hide() {
        _pane.setVisible(false);
    }

    @Override
    public void show() {
        _pane.setVisible(true);
    }
}
