package EIProject.Controller.Inspector;

import EIProject.Main;
import EIProject.Model.Graphic.AGraphic;
import EIProject.Model.Graphic.Numerisation.Room.ARoom;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

import java.beans.EventHandler;
import java.io.IOException;

/**
 * Created by nicol on 09/05/2017.
 */
public class RoomInspector implements IInspector {

    ARoom _room;

    @FXML
    VBox _pane;
    @FXML
    TextField _name;
    @FXML
    TextArea _description;
    @FXML
    TextField _surface;
    @FXML
    Label _nbrPhoto;
    @FXML
    ColorPicker _color;

    @FXML
    protected void initialize() {
        _pane.setVisible(false);
        _name.textProperty().addListener((obs, oldText, newText) -> {
            _room.set_name(newText);
        });

        _description.textProperty().addListener((obs, oldText, newText) -> {
            _room.set_description(newText);
        });

        _surface.textProperty().addListener((obs, oldText, newText) -> {
            _room.set_surface(newText);
        });

        _color.valueProperty().addListener((obs, oldText, newText) -> {
               _room.set_color(_color.getValue());
        });
    }

    @Override
    public Node setTarget(AGraphic target) {
        if (target instanceof ARoom) {
            _room = (ARoom) target;

            _name.setText(_room.get_name());
            _description.setText(_room.get_description());
            _surface.setText(_room.get_surface());
            _nbrPhoto.setText(_room.get_l_photo().size() + " photos");
            _pane.setVisible(true);
            _color.setValue(_room.get_color());
            return _pane;
        } else
            return null;
    }

    @Override
    public void hide() {
        _pane.setVisible(false);
    }
    @Override
    public void show() {
        _pane.setVisible(true);
    }
}
