package EIProject.Controller;

import EIProject.Main;
import EIProject.Model.Dao.UserDao;
import EIProject.Model.Dao.UserDaoImpl;
import EIProject.Model.SQL.User;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import org.apache.commons.validator.routines.EmailValidator;
import org.jasypt.util.text.StrongTextEncryptor;

import java.io.*;
import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.DAYS;

public class InterfaceLogin {

    static private User User = null;

    @FXML
    private TextField _textLogin;
    @FXML
    private PasswordField _textPassword;
    @FXML
    private Label _labErrorPassword;
    @FXML
    private Label _labErrorLogin;
    @FXML
    private Label _labConnOk;
    @FXML
    private SplitPane _spLogin;
    @FXML
    private AnchorPane _paneLicenseInfo;
    @FXML
    private AnchorPane _paneLicenseExpired;
    @FXML
    private AnchorPane _paneSignup;
    @FXML
    private Pane _paneSuccesSignup;
    @FXML
    private Pane _paneErrorSignup;
    @FXML
    private Label _labLicenseEmail;
    @FXML
    private Label _labLicenseCompany;
    @FXML
    private Label _labLicenseTime;
    @FXML
    private Label _labLicenseTime2;
    @FXML
    private Label _labLicenseEmail2;
    @FXML
    private Label _labErrorLoginSignup;
    @FXML
    private Label _labErrorPasswordSignup;
    @FXML
    private Label _labErrorPasswordSignup2;
    @FXML
    private Label _labErrorCompanySignup;
    @FXML
    private Label _labErrorSignup;
    @FXML
    private TextField _textLoginSignup;
    @FXML
    private PasswordField _textPasswordSignup;
    @FXML
    private PasswordField _textPasswordSignup2;
    @FXML
    private TextField _textCompanySignup;
    @FXML
    private CheckBox _cbAutoLogin;

    @FXML
    private void initialize() {
        if (User == null)
            autoLogin();
        else
            about();
    }

    @FXML
    public void displaySignup() {
        _spLogin.setVisible(false);
        _paneLicenseExpired.setVisible(false);
        _paneLicenseInfo.setVisible(false);
        _paneSignup.setVisible(true);
    }

    @FXML
    public void signup() {
        _labErrorLoginSignup.setVisible(false);
        _labErrorPasswordSignup.setVisible(false);
        _labErrorPasswordSignup2.setVisible(false);
        _labErrorCompanySignup.setVisible(false);
        _labErrorSignup.setVisible(false);
        if(_textLoginSignup.getText().isEmpty()) {
            _labErrorLoginSignup.setText("Email field cannot be empty");
            _labErrorLoginSignup.setVisible(true);
        }
        else if(!EmailValidator.getInstance().isValid(_textLoginSignup.getText())) {
            _labErrorLoginSignup.setText("Email invalid !");
            _labErrorLoginSignup.setVisible(true);
        }
        else if(_textCompanySignup.getText().isEmpty()) {
            _labErrorCompanySignup.setText("Company field cannot be empty");
            _labErrorCompanySignup.setVisible(true);
        }
        else if(_textPasswordSignup.getText().isEmpty()) {
            _labErrorPasswordSignup.setText("Password field cannot be empty");
            _labErrorPasswordSignup.setVisible(true);
        }
        else if(_textPasswordSignup2.getText().isEmpty()) {
            _labErrorPasswordSignup2.setText("Password field cannot be empty");
            _labErrorPasswordSignup2.setVisible(true);
        }
        else if (!_textPasswordSignup.getText().contentEquals(_textPasswordSignup2.getText())) {
            _labErrorPasswordSignup.setText("Password not match. Please, witre again");
            _labErrorPasswordSignup2.setText("Password not match. Please, witre again");
            _labErrorPasswordSignup.setVisible(true);
            _labErrorPasswordSignup2.setVisible(true);
            _textPasswordSignup.setText("");
            _textPasswordSignup2.setText("");
        }
        else {
            User signup = new User(_textLoginSignup.getText(), _textPasswordSignup.getText(), _textCompanySignup.getText());
            UserDao dao = new UserDaoImpl();
            if (dao.readUser(signup)) {
                _labErrorSignup.setText("An account already use this email");
                _labErrorSignup.setVisible(true);
            }
            else {
                if (!dao.create(signup)) {
                    _paneSignup.setDisable(true);
                    _paneSuccesSignup.setVisible(true);
                }
                else {
                    _paneErrorSignup.setVisible(true);
                    _paneSignup.setDisable(true);
                }
            }
        }
    }

    @FXML
    public void successSignup() {
        _paneErrorSignup.setVisible(false);
        _paneSignup.setVisible(false);
        _paneSuccesSignup.setVisible(false);
        _paneLicenseInfo.setVisible(false);
        _paneLicenseExpired.setVisible(false);
        _spLogin.setVisible(true);
    }

    @FXML
    public void closeError() {
        _paneSignup.setDisable(false);
        _paneErrorSignup.setVisible(false);
    }

    @FXML
    public void account() {
        _paneLicenseExpired.setVisible(false);
        _paneLicenseInfo.setVisible(false);
        _labErrorLogin.setVisible(false);
        _labErrorPassword.setVisible(false);
        if(_textLogin.getText().isEmpty())
        {
            _labErrorLogin.setText("Email field cannot be empty");
            _labErrorLogin.setVisible(true);
        }
        if(_textPassword.getText().isEmpty())
        {
            _labErrorPassword.setText("Password field cannot be empty");
            _labErrorPassword.setVisible(true);
        }
        if(!_textLogin.getText().isEmpty() && !_textPassword.getText().isEmpty())
            login(_textLogin.getText(), _textPassword.getText(), false);
    }

    private void login(String email, String password, boolean autoLogin) {
        UserDao dao = new UserDaoImpl();
        if (autoLogin)
            User = dao.autoLogin(email, password);
        else
            User = dao.login(email, password, _cbAutoLogin.isSelected());
        if (User != null)
        {
            _labConnOk.setVisible(true);
            if ((DAYS.between(LocalDate.now(), User.get_license().toLocalDate())) > 0) {
                _paneLicenseInfo.setVisible(true);
                _labLicenseEmail.setText(User.get_email());
                _labLicenseCompany.setText(User.get_companyName());
                _labLicenseTime.setText(User.get_date());
            }
            else {
                _paneLicenseExpired.setVisible(true);
                _labLicenseEmail2.setText(User.get_email());
                _labLicenseTime2.setText(User.get_date());
                Main.licenseExpired();
            }
        }
        else {
            _textLogin.setText("");
            _textPassword.setText("");
            _labErrorLogin.setText("Error: please enter your email again");
            _labErrorLogin.setVisible(true);
            _labErrorPassword.setText("Please enter your password again");
            _labErrorPassword.setVisible(true);
        }
    }

    private void autoLogin() {
        File lic = new File("License.lic");
        if (lic.exists()) {
            try {
                DataInputStream dis = new DataInputStream(new BufferedInputStream(
                        new FileInputStream(lic)));
                StrongTextEncryptor encryptorlic = new StrongTextEncryptor();
                encryptorlic.setPassword("123");
                String string = encryptorlic.decrypt(dis.readUTF());
                if (string.substring(string.indexOf("@-@") + 3, string.length()).contentEquals("true"))
                    login(string.substring(string.indexOf("@@@") + 3, string.indexOf("@_@")), string.substring(string.indexOf("@_@") + 3, string.indexOf("@-@")), true);
                dis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void about() {
        _paneLicenseInfo.setVisible(true);
        _labLicenseEmail.setText(User.get_email());
        _labLicenseCompany.setText(User.get_companyName());
        _labLicenseTime.setText(User.get_date());
    }

    public void displaylogin() {
        _spLogin.setVisible(true);
        _paneLicenseExpired.setVisible(false);
        _paneLicenseInfo.setVisible(false);
        _paneSignup.setVisible(false);
    }
    public static EIProject.Model.SQL.User getUser() {
        return User;
    }

    public static void setUser(EIProject.Model.SQL.User user) {
        User = user;
    }
}
