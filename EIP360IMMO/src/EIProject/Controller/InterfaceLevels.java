package EIProject.Controller;

import EIProject.Controller.Inspector.StairInspector;
import EIProject.Main;
import EIProject.Model.Graphic.AGraphic;
import EIProject.Model.Graphic.Numerisation.Symbol.Stairs;
import EIProject.Model.Level;
import EIProject.Model.Plan;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;

import java.io.File;
import java.util.HashMap;

/**
 * Created by nicol on 21/02/2017.
 */
public class InterfaceLevels {

    static public InterfaceLevels instance;

    @FXML
    private AnchorPane _pane;

    @FXML
    private TextField _text;
    @FXML
    private Button _btnAddLevel;

    private HashMap<Integer, Button> _m_button;
    private HashMap<Integer, Button> _m_delButton;
    private Integer _currentId;

    private EventHandler<KeyEvent> keyEvent = new EventHandler<KeyEvent>() {
        @Override
        public void handle(KeyEvent event) {
            if (event.getCode() == KeyCode.ENTER)
                addLevel();
        }
    };

    private EventHandler<ActionEvent> actionEvent = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            changeLevel(event);
        }
    };

    private EventHandler<ActionEvent> delEvent = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            Integer index = (Integer) ((Button)event.getSource()).getUserData();

            if (_m_button.size() > 1) {
                removeButton(_m_button.get(index));

                Level lvltodel = InterfaceMain.instance.get_plan().get_m_levels().get(index);
                for (AGraphic tmp : lvltodel.get_l_item())
                {
                    if (tmp instanceof Stairs)
                        StairInspector.remove_stairs((Stairs)tmp);
                }
                InterfaceMain.instance.get_plan().del_level(index);

                changeLevel((Integer)_m_button.keySet().toArray()[0]);
            }
        }
    };

    public void initialize() {
        if (instance == null)
            instance = this;

        _m_button = new HashMap<>();
        _m_delButton = new HashMap<>();

        _text.setOnKeyPressed(keyEvent);

        if (Main.getHelper())
            _btnAddLevel.setDisable(true);
    }

    public void helperLevel() {
        _btnAddLevel.setDisable(false);
        _btnAddLevel.setEffect(InterfaceMain.instance.getEffectLighting());
    }

    public void helper() {
        _btnAddLevel.setEffect(null);
        InterfaceAssets.instance.helperAsset();
    }
    // Reset left side view
    public void reset() {
        _currentId = 0;
        Integer count = 0;
        int got = 0;

        while (got < _m_button.size()) {
            if (_m_button.get(count) != null) {
                removeButton(_m_button.get(count));
                got += 1;
            }
            count += 1;
        }
    }

    //Set up first level button
    public void addLevels() {
        HashMap<Integer, Level> levels_iterate = InterfaceMain.instance.get_plan().get_m_levels();
        Integer count = 0;
        int got = 0;
        while (got < levels_iterate.size()) {
            if (levels_iterate.get(count) != null) {
                _text.setText(levels_iterate.get(count).get_name());//"Level 1");
                addButton();
                got += 1;
            }
            _currentId += 1;
            count += 1;
        }
        _text.setLayoutY(100 + 70 * (got - 1));
        _btnAddLevel.setLayoutY(170 + 70 * (got - 1));
    }

    // Add button based on informations of _text/_btnAddLevel
    public void addButton() {
        Button newB = new Button();

        newB.setPrefSize(150, 55);
        newB.setText(_text.getText());
        newB.setLayoutX(30);
        newB.setLayoutY(_m_button.size() * 70 + 30);
        newB.setUserData(_currentId);
        newB.setOnAction(actionEvent);

        Button newBdel = new Button();

        newBdel.setPrefSize(50, 55);
        newBdel.setText("X");
        newBdel.setLayoutX(180);
        newBdel.setLayoutY(_m_button.size() * 70 + 30);
        newBdel.setUserData(_currentId);
        newBdel.setOnAction(delEvent);

        _m_button.put(_currentId, newB);
        _m_delButton.put(_currentId, newBdel);
        _pane.getChildren().add(newB);
        _pane.getChildren().add(newBdel);
        //_currentId++;
        _text.clear();
    }

    // Remove button in args
    private void removeButton(Button toRemove) {
        Integer index = (Integer)toRemove.getUserData();
        _pane.getChildren().remove(_m_delButton.get(index));
        _pane.getChildren().remove(toRemove);

        _m_button.remove(index);
        _m_delButton.remove(index);

        int i = 0;
        for (Button b : _m_button.values()) {
            b.setLayoutY(i * 70 + 30);
            _m_delButton.get((Integer) b.getUserData()).setLayoutY(i * 70 + 30);
            i++;
        }
        _text.setLayoutY(i * 70 + 30);
        _btnAddLevel.setLayoutY((i+1) * 70 + 30);
    }

    private void updateText() {
        _text.setLayoutY(_text.getLayoutY() + 70);
        _text.clear();
        _btnAddLevel.setLayoutY(_btnAddLevel.getLayoutY() + 70);
    }

    //Action Event
    @FXML
    public void addLevel() {
        if (!_text.getText().isEmpty()) {

            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("All Images", "*.jpg", "*.png"),
                    new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                    new FileChooser.ExtensionFilter("PNG", "*.png")
            );
            fileChooser.setInitialDirectory(
                    new File(System.getProperty("user.home"))
            );
            File file = fileChooser.showOpenDialog(null);
            Plan plan = InterfaceMain.instance.get_plan();
            if (file != null) {
                String namesend = _text.getText();
                if (namesend == "") {
                    namesend += (plan.get_m_levels().size() + 1);
                }
                plan.set_currLevel(plan.get_m_levels().size());
                plan.add_level(namesend, file.getAbsolutePath(), _currentId);
                InterfaceMain.instance.drawingMode();

                changeLevel(_currentId);
                addButton();
                _currentId += 1;
                updateText();

                InterfaceMain.instance.drawingMode();
            }
        }
    }

    @FXML
    public void changeLevel(ActionEvent event) {
        Integer id = (Integer) ((Node) event.getSource()).getUserData();

        changeLevel(id);
        InspectorController.instance.close();
    }

    public void changeLevel(int id) {
        if (id >= 0) {
            InterfaceMain.instance.get_plan().set_currLevel(id);
            InterfaceMain.instance.fillDrawingStage();
            InterfaceMain.instance.get_plan().get_currLevel().drawLevel();
        }
    }
}
