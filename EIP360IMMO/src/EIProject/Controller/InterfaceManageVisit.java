package EIProject.Controller;

import EIProject.Main;
import EIProject.Model.Dao.VisitDao;
import EIProject.Model.Dao.VisitDaoImpl;
import EIProject.Model.SQL.Visit;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

import java.util.ArrayList;
import java.util.List;

public class InterfaceManageVisit {

    @FXML
    private TableView<Visit> _tableVisit;
    @FXML
    private TableColumn<Visit, String> _tableColVisitName = new TableColumn<>("Name");
    @FXML
    private TableColumn<Visit, Boolean> _tableColVisible = new TableColumn<>("Visible");
    @FXML
    private Label _labNameVisit;
    @FXML
    private Label _labView;
    @FXML
    private Label _labVisible;
    @FXML
    private Label _labLastEdit;
    @FXML
    private SplitPane _spManageExport;
    @FXML
    private AnchorPane _anchorpVisitDetails;
    @FXML
    private Pane _paneError;
    @FXML
    private Label _labError;
    @FXML
    private Label _labError2;
    @FXML
    private Button _btnVisible;
    @FXML
    private Button _btnDelete;
    @FXML
    private Button _btnNext;
    @FXML
    private TextArea _txtAreaHelp;
    @FXML
    private Pane _paneHelper;
    private List<Button> _lButton = null;
    private List<String> _lStringHelp = null;

    @FXML
    private void initialize() {
        _spManageExport.setDisable(false);
        _paneError.setVisible(false);
        _labError.setText("");
        _labError2.setText("");
        _tableColVisitName.setCellValueFactory(new PropertyValueFactory<>("Name"));
        _tableColVisible.setCellValueFactory((new PropertyValueFactory<>("Visible")));
        _tableColVisible.setCellFactory(column -> new TableCell<Visit, Boolean>() {
            @Override
            protected void updateItem(Boolean item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty)
                    setStyle("-fx-background-color: transparent");
                else {
                    Visit visit = getTableView().getItems().get(getIndex());
                    if (visit.getVisible())
                        setStyle("-fx-background-color: darkgreen");
                    else
                        setStyle("-fx-background-color: orangered");
                }
            }
        });
        VisitDao dao = new VisitDaoImpl();
        ObservableList<Visit> _visits;
        if ((_visits = dao.readAll(InterfaceLogin.getUser())) == null) {
            _paneError.setVisible(true);
            _labError.setText("Connection error ! Please try again.");
            _labError2.setText("Check your connection if problem persist.");
            _spManageExport.setDisable(true);
        } else
            _tableVisit.setItems(_visits);
        showVisitDetails(null);
        _tableVisit.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showVisitDetails(newValue));
        //TODO Implement Software Properties - Helper On/Off  (CheckBox)
        if (Main.getHelper())
            initHelper();
        else
            _paneHelper.setVisible(false);
    }

    private void showVisitDetails(Visit visit) {
        if (visit == null) {
            _labNameVisit.setText("");
            _labView.setText("");
            _labVisible.setText("");
            _labLastEdit.setText("");
            _anchorpVisitDetails.setDisable(true);
        } else {
            _anchorpVisitDetails.setDisable(false);
            _labNameVisit.setText(visit.getName());
            _labVisible.setText(String.valueOf(visit.getVisible()));
            _labView.setText(String.valueOf(visit.getView()));
            _labLastEdit.setText(visit.getLastEdit());
        }
    }

    @FXML
    private void deleteVisit() {
        int i = _tableVisit.getSelectionModel().getSelectedIndex();
        VisitDao dao = new VisitDaoImpl();
        if (i >= 0) {
            if (dao.delete(_tableVisit.getSelectionModel().getSelectedItem())) {
                _paneError.setVisible(true);
                _labError.setText("Connection error ! Please try again.");
                _labError2.setText("Check your connection if problem persist.");
                _spManageExport.setDisable(true);
            } else
                _tableVisit.getItems().remove(i);
        } else {
            _paneError.setVisible(true);
            _labError.setText("Please select one visit !");
            _spManageExport.setDisable(true);
        }
    }

    @FXML
    private void updateVisible() {
        int i = _tableVisit.getSelectionModel().getSelectedIndex();
        VisitDao dao = new VisitDaoImpl();
        Visit visit;
        if (i >= 0) {
            if (dao.updateVisible(_tableVisit.getSelectionModel().getSelectedItem())) {
                _paneError.setVisible(true);
                _labError.setText("Connection error ! Please try again.");
                _labError2.setText("Check your connection if problem persist.");
                _spManageExport.setDisable(true);
            } else {
                visit = _tableVisit.getItems().get(i);
                if (visit.getVisible()) {
                    visit.setVisible(false);
                    _tableVisit.getItems().set(i, visit);
                } else {
                    visit.setVisible(true);
                    _tableVisit.getItems().set(i, visit);
                }
            }
        } else {
            _paneError.setVisible(true);
            _labError.setText("Please select one visit !");
            _spManageExport.setDisable(true);
        }
    }

    @FXML
    private void closeError() {
        _spManageExport.setDisable(false);
        _paneError.setVisible(false);
        _labError.setText("");
        _labError2.setText("");
    }

    private void initHelper() {
        _paneHelper.setVisible(true);
        _btnNext.setEffect(InterfaceMain.instance.getEffectLighting());
        _btnNext.setDisable(false);
        _btnVisible.setDisable(true);
        _btnDelete.setDisable(true);
        _lButton = new ArrayList<Button>();
        _lButton.add(_btnNext);
        _lButton.add(_btnVisible);
        _lButton.add(_btnDelete);
        _lStringHelp = new ArrayList<String>();
        if (Main.getLanguage().equals("fr")) {
            _lStringHelp.add("Cette interface de management de visite vous donne acces a l'ensemble des fonctionnalité pour l'administration de vos visite virtuelle.");
            _lStringHelp.add("Le bouton Changer Visibilité vous permez de rendre visible ou non votre visite virtuelle, sur votre page web.");
            _lStringHelp.add("Le bouton Supprimer Visite vous permez d'effacer completement une visite virtuelle de votre page web.\n" +
                    "!!! Cette opération ne peux pas être annulé !!! .");
        }
        else {
            _lStringHelp.add ("This edit visit interface gives you access to all the features for administering your virtual visits.");
            _lStringHelp.add ("The Switch Visibility button allows you to make your virtual visit visible or not on your web page.");
            _lStringHelp.add ("The Delete Visit button allows you to completely erase a virtual visit of your web page.\n" +
                    "!!! This procedure can not be undo !!!.");        }
        _txtAreaHelp.setText(_lStringHelp.get(0));
    }

    @FXML
    private void helper() {
        int x = 0;
        if (!_lButton.isEmpty()) {
            while (_lButton.get(x).isDisable())
                x++;
            _lButton.get(x).setEffect(null);
            _lButton.get(x).setDisable(true);
            if (x  + 1 < _lButton.size()) {
                _lButton.get(x + 1).setEffect(InterfaceMain.instance.getEffectLighting());
                _lButton.get(x + 1).setDisable(false);
                if (x + 1 < _lStringHelp.size())
                    _txtAreaHelp.setText(_lStringHelp.get(x + 1));
                else
                    _txtAreaHelp.setText("Help for button " + _lButton.get(x + 1).getText() + " NYI! cf TODO Language Class");
            } else {
                _paneHelper.setVisible(false);
                for (Button a_lButton : _lButton)
                    a_lButton.setDisable(false);
            }
            if (_lButton.get(x).equals(_btnNext)) {
                _lButton.get(x).setDisable(false);
                _lButton.remove(x);
                _lStringHelp.remove(x);
            }

        }
    }
}