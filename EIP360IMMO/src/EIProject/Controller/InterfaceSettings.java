package EIProject.Controller;


import EIProject.Main;
import EIProject.Model.Plan;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class InterfaceSettings {

    static public InterfaceSettings instance;

    @FXML
    private ImageView _settingsFrench;

    @FXML
    private ImageView _settingsEnglish;

    @FXML
    private Label _restartLabel1;
    @FXML
    private Label _restartLabel2;

    @FXML
    private CheckBox _cbHelperState;

    @FXML
    private TextField _name;

    public void initialize() {
        if (instance == null)
            instance = this;
        _cbHelperState.setSelected(Main.getHelper());
        _cbHelperState.setOnAction(event -> {
            Main.setHelper(_cbHelperState.isSelected());
        });
        _name.setText(InterfaceMain.instance.get_plan().get_name());
    }

    public void chooseLanguage() {
        String lang = Main.getLanguage();

        if (lang.equals("fr")) {
            _settingsFrench.setImage(new Image(getClass().getResource("../view/Styles/ressources/FrenchIcon.png").toString()));
            _settingsEnglish.setImage(new Image(getClass().getResource("../view/Styles/ressources/EnglishIconGray.png").toString()));
        } else {
            _settingsEnglish.setImage(new Image(getClass().getResource("../view/Styles/ressources/EnglishIcon.png").toString()));
            _settingsFrench.setImage(new Image(getClass().getResource("../view/Styles/ressources/FrenchIconGray.png").toString()));
        }
    }

    @FXML
    public void setLanguageEnglish() {
        Main.setLanguage("en");
        _restartLabel1.setVisible(true);
        _restartLabel2.setVisible(true);
        _settingsEnglish.setImage(new Image(getClass().getResource("../view/Styles/ressources/EnglishIcon.png").toString()));
        _settingsFrench.setImage(new Image(getClass().getResource("../view/Styles/ressources/FrenchIconGray.png").toString()));
    }

    @FXML
    public void setLanguageFrench() {
        Main.setLanguage("fr");
        _restartLabel1.setVisible(true);
        _restartLabel2.setVisible(true);
        _settingsFrench.setImage(new Image(getClass().getResource("../view/Styles/ressources/FrenchIcon.png").toString()));
        _settingsEnglish.setImage(new Image(getClass().getResource("../view/Styles/ressources/EnglishIconGray.png").toString()));
    }

    @FXML
    public void confirmName() {
        String name = _name.getText();
        if (name != null && name != "") {
            InterfaceMain.instance.get_plan().set_name(name);
        }
    }
}
