package EIProject.Model;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;

/**
 * Created by Lucas on 30/09/2017.
 */

@XmlRootElement(name = "Plan")
@XmlAccessorType(XmlAccessType.FIELD)
public class Settings {

    @XmlElement(name = "language")
    private String _language;

    @XmlElement(name = "helper")
    private boolean _helper;

    @XmlElementWrapper(name = "recentlyOpenedFiles")
    @XmlElement(name = "recentFile")
    private ArrayList<String> _l_recentlyOpened;

    public Settings() {
        // needed for save/load
    }

    public String get_language() {
        return _language;
    }
    public void set_language(String _language) {
        this._language = _language;
    }

    public boolean get_helper() {
        return _helper;
    }
    public void set_helper(boolean _helper) {
        this._helper = _helper;
    }
    public ArrayList<String> get_l_recentlyOpened() {
        return _l_recentlyOpened;
    }
    public void set_l_recentlyOpened(ArrayList<String> _l_recentlyOpened) {
        this._l_recentlyOpened = _l_recentlyOpened;
    }
}
