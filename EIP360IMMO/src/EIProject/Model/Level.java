package EIProject.Model;

import EIProject.Controller.InterfaceMain;
import EIProject.Model.Graphic.AGraphic;
import EIProject.Model.Graphic.Numerisation.Room.ARoom;
import EIProject.Model.Graphic.Numerisation.Room.RoomPolygon;
import EIProject.Model.Graphic.Numerisation.Room.RoomRectangle;
import EIProject.Model.Graphic.Numerisation.Symbol.ASymbol;
import EIProject.Model.Graphic.Numerisation.Symbol.Door;
import EIProject.Model.Graphic.Numerisation.Symbol.Stairs;
import EIProject.Model.Graphic.VisiteVirtuelle.Link;
import EIProject.Model.Graphic.VisiteVirtuelle.Photo;
import javafx.scene.image.Image;

import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

import javax.xml.bind.annotation.*;
import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 15/06/2016.
 *
 * @author Lucas Burgat
 * @version 1.0
 *          Cette class représenté un étages d'une maison
 */

@XmlRootElement(name = "Level")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlSeeAlso({AGraphic.class, ARoom.class, RoomRectangle.class, RoomPolygon.class, ASymbol.class, Door.class, Stairs.class, Link.class, Photo.class})
public class Level implements java.io.Serializable {

    static int _s_id = 0;

    public int id;
    // global variables
    @XmlElement(name = "name")
    private String _name;

    @XmlElement(name = "picturePath")
    private String _picture;

    @XmlElementWrapper(name = "graphicalElements")
    @XmlElement(name = "AGraphic")
    private ArrayList<AGraphic> _l_item;

    @XmlElement(name = "sizeX")
    private double _sizeX;

    @XmlElement(name = "sizeY")
    private double _sizeY;

    public Level() {
        // needed for save/load
    }

    public Level(String Name, String Picture) {
        _name = Name;
        _picture = Picture;

        Image tmp;
        File tmp2 = new File(_picture);
        try {
            tmp = new Image(tmp2.toURI().toURL().toExternalForm());
        } catch (Exception e) {
            tmp = new Image(tmp2.getAbsolutePath());
        }
        _sizeX = tmp.getWidth();
        _sizeY = tmp.getHeight();

        _l_item = new ArrayList<>();

        id = _s_id;
        _s_id += 1;
    }

    public void drawLevel() {
        for (AGraphic g: _l_item)
            g.draw();
        frontOrder();
    }

    public void drawLevelMatthieuTest() {
        for (AGraphic g: _l_item) {
            g.drawMatthieuTest();
            if (g instanceof Photo) {
                Photo photop = (Photo)g;
                g.get_shape().setOnMouseClicked(photop.mouseHandler);
            }
        }
        frontOrder();
    }


    public void frontOrder() {
        for (AGraphic g: _l_item) {
            if (g instanceof Link) {
                g.toFront();
            }
        }
        for (AGraphic g: _l_item) {
            if (g instanceof ASymbol) {
                g.toFront();
            }
        }
    }

    public void createRoom(Rectangle rect) {
        RoomRectangle r = new RoomRectangle(rect);
        _l_item.add(r);
        frontOrder();
    }

    public void createRoom(ArrayList<Line> line) {
        RoomPolygon r = new RoomPolygon(line);
        _l_item.add(r);
        frontOrder();
    }

    public void generateLinkSelect(ArrayList<Photo> l_photo) {
        if (l_photo != null && l_photo.size() >= 2) {

            Iterator<Photo> i = l_photo.iterator();
            Photo p1 = i.next();
            while (i.hasNext()) {
                Photo p2 = i.next();

                Link l = Link.newLink(p1, p2, null,null);
                if (l != null)
                    _l_item.add(l);

                p1 = p2;
            }
        }
    }

    public void generateLinkAuto() {

        // Get all rooms from the current level
        ArrayList<ARoom> l_rooms = Plan.<AGraphic, ARoom>shortListByClass(_l_item, new RoomRectangle(), null);

        // Iterate through each room's picture and rooms and get all delta distance
        for (ARoom r: l_rooms) {
            for (Photo pic : r.get_l_photo()) {
                System.out.print("[CURR] " + pic.get_pId() + "  [" + pic.get_x() + "," + pic.get_y() + "]\n");
                SortedMap<Double, Photo> dist = new TreeMap<>();
                for (Photo each : r.get_l_photo()) {
                    if (pic != each) {
                        Double d = Math.sqrt(Math.pow((each.get_x() - pic.get_x()), 2) + Math.pow((each.get_y() - pic.get_y()), 2));
                            dist.put(d, each);
                    }
                }

                // Get the smallest one except the one tested
                if (dist.size() > 0) {
                    Photo nt = Collections.min(dist.entrySet(), new Comparator<Map.Entry<Double, Photo>>() {
                        @Override
                        public int compare(Map.Entry<Double, Photo> o1, Map.Entry<Double, Photo> o2) {
                            return o1.getKey().intValue() - o2.getKey().intValue();
                        }
                    }).getValue();

                    Link l = Link.newLink(pic, nt, null,null);
                    if (l != null)
                        _l_item.add(l);
                    dist.clear();
                }
            }
        }
    }

    //getters & setters
    public String get_name() {
        return _name;
    }

    public String get_picture() {
        return _picture;
    }

    public double get_sizeX() {
        return _sizeX;
    }

    public double get_sizeY() {
        return _sizeY;
    }

    public ArrayList<AGraphic> get_l_item() {
        return _l_item;
    }

    public static void set_s_id(int _s_id) {
        Level._s_id = _s_id;
    }
}

