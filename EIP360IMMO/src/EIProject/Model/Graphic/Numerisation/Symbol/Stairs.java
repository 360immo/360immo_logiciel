package EIProject.Model.Graphic.Numerisation.Symbol;

import EIProject.Controller.Inspector.StairInspector;
import EIProject.Controller.InterfaceMain;
import EIProject.Model.Graphic.AGraphic;
import EIProject.Model.Graphic.Numerisation.Room.ARoom;
import EIProject.Model.Level;
import EIProject.Model.Graphic.Numerisation.Room.RoomPolygon;
import EIProject.Model.Graphic.Numerisation.Room.RoomRectangle;
import EIProject.Model.Plan;
import EIProject.Model.Preview.LinkPreview;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;

import javax.xml.bind.annotation.*;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Lucas on 16/09/2016.
 */

@XmlRootElement(name = "Stairs")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Stairs extends ASymbol implements java.io.Serializable {
    @XmlElement(name = "stairsAnchorX")
    private double _anchorX;
    @XmlElement(name = "stairsAnchorY")
    private double _anchorY;
    @XmlElement(name = "stairsSaveAnchorX")
    private double _saveAnchorX;
    @XmlElement(name = "stairsSaveAnchorY")
    private double _saveAnchorY;

    @XmlElement(name = "stairsMouseEventX")
    private double _mouseEvent_x;
    @XmlElement(name = "stairsMouseEventY")
    private double _mouseEvent_y;

    private Stairs _linkedStairs;
    private Level _level;

    public Stairs() {
        // needed for save/load
    }

    public Stairs(ARoom room, Rectangle rect) {
        super();

        _created = createStairs(rect);

        _room = room;
        _shape.setAccessibleText("Symbol");

        _level = InterfaceMain.instance.get_plan().get_currLevel(); // Get level of the stair, need for preview

        if (_created) {
            room.get_l_symbol().add(this); //draw door

            InterfaceMain.instance.get_drawingPane().getChildren().add(_shape);
            setEvent();
        }

        StairInspector.add_stairs(this);
    }

    private void testMethode() {
        if (InterfaceMain.instance.get_plan().get_m_levels().size() > 1) {
            Level l = InterfaceMain.instance.get_plan().get_m_levels().get(1);
            List<Stairs> stairs = Plan.shortListByClass(l.get_l_item(), new Stairs(), null);
            if (stairs.size() > 0) {
                _linkedStairs = stairs.get(0);
                _linkedStairs.set_linkedStair(this);
            }
        }
    }

    private boolean createStairs(Rectangle rect) {
        _shape = new Rectangle(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
        _color = Color.rgb(220, 43, 103, 0.8);
        _shape.setFill(_color);
        _shape.setStroke(Color.rgb(42, 43, 44));
        _shape.setStrokeWidth(3);
        Image stairs = new Image("EIProject/view/Styles/ressources/StairIconSmall.png");
        _shape.setFill(new ImagePattern(stairs, 0, 0, 1, 1, true));
        return (true);
    }

    protected void clickEvent(MouseEvent e) {
        super.clickEvent(e);

        _mouseEvent_x = Plan.getNearest(e.getScreenX());
        _mouseEvent_y = Plan.getNearest(e.getScreenY());

        if (InterfaceMain.instance.get_curentTool() == InterfaceMain.Tools.LINKMODE) {
            if (InterfaceMain.instance.get_preview() instanceof LinkPreview) {
                ((LinkPreview) InterfaceMain.instance.get_preview()).clickStair(this);
            }
        }

        _saveAnchorX = 0.0;
        _saveAnchorY = 0.0;
    }

    protected void dragEvent(MouseEvent e) { //move stairs
        super.dragEvent(e);
    }

    //Event call when click is release
    protected void releaseEvent(MouseEvent e) { //check if correct
        super.releaseEvent(e);
    }

    public void move(double offsetX, double offsetY) {
        super.move(offsetX, offsetY);
    }

    public void draw() {
        super.draw();
    }

    public void remove() {
        super.remove();
        StairInspector.remove_stairs(this);
    }

    public void clean() {
        super.clean();
    }

    public double get_anchorX() {
        return _anchorX;
    }

    public double get_anchorY() {
        return _anchorY;
    }

    public ARoom get_room() {
        return _room;
    }

    @XmlTransient
    public Stairs get_linkedStair() {
        return _linkedStairs;
    }

    public void set_linkedStair(Stairs _linkedStair) {
        this._linkedStairs = _linkedStair;
    }

    public Level get_level() {
        return _level;
    }

    //Save / load stuff
    @XmlElement
    private double _saveShapeX;
    @XmlElement
    private double _saveShapeY;
    @XmlElement
    private double _saveShapeWidth;
    @XmlElement
    private double _saveShapeHeight;
    @XmlElement
    private int _linkedStairsID;
    @XmlElement
    private int _saveRoom;

    public double get_saveShapeX() {
        return _saveShapeX;
    }
    public double get_saveShapeY() {
        return _saveShapeY;
    }
    public double get_saveShapeWidth() {
        return _saveShapeWidth;
    }
    public double get_saveShapeHeight() {
        return _saveShapeHeight;
    }
    public int get_linkedStairsID() {
        return _linkedStairsID;
    }
    public int get_saveRoom() {
        return _saveRoom;
    }

    public void saveShape() {
        Rectangle tmp_shape = (Rectangle)_shape;
        _saveShapeX = tmp_shape.getX();
        _saveShapeY = tmp_shape.getY();
        _saveShapeWidth = tmp_shape.getWidth();
        _saveShapeHeight = tmp_shape.getHeight();
        _saveRoom = _room.get_id();
        if (_linkedStairs != null)
            _linkedStairsID = _linkedStairs.get_id();
        else
            _linkedStairsID = -1;
    }

    public void loadShape(HashMap<Integer, Level> items, Level lvl) {
        mouseHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (e.getEventType() == MouseEvent.MOUSE_ENTERED) {
                    hoverEnterEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_MOVED){
                    hoverMoveEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_EXITED) {
                    hoverExitEvent(e);
                }

                if (e.getEventType() == MouseEvent.MOUSE_PRESSED) {
                    select(e);
                    clickEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_DRAGGED) {
                    dragEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_RELEASED) {
                    releaseEvent(e);
                }
            }
        };
        _color = Color.rgb(220, 43, 103, 0.8);
        _shape = new Rectangle(_saveShapeX, _saveShapeY, _saveShapeWidth, _saveShapeHeight);
        _shape.setFill(_color);
        _shape.setStroke(Color.rgb(42, 43, 44));
        _shape.setStrokeWidth(3);
        Image stairs = new Image("EIProject/view/Styles/ressources/StairIconSmall.png");
        _shape.setFill(new ImagePattern(stairs, 0, 0, 1, 1, true));

        _shape.setOnMouseDragged(mouseHandler);
        _shape.setOnMousePressed(mouseHandler);
        _shape.setOnMouseReleased(mouseHandler);
        _shape.setOnMouseEntered(mouseHandler);
        _shape.setOnMouseMoved(mouseHandler);
        _shape.setOnMouseExited(mouseHandler);

        _level = lvl;

        Integer count = 0;
        Level tmp_level = items.get(count);
        while (tmp_level != null)
        {
            for (AGraphic tmp : tmp_level.get_l_item())
            {
                if (tmp instanceof Stairs && tmp.get_id() == _linkedStairsID)
                    _linkedStairs = (Stairs)tmp;
                if (tmp_level == lvl && tmp instanceof ARoom)
                {
                    if (tmp.get_id() == _saveRoom) {
                        ArrayList<ASymbol> edit = ((ARoom) tmp).get_l_symbol();
                        if (edit == null) {
                            edit = new ArrayList<>();
                        }
                        edit.add(this);
                        ((ARoom) tmp).set_l_symbol(edit);
                        _room = (ARoom)tmp;
                    }
                }
            }
            count += 1;
            tmp_level = items.get(count);
        }

        _shape.setAccessibleText("Symbol");
        StairInspector.add_stairs(this);
    }
}

