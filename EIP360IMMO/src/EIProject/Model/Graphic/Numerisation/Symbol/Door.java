package EIProject.Model.Graphic.Numerisation.Symbol;

import EIProject.Controller.InterfaceMain;
import EIProject.Model.Graphic.AGraphic;
import EIProject.Model.Graphic.Numerisation.Room.ARoom;
import EIProject.Model.Graphic.Numerisation.Room.RoomPolygon;
import EIProject.Model.Graphic.Numerisation.Room.RoomRectangle;
import EIProject.Model.Graphic.Numerisation.Symbol.ASymbol;
import EIProject.Model.Plan;
import EIProject.Model.Preview.LinkPreview;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.scene.transform.Rotate;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lucas on 16/09/2016.
 */

@XmlRootElement(name = "Door")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Door extends ASymbol implements java.io.Serializable {

    @XmlElement(name = "doorAnchorX")
    private double _anchorX;
    @XmlElement(name = "doorAnchorY")
    private double _anchorY;
    @XmlElement(name = "doorSaveAnchorX")
    private double _saveAnchorX;
    @XmlElement(name = "doorSaveAnchorY")
    private double _saveAnchorY;
    @XmlElement(name = "doorAngle")
    private double _angle;


    @XmlElement(name = "doorMouseEventX")
    private double _mouseEvent_x;
    @XmlElement(name = "doorMouseEventY")
    private double _mouseEvent_y;

    public Door() {
        // needed for save / load
    }

    public Door(ARoom room, Rectangle rect) {
        super();

        _created = createDoor(room, rect);

        _room = room;
        _shape.setAccessibleText("Symbol");


        if (_created) {
            room.get_l_symbol().add(this); //draw door

            InterfaceMain.instance.get_drawingPane().getChildren().add(_shape);
            setEvent();
        }
    }

    private boolean createDoor(ARoom room, Rectangle rect) {

        if (room.get_shape() instanceof Rectangle) {
            //Rectangle-shaped room door placement correction

            Rectangle frame = (Rectangle)room.get_shape();
            double deltaUp = rect.getY() - frame.getY();
            double deltaDown = (frame.getY() + frame.getHeight()) - rect.getY();
            double deltaLeft = rect.getX() - frame.getX();
            double deltaRight = (frame.getX() + frame.getWidth()) - rect.getX();

            if (deltaUp >= 0 && deltaUp <= deltaDown && deltaUp <= deltaLeft && deltaUp <= deltaRight)
            {
                rect.setY(rect.getY() - deltaUp - (_padding * 2));
                rect.setX(rect.getX() - _padding);
                rect.setRotate(90);
                _angle = 90;
            }
            else if (deltaDown >= 0 && deltaDown <= deltaUp && deltaDown <= deltaLeft && deltaDown <= deltaRight)
            {
                rect.setY(rect.getY() + deltaDown - (_padding * 2));
                rect.setX(rect.getX() - _padding);
                rect.setRotate(90);
                _angle = 90;

            }
            else if (deltaLeft >= 0 && deltaLeft <= deltaDown && deltaLeft <= deltaUp && deltaLeft <= deltaRight)
            {
                rect.setX(rect.getX() - deltaLeft - _padding);
                rect.setY(rect.getY() - (_padding * 2));
                rect.setRotate(0);
                _angle = 0;
            }
            else //deltaRight >= 0 && deltaRight <= deltaDown && deltaRight <= deltaLeft && deltaRight <= deltaUp
            {
                rect.setX(rect.getX() + deltaRight - _padding);
                rect.setY(rect.getY() - (_padding * 2));
                rect.setRotate(0);
                _angle = 0;
            }

        } else if (room.get_shape() instanceof Polygon) {
            rect = polygonDoorCorrection(((Polygon)room.get_shape()).getPoints(), rect);
        }

        // set anchor after calculation for further use
        _anchorX = rect.getX();
        _anchorY = rect.getY();

        // put correct shape to draw in _shape
        _shape = rect;//new Rectangle(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
        //_shape.setRotate(_angle);
        //_shape.getTransforms().add(new Rotate(_angle, _anchorX, _anchorY));


        // check if there is no collision
        if (!checkCollision(room.get_l_symbol(), (Rectangle)_shape)) {
            // draw door
            _color = Color.rgb(220, 103, 43, 0.8);
            _shape.setFill(_color);
            _shape.setStroke(Color.rgb(42, 43, 44));
            _shape.setStrokeWidth(3);
            return (true);
        }
        return (false);
    }

    private boolean checkCollision(ArrayList<ASymbol> doors, Rectangle chk) {
        boolean ret = false;
        Door c_door;

        chk.setX(chk.getX() + 5);
        chk.setY(chk.getY() + 5);
        chk.setWidth(chk.getWidth() - 5);
        chk.setHeight(chk.getHeight() - 5);

        for (ASymbol d : doors) {
            if (d instanceof Door) {
                c_door = (Door)d;
                if (c_door.get_shape() != _shape) {
                    if (c_door.get_shape().getBoundsInParent().intersects(chk.getBoundsInParent())) {
                        ret = true;
                    }
                }
            }
        }

        chk.setX(chk.getX() - 5);
        chk.setY(chk.getY() - 5);
        chk.setWidth(chk.getWidth() + 5);
        chk.setHeight(chk.getHeight() + 5);

        return (ret);
    }

    private Rectangle polygonDoorCorrection(List<Double> corners, Rectangle rect) {

        ArrayList<Double> angles = new ArrayList<>();
        ArrayList<Integer> padding = new ArrayList<>();
        ArrayList<Double> deltaX = new ArrayList<>();
        ArrayList<Double> deltaY = new ArrayList<>();
        int count = 0;
        double x1, y1, x2, y2, tmpAngle, a, b, tmpDelta;

        // do all the vectors of the room
        while ((count + 2) < corners.size()) {
            x1 = corners.get(count);
            y1 = corners.get(count + 1);
            x2 = corners.get(count + 2);
            y2 = corners.get(count + 3);

            boolean calc = true;
            a = 0;
            b = 0;

            if ((x2 - x1) == 0) {
                deltaX.add(x2 - rect.getX());
                deltaY.add(10000000.0);
                angles.add(0.0);
                calc = false;
            } else if (y2 - y1 == 0){
                a = 0;
                b = y1;
            } else {
                a = ((y2 - y1) / (x2 - x1));
                b = (y1 - (a * x1));
            }

            if (calc) {
                tmpDelta = ((rect.getY() - b) / a);
                if (((tmpDelta >= x1 && tmpDelta <= x2) || (tmpDelta >= x2 && tmpDelta <= x1))
                        && ((rect.getY() >= y1 && rect.getY() <= y2) || (rect.getY() >= y2 && rect.getY() <= y1))) {

                    deltaX.add(tmpDelta - rect.getX());

                } else {
                    deltaX.add(10000000.0);
                }

                tmpDelta = (a * rect.getX() + b);
                if (((rect.getX() >= x1 && rect.getX() <= x2) || (rect.getX() >= x2 && rect.getX() <= x1))
                        && ((tmpDelta >= y1 && tmpDelta <= y2) || (tmpDelta >= y2 && tmpDelta <= y1))) {

                    deltaY.add(tmpDelta - rect.getY());

                } else {
                    deltaY.add(10000000.0);
                }


                tmpAngle = angleCalculation(corners.get(count), corners.get(count + 1), corners.get(count + 2), corners.get(count + 3));
                angles.add(tmpAngle);
            }
            count += 2;
        }
        // do last vector
        x1 = corners.get(count);
        y1 = corners.get(count + 1);
        x2 = corners.get(0);
        y2 = corners.get(1);

        if ((x2 - x1) == 0) {
            a = x1;
            b = 0;
        } else if (y2 - y1 == 0){
            a = 0;
            b = y1;
        } else {
            a = ((y2 - y1) / (x2 - x1));
            b = (y1 - (a * x1));
        }

        tmpDelta = ((rect.getY() - b) / a);
        if (((tmpDelta >= x1 && tmpDelta <= x2) || (tmpDelta >= x2 && tmpDelta <= x1))
                && ((rect.getY() >= y1 && rect.getY() <= y2) || (rect.getY() >= y2 && rect.getY() <= y1))) {

            deltaX.add(tmpDelta - rect.getX());

        } else {
            deltaX.add(10000000.0);
        }

        tmpDelta = (a * rect.getX() + b);
        if (((rect.getX() >= x1 && rect.getX() <= x2) || (rect.getX() >= x2 && rect.getX() <= x1))
                && ((tmpDelta >= y1 && tmpDelta <= y2) || (tmpDelta >= y2 && tmpDelta <= y1))) {

            deltaY.add(tmpDelta - rect.getY());

        } else {
            deltaY.add(10000000.0);
        }


        tmpAngle = angleCalculation(corners.get(count), corners.get(count + 1), corners.get(0), corners.get(1));
        angles.add(tmpAngle);
        rect = selectCloser(deltaX, deltaY, angles, rect, padding);
        return (rect);
    }

    private Rectangle selectCloser(ArrayList<Double> deltaX, ArrayList<Double> deltaY, ArrayList<Double> angles, Rectangle rect, ArrayList<Integer> padding) {
        double lowestX, lowestY;
        int idxLowX, idxLowY;
        int count;

        count = 0;
        lowestX = 10000000.0;
        idxLowX = 0;
        while (count < deltaX.size()) {
            if (Math.abs(deltaX.get(count)) < Math.abs(lowestX)) {
                lowestX = deltaX.get(count);
                idxLowX = count;
            }
            count += 1;
        }

        count = 0;
        lowestY = 10000000.0;
        idxLowY = 0;
        while (count < deltaY.size()) {
            if (Math.abs(deltaY.get(count)) < Math.abs(lowestY)) {
                lowestY = deltaY.get(count);
                idxLowY = count;
            }
            count += 1;
        }


        if (Math.abs(deltaX.get(idxLowX)) <= Math.abs(deltaY.get(idxLowY))) {
            rect.setX(rect.getX() + deltaX.get(idxLowX) - (_padding));
            rect.setY(rect.getY() - (_padding * 2));
            rect.setRotate(angles.get(idxLowX));
            _angle = angles.get(idxLowX);
        } else {
            rect.setY(rect.getY() + deltaY.get(idxLowY) - (_padding * 2));
            rect.setX(rect.getX() - _padding);
            rect.setRotate(angles.get(idxLowY));
            _angle = angles.get(idxLowY);
        }

        return (rect);
    }

    private double angleCalculation(double x1, double y1, double x2, double y2) {
        double a, b, delta, tan;
        double angle = 1;

        if ((x2 - x1) == 0) {
            return (0);
        } else if (y2 - y1 == 0) {
            return (90);
        }

        if ((x1 <= x2 && y1 <= y2) || (x2 <= x1 && y2 <= y1)) {
            angle = -1;
        }

        a = (y2 - y1) / (x2 - x1);
        b = y1 - (a * x1);

        x2 = x1 + 10;
        y2 = a * x2 + b;

        delta = Math.sqrt(Math.pow((y2 - y1), 2));

        tan = 10.0 / delta;

        angle *= Math.toDegrees(Math.atan(tan));

        return (angle);
    }

    protected void clickEvent(MouseEvent e) {
        super.clickEvent(e);

        _mouseEvent_x = Plan.getNearest(e.getScreenX());
        _mouseEvent_y = Plan.getNearest(e.getScreenY());

        if (InterfaceMain.instance.get_curentTool() == InterfaceMain.Tools.LINKMODE) {
            if (InterfaceMain.instance.get_preview() instanceof LinkPreview) {
                ((LinkPreview) InterfaceMain.instance.get_preview()).clickDoor(this);
            }
        }

        _saveAnchorX = 0.0;
        _saveAnchorY = 0.0;
    }

    protected void dragEvent(MouseEvent e) { //move door
        super.dragEvent(e);
/*        double offsetX = Plan.getNearest(e.getX()) - _anchorX;
        double offsetY = Plan.getNearest(e.getY()) - _anchorY;

        _saveAnchorX -= offsetX;
        _saveAnchorY -= offsetY;

        move_door(offsetX, offsetY);

        _anchorX = Plan.getNearest(e.getX());
        _anchorY = Plan.getNearest(e.getY()); */
    }

    //Event call when click is release
    protected void releaseEvent(MouseEvent e) { //check if correct
        super.releaseEvent(e);
/*        if (checkCollision(_room.get_l_symbol()) == true) {
            move_door(_saveAnchorX, _saveAnchorY);
            _anchorX = ((Rectangle)(_shape)).getX();
            _anchorY = ((Rectangle)(_shape)).getY();
        } */
    }


    public void move(double offsetX, double offsetY) {
        super.move(offsetX, offsetY);
    }

    public void move_door(double offsetX, double offsetY) {
        Rectangle rect = (Rectangle) _shape;

        rect.setX(rect.getX() + offsetX);
        rect.setY(rect.getY() + offsetY);
    }

    public void draw() {
        super.draw();
    }

    public void remove() {
        super.remove();
    }

    public void clean() {
        super.clean();
    }

    public double get_anchorX() {
        return _anchorX;
    }

    public double get_anchorY() {
        return _anchorY;
    }

    public double get_angle() {
        return _angle;
    }

    public ARoom get_room() {
        return _room;
    }

    //Save / load stuff
    @XmlElement
    private double _saveShapeX;
    @XmlElement
    private double _saveShapeY;
    @XmlElement
    private double _saveShapeWidth;
    @XmlElement
    private double _saveShapeHeight;
    @XmlElement
    private int _saveRoom;

    public double get_saveShapeX() {
        return _saveShapeX;
    }
    public double get_saveShapeY() {
        return _saveShapeY;
    }
    public double get_saveShapeWidth() {
        return _saveShapeWidth;
    }
    public double get_saveShapeHeight() {
        return _saveShapeHeight;
    }
    public int get_saveRoom() {
        return _saveRoom;
    }

    public void saveShape() {
        Rectangle tmp_shape = (Rectangle)_shape;
        _saveShapeX = tmp_shape.getX();
        _saveShapeY = tmp_shape.getY();
        _saveShapeWidth = tmp_shape.getWidth();
        _saveShapeHeight = tmp_shape.getHeight();
        _saveRoom = _room.get_id();
    }

    public void loadShape(ArrayList<AGraphic> items) {
        mouseHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (e.getEventType() == MouseEvent.MOUSE_ENTERED) {
                    hoverEnterEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_MOVED){
                    hoverMoveEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_EXITED) {
                    hoverExitEvent(e);
                }

                if (e.getEventType() == MouseEvent.MOUSE_PRESSED) {
                    select(e);
                    clickEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_DRAGGED) {
                    dragEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_RELEASED) {
                    releaseEvent(e);
                }
            }
        };
        _color = Color.rgb(220, 103, 43, 0.8);
        _shape = new Rectangle(_saveShapeX, _saveShapeY, _saveShapeWidth, _saveShapeHeight);
        _shape.setFill(_color);
        _shape.setStroke(Color.rgb(42, 43, 44));
        _shape.setStrokeWidth(3);
        _shape.setRotate(_angle);

        _shape.setOnMouseDragged(mouseHandler);
        _shape.setOnMousePressed(mouseHandler);
        _shape.setOnMouseReleased(mouseHandler);
        _shape.setOnMouseEntered(mouseHandler);
        _shape.setOnMouseMoved(mouseHandler);
        _shape.setOnMouseExited(mouseHandler);


        for (AGraphic tmp : items) {
            if (tmp instanceof RoomRectangle || tmp instanceof RoomPolygon) {
                if (tmp.get_id() == _saveRoom) {
                    ArrayList<ASymbol> edit = ((ARoom) tmp).get_l_symbol();
                    if (edit == null) {
                        edit = new ArrayList<>();
                    }
                    edit.add(this);
                    ((ARoom) tmp).set_l_symbol(edit);
                    _room = (ARoom)tmp;
                }
            }
        }

        _shape.setAccessibleText("Symbol");
    }
}

