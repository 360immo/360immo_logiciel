package EIProject.Model.Graphic.Numerisation.Symbol;

import EIProject.Controller.InterfaceMain;
import EIProject.Model.Graphic.AGraphic;
import EIProject.Model.Graphic.Numerisation.Room.ARoom;
import EIProject.Model.Graphic.VisiteVirtuelle.Link;
import EIProject.Model.Plan;
import EIProject.Model.Preview.LinkPreview;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lucas on 16/09/2016.
 */

@XmlTransient
//@XmlSeeAlso({Door.class, Stairs.class})
public class ASymbol extends AGraphic implements java.io.Serializable {
    static private int _s_symbolId = 0;

    @XmlElement(name = "symbolID")
    private int _symbolId;
    @XmlElement(name = "symbolPadding")
    protected double _padding;
    @XmlElement(name = "created")
    protected boolean _created;

    protected ARoom _room;

    protected ArrayList<Link> _l_link;

    public ASymbol() {

        // needed for save/load
        _id = _s_countId;
        _s_countId++;

        _symbolId = _s_symbolId;
        _s_symbolId++;

        _padding = 12.5;
        _l_link = new ArrayList<>();
    }

    protected void clickEvent(MouseEvent e) {

    }

    protected void dragEvent(MouseEvent e) { //move symbol

    }

    //Event call when click is release
    protected void releaseEvent(MouseEvent e) {

    }

    public double getCenterX() {
        Rectangle rect = (Rectangle) _shape;
        return ((rect.getX() + rect.getWidth() / 2));
    }

    public double getCenterY() {
        Rectangle rect = (Rectangle) _shape;
        return ((rect.getY() + rect.getHeight() / 2));
    }

    public void move(double offsetX, double offsetY) {
        Rectangle rect = (Rectangle) _shape;

        rect.setX(rect.getX() + offsetX);
        rect.setY(rect.getY() + offsetY);
    }

    public void draw() {
        super.draw();
    }

    public void remove() {
        super.remove();
        if (_room.get_l_symbol().contains(this))
            _room.get_l_symbol().remove(this);
        ArrayList<Link> tmp = new ArrayList<>();
        tmp.addAll(_l_link);
        for (Link l: tmp) {
            l.remove();
        }
    }

    public void clean() {
        super.clean();
    }

    public static int get_s_symbolId() {
        return _s_symbolId;
    }

    public static void set_s_symbolId(int _s_symbolId) {
        ASymbol._s_symbolId = _s_symbolId;
    }

    public int get_sId() {
        return _symbolId;
    }

    public boolean is_created() {
        return _created;
    }

    /*public double get_padding() {
        return _padding;
    }*/

    public void set_padding(double _padding) {
        this._padding = _padding;
    }

    public void addLink(Link l) {
        if (!_l_link.contains(l))
            _l_link.add(l);
    }

    public void removeLink(Link l) {
        if (_l_link.contains(l))
            _l_link.remove(l);
    }
}

