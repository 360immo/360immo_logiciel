package EIProject.Model.Graphic.Numerisation.Room;


import EIProject.Controller.InterfaceMain;
import EIProject.Model.Graphic.AGraphic;
import EIProject.Model.Graphic.Numerisation.Symbol.ASymbol;
import EIProject.Model.Graphic.Numerisation.Symbol.Door;
import EIProject.Model.Graphic.Numerisation.Symbol.Stairs;
import EIProject.Model.Graphic.VisiteVirtuelle.Photo;
import EIProject.Model.Plan;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javax.xml.bind.annotation.*;

import java.util.ArrayList;

/**
 * 28/03/2017.
 *
 * @author Nicolas Legendre
 * @version 1.0
 *          Cette class abtraite représente une salle.
 *          Elle permet de géré les Photos et les Symboles poser dans les salles de façon generique,
 *          que la salle soit rectangulaire ou non
 */
@XmlTransient
//@XmlSeeAlso({RoomPolygon.class, RoomRectangle.class})
public abstract class ARoom extends AGraphic {

    static private Color[] _s_colorTab = {Color.rgb(220, 53, 34, 0.8)};//,
//            Color.rgb(30, 76, 220, 0.8),
//            Color.rgb(0, 220, 39, 0.8),
//            Color.rgb(220, 154, 12, 0.8),
//            Color.rgb(136, 0, 220, 0.8)};
    static private int _s_colorId = 0;

    private Circle _tmpCircle;

    //@XmlElementWrapper(name = "roomPhotos")
    //@XmlElement(name = "Photo")
    protected ArrayList<Photo> _l_photo;
    protected ArrayList<ASymbol> _l_symbol;

    @XmlElement(name = "meX")
    private double _me_x;
    @XmlElement(name = "meY")
    private double _me_y;
    @XmlElement(name = "saveOffsetX")
    private double _saveOffsetX;
    @XmlElement(name = "saveOffsetY")
    private double _saveOffsetY;

    private static Shape _inter;

    //Additional data
    private String _name;
    private String _description;
    private String _surface;

    protected ARoom() {
        _l_photo = new ArrayList<>();
        _l_symbol = new ArrayList<>();

        _tmpCircle = new Circle();
        _tmpCircle.setVisible(false);
        _tmpCircle.setRadius(10);
        _tmpCircle.setFill(Color.TRANSPARENT);
        _tmpCircle.setStroke(Color.rgb(42, 43, 44, 0.5));
        _tmpCircle.setStrokeWidth(3);
        _tmpCircle.setDisable(true);

        _name = "Room" + _s_countId;
    }

    protected void initColor() {
        _color = _s_colorTab[_s_colorId];
        _s_colorId = (_s_colorId + 1) % _s_colorTab.length;
    }

    public void testclickEvent(MouseEvent e) {
        clickEvent(e);
    }

    protected void clickEvent(MouseEvent e) {
        double zoom = InterfaceMain.instance.get_scrollValue() / 100.0;
        _me_x = Plan.getNearest(e.getScreenX() * zoom);
        _me_y = Plan.getNearest(e.getScreenY() * zoom);
        _saveOffsetX = _saveOffsetY = 0.f;

        if (!e.isControlDown() && e.getButton() != MouseButton.SECONDARY) {
            if (InterfaceMain.instance.get_curentTool() == InterfaceMain.Tools.PHOTOMODE) {
                addPhoto(e);
            } else if (InterfaceMain.instance.get_curentTool() == InterfaceMain.Tools.DOORMODE) {
                addDoor(e);
            } else if (InterfaceMain.instance.get_curentTool() == InterfaceMain.Tools.STAIRSMODE) {
                addStairs(e);
            }
        }
    }

    public void move(double offsetX, double offsetY) {
        for (Photo p : _l_photo) {
            p.move(offsetX, offsetY);
        }

        for (ASymbol s: _l_symbol) {
            s.move(offsetX, offsetY);
        }
    }

    protected void dragEvent(MouseEvent e) {
        double zoom = InterfaceMain.instance.get_scrollValue() / 100.0;
        double offsetX = Plan.getNearest(e.getScreenX() * zoom) - _me_x;
        double offsetY = Plan.getNearest(e.getScreenY() * zoom) - _me_y;

        _saveOffsetX -= offsetX;
        _saveOffsetY -= offsetY;

        InterfaceMain.instance.get_plan().moveRoomSelect(offsetX, offsetY);

        _me_x = Plan.getNearest(e.getScreenX() * zoom);
        _me_y = Plan.getNearest(e.getScreenY() * zoom);
    }

    protected void releaseEvent(MouseEvent e) {
        InterfaceMain.instance.get_plan().collideMoveCheck(_saveOffsetY, _saveOffsetX);
    }

    protected void hoverEnterEvent(MouseEvent e) {
        super.hoverEnterEvent(e);
        if (InterfaceMain.instance.get_curentTool() == InterfaceMain.Tools.PHOTOMODE) {
            _tmpCircle.setCenterX(Plan.getNearest(e.getX()));
            _tmpCircle.setCenterY(Plan.getNearest(e.getY()));
            _tmpCircle.setVisible(true);
            if (!InterfaceMain.instance.get_drawingPane().getChildren().contains(_tmpCircle))
                InterfaceMain.instance.get_drawingPane().getChildren().add(_tmpCircle);
        }
    }

    protected void hoverMoveEvent(MouseEvent e) {
        if (InterfaceMain.instance.get_curentTool() == InterfaceMain.Tools.PHOTOMODE) {
            _tmpCircle.setCenterX(Plan.getNearest(e.getX()));
            _tmpCircle.setCenterY(Plan.getNearest(e.getY()));
        }
    }

    protected void hoverExitEvent(MouseEvent e) {
        super.hoverExitEvent(e);
        if (InterfaceMain.instance.get_curentTool() == InterfaceMain.Tools.PHOTOMODE) {
            _tmpCircle.setVisible(false);
            if (InterfaceMain.instance.get_drawingPane().getChildren().contains(_tmpCircle))
                InterfaceMain.instance.get_drawingPane().getChildren().remove(_tmpCircle);
        }
    }

    /*Add Photo*/
    private void addPhoto(MouseEvent e) {
        Photo newPhoto = new Photo(this, new Circle(Plan.getNearest(e.getX()), Plan.getNearest(e.getY()), 10));
        _l_photo.add(newPhoto);
        InterfaceMain.instance.get_plan().get_currLevel().get_l_item().add(newPhoto);
    }

    /*Add Door*/
    private void addDoor(MouseEvent e) {
        ASymbol newDoor = new Door(this, new Rectangle(Plan.getNearest(e.getX()), Plan.getNearest(e.getY()), 25, 50));
        if (newDoor.is_created()) {
            InterfaceMain.instance.get_plan().get_currLevel().get_l_item().add(newDoor);
        }
    }

    /*Add Photo*/
    private void addStairs(MouseEvent e) {
        Stairs newStairs = new Stairs(this, new Rectangle(e.getX() - 12.5, e.getY() - 12.5, 25, 25));
        InterfaceMain.instance.get_plan().get_currLevel().get_l_item().add(newStairs);
    }

    public void draw() {
        super.draw();
        for (Photo p : _l_photo) {
            p.draw();
        }
        for (ASymbol s : _l_symbol) {
            s.draw();
        }
    }

    public void remove() {
        super.remove();

        if (InterfaceMain.instance.get_drawingPane().getChildren().contains(_tmpCircle))
            InterfaceMain.instance.get_drawingPane().getChildren().remove(_tmpCircle);

        ArrayList<Photo> tmp = new ArrayList<>();
        tmp.addAll(_l_photo);
        System.out.println(tmp.size());
        for (Photo p : tmp) {
            p.remove();
        }

        ArrayList<ASymbol> tmp2 = new ArrayList<>();
        tmp2.addAll(_l_symbol);
        for (ASymbol s : tmp2) {
            s.remove();
        }
    }

    public void clean() {
        super.clean();
        for (Photo p : _l_photo) {
            p.clean();
        }
        for (ASymbol s : _l_symbol) {
            s.clean();
        }
    }

    public static Boolean collide(ARoom room) {
        Boolean res = false;

        if (room.isInside()) {
            if (_inter != null)
                InterfaceMain.instance.get_drawingPane().getChildren().remove(_inter);
            for (Node n : InterfaceMain.instance.get_drawingPane().getChildren()) {
                if ((room.get_shape() != n)
                        && (n.getClass() == Rectangle.class || n.getClass() == Polygon.class)
                        && n.getAccessibleText() != "Symbol") {
                    Shape tmp = (Shape) n;

                    Shape intersect = Shape.intersect(room.get_shape(), tmp);

                    if (tmp.isVisible() && intersect.getBoundsInLocal().getWidth() > -1 && intersect.getBoundsInLocal().getHeight() > -1) {
                        res = true;
                    }
                }
            }
        } else {
            res = true;
        }

        return res;
    }

    public void error() {
        _shape.setFill(Color.web("red", 0.5));
    }

    public void exitError() {
        if (InterfaceMain.instance.get_l_select().contains(this)) {
            _shape.setFill(Color.WHITE);
            _shape.setStrokeWidth(3);
        } else
            _shape.setFill(_color);
    }

    public abstract Boolean isInside();

    public Circle get_tmpCircle() {
        return _tmpCircle;
    }

    public double get_me_x() {
        return _me_x;
    }

    public double get_me_y() {
        return _me_y;
    }

    public double get_saveOffsetX() {
        return _saveOffsetX;
    }

    public double get_saveOffsetY() {
        return _saveOffsetY;
    }

    public static Shape get_inter() {
        return _inter;
    }

    @XmlTransient
    public ArrayList<Photo> get_l_photo() {
        return _l_photo;
    }

    public void set_l_photo(ArrayList<Photo> _l_photo) {
        this._l_photo = _l_photo;
    }

    @XmlTransient
    public ArrayList<ASymbol> get_l_symbol() {
        return _l_symbol;
    }

    public void set_l_symbol(ArrayList<ASymbol> _l_symbol) {
        this._l_symbol = _l_symbol;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_description() {
        return _description;
    }

    public void set_description(String _description) {
        this._description = _description;
    }

    public String get_surface() {
        return _surface;
    }

    public void set_surface(String _surface) {
        this._surface = _surface;
    }
}
