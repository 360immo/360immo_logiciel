package EIProject.Model.Graphic.Numerisation.Room;

import EIProject.Controller.InterfaceMain;
import EIProject.Model.Level;
import EIProject.Model.Plan;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

import static javafx.scene.shape.StrokeType.INSIDE;

/**
 * 28/03/2017.
 *
 * @author Nicolas Legendre
 * @version 1.0
 *          Cette class représente une salle qui n'est pas de forme rectangulaire.
 */

@XmlRootElement(name = "RoomPolygon")
@XmlAccessorType(XmlAccessType.PROPERTY)
//@XmlType(propOrder={"_id", "_s_countId", "_color", "_shape", "_l_photo", "_l_symbol", "_me_x", "_me_y", "_saveOffsetX", "_saveOffsetY", "_inter"})
public class RoomPolygon extends ARoom {

    public RoomPolygon() {

    }

    public RoomPolygon(ArrayList<Line> l_lines) {
        _id = _s_countId;
        _s_countId++;

        initColor();

        Polygon poly = new Polygon();
        poly.setFill(_color);
        poly.setStroke(Color.rgb(42, 43, 44));
        poly.setStrokeWidth(3);
        poly.setStrokeType(INSIDE);

        for (Line l : l_lines) {
            poly.getPoints().add(l.getStartX());
            poly.getPoints().add(l.getStartY());
        }

        _shape = poly;

        InterfaceMain.instance.get_drawingPane().getChildren().add(_shape);
        setEvent();
    }

    public void clickEvent(MouseEvent e) {
        super.clickEvent(e);
    }

    protected void dragEvent(MouseEvent e) {
        super.dragEvent(e);
    }

    public void move(double offsetX, double offsetY) {
        int i = 0;

        for (Double d : ((Polygon) _shape).getPoints()) {

            if (i % 2 == 0) {
                d += offsetX;
            } else {
                d += offsetY;
            }

            ((Polygon) _shape).getPoints().set(i, d);
            i++;
        }

        super.move(offsetX, offsetY);
    }

    public Boolean isInside() {
        Boolean res = true;
        Level level = InterfaceMain.instance.get_plan().get_currLevel();
        int i = 0;

        for (Double d : ((Polygon) _shape).getPoints()) {

            if (i % 2 == 0) {
                if (d < 0 || d > level.get_sizeX())
                    res = false;
            } else {
                if (d < 0 || d > level.get_sizeY())
                    res = false;
            }

            ((Polygon) _shape).getPoints().set(i, d);
            i++;
        }
        return res;
    }


    //Save / load stuff
    @XmlElementWrapper(name = "shapePoints")
    @XmlElement (name = "shapePoint")
    private List<Double> _shapePoints;
    public List<Double> get_shapePoints() {
        return _shapePoints;
    }

    public void saveShape() {
        Polygon tmp_shape = (Polygon)_shape;
        _shapePoints = tmp_shape.getPoints();
    }

    public void loadShape() {
        mouseHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (e.getEventType() == MouseEvent.MOUSE_ENTERED) {
                    hoverEnterEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_MOVED){
                    hoverMoveEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_EXITED) {
                    hoverExitEvent(e);
                }

                if (e.getEventType() == MouseEvent.MOUSE_PRESSED) {
                    select(e);
                    clickEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_DRAGGED) {
                    dragEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_RELEASED) {
                    releaseEvent(e);
                }
            }
        };
        _color = Color.rgb(220, 53, 34, 0.8);
        _shape = new Polygon();
        for (Double d : _shapePoints) {
            ((Polygon)_shape).getPoints().add(d);
        }
        _shape.setFill(_color);
        _shape.setStroke(Color.rgb(42, 43, 44));
        _shape.setStrokeWidth(3);
        _shape.setStrokeType(INSIDE);

        _shape.setOnMouseDragged(mouseHandler);
        _shape.setOnMousePressed(mouseHandler);
        _shape.setOnMouseReleased(mouseHandler);
        _shape.setOnMouseEntered(mouseHandler);
        _shape.setOnMouseMoved(mouseHandler);
        _shape.setOnMouseExited(mouseHandler);
    }
}
