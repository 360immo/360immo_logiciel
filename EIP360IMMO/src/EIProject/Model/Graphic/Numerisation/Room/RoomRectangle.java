package EIProject.Model.Graphic.Numerisation.Room;

import EIProject.Controller.InterfaceMain;
import EIProject.Model.Graphic.Numerisation.Symbol.ASymbol;
import EIProject.Model.Graphic.VisiteVirtuelle.Photo;
import EIProject.Model.Level;
import EIProject.Model.Plan;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;

import static javafx.scene.shape.StrokeType.INSIDE;


/**
 * Created by Lucas on 15/09/2016.
 * @version 2.0
 * @implNote Dessine un Rectangle qui reprensente une salle de forme rectangulaire.
 */

@XmlRootElement(name = "RoomRectangle")
@XmlAccessorType(XmlAccessType.PROPERTY)
//@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(propOrder={"_id", "_s_countId", "_color", "_shape", "_l_photo", "_l_symbol", "_me_x", "_me_y", "_saveOffsetX", "_saveOffsetY", "_inter"})
public class RoomRectangle extends ARoom implements java.io.Serializable {

    @XmlElement(name = "roomName")
    public String name;
    @XmlElement(name = "roomDescription")
    public String description;
    @XmlElement(name = "roomSize")
    public String size;

    @XmlElement(name = "marge")
    private int _m = 8; //Marge event

    private Rectangle _tmpRect;

    @XmlElement(name = "roomResize")
    private boolean _resize;

    public RoomRectangle() {
        // needed for save/load
    }

    public RoomRectangle(Rectangle rect) {
        super();
        _id = _s_countId;
        _s_countId++;

        initColor();

        _shape = new Rectangle(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
        _shape.setFill(_color);
        _shape.setStroke(Color.rgb(42, 43, 44));
        _shape.setStrokeWidth(3);
        _shape.setStrokeType(INSIDE);

        _tmpRect = new Rectangle();

        InterfaceMain.instance.get_drawingPane().getChildren().add(_shape);
        setEvent();
    }

    protected void clickEvent(MouseEvent e) {
        super.clickEvent(e);

        if (isInResizeZone(e)) {
            initResize();
        }

        if (_resize && e.getButton() == MouseButton.SECONDARY)
            cancelResize();
    }

    protected void dragEvent(MouseEvent e) {
        if (_resize)
            resize(e);
        else
            super.dragEvent(e);
    }

    //Event call when click is release
    protected void releaseEvent(MouseEvent e) {
        if (_resize) {
            _resize = false;

            ArrayList<Photo> tmp = new ArrayList<>();
            tmp.addAll(_l_photo);
            for (Photo p: tmp) {
                if (!p.get_shape().getBoundsInLocal().intersects(_shape.getBoundsInLocal())) {
                    p.remove();
                }
            }

            ArrayList<ASymbol> tmp_symbol = new ArrayList<>();
            tmp_symbol.addAll(_l_symbol);
            for (ASymbol s: tmp_symbol) {
                if (!s.get_shape().getBoundsInLocal().intersects(_shape.getBoundsInLocal())) {
                    s.remove();
                }
            }
        }
        super.releaseEvent(e);
    }

    protected void hoverMoveEvent(MouseEvent e) {
        super.hoverMoveEvent(e);
        if (InterfaceMain.instance.get_curentTool() != InterfaceMain.Tools.PHOTOMODE) {
            _shape.setCursor(currentMouseState(e));
        }
    }

    public void move(double offsetX, double offsetY) {
        Rectangle rect = (Rectangle) _shape;

        rect.setX(rect.getX() + offsetX);
        rect.setY(rect.getY() + offsetY);

        super.move(offsetX, offsetY);
    }

    public Boolean isInside() {
        Boolean res = true;
        Rectangle rect = (Rectangle) _shape;
        Level level = InterfaceMain.instance.get_plan().get_currLevel();

        if (rect.getX() < 0 || (rect.getX() + rect.getWidth()) > level.get_sizeX()
                || rect.getY() < 0 || (rect.getY() + rect.getHeight()) > level.get_sizeY())
            res = false;

        return res;
    }


    /* Resizable rect */
    private void initResize() {
        _resize = true;

        _tmpRect.setX(((Rectangle)_shape).getX());
        _tmpRect.setY(((Rectangle)_shape).getY());
        _tmpRect.setWidth(((Rectangle)_shape).getWidth());
        _tmpRect.setHeight(((Rectangle)_shape).getHeight());

    }

    private void resize(MouseEvent e) {
        Rectangle rect = (Rectangle) _shape;

        double newX = nodeX();
        double newY = nodeY();
        double newH = rect.getHeight();
        double newW = rect.getWidth();

        double mX = Plan.getNearest(e.getX());
        double mY = Plan.getNearest(e.getY());

        Rectangle tmp = new Rectangle(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());

        if (_shape.getCursor() == Cursor.E_RESIZE || _shape.getCursor() == Cursor.NE_RESIZE || _shape.getCursor() == Cursor.SE_RESIZE) {
            newW = mX - nodeX();
        }
        // Left Resize
        if (_shape.getCursor() == Cursor.W_RESIZE || _shape.getCursor() == Cursor.NW_RESIZE || _shape.getCursor() == Cursor.SW_RESIZE) {
            newX = mX;
            newW = rect.getWidth() + nodeX() - newX;
        }

        // Bottom Resize
        if (_shape.getCursor() == Cursor.S_RESIZE || _shape.getCursor() == Cursor.SE_RESIZE || _shape.getCursor() == Cursor.SW_RESIZE) {
            newH = mY - nodeY();
        }
        // Top Resize
        if (_shape.getCursor() == Cursor.N_RESIZE || _shape.getCursor() == Cursor.NW_RESIZE || _shape.getCursor() == Cursor.NE_RESIZE) {
            newY = mY;
            newH = rect.getHeight() + nodeY() - newY;
        }

        double offsetX = newW - rect.getWidth();
        double offsetY = newH - rect.getHeight();

        if (newW > 0 && newH > 0) {
            rect.setX(newX);
            rect.setY(newY);
            rect.setWidth(newW);
            rect.setHeight(newH);

            if (collide(this)) {
                rect.setX(tmp.getX());
                rect.setY(tmp.getY());
                rect.setWidth(tmp.getWidth());
                rect.setHeight(tmp.getHeight());
            } else {
                for (ASymbol s: _l_symbol) {
                    s.move(offsetX, offsetY);
                }
            }
        }
    }

    private void cancelResize() {
        _resize = false;

        ((Rectangle)_shape).setX(_tmpRect.getX());
        ((Rectangle)_shape).setY(_tmpRect.getY());
        ((Rectangle)_shape).setWidth(_tmpRect.getWidth());
        ((Rectangle)_shape).setHeight(_tmpRect.getHeight());
    }

    private boolean isInResizeZone(MouseEvent event) {
        return isLeftResizeZone(event) || isRightResizeZone(event)
                || isBottomResizeZone(event) || isTopResizeZone(event);
    }

    private boolean isLeftResizeZone(MouseEvent event) {
        return intersect(nodeX(), event.getX());
    }

    private boolean isRightResizeZone(MouseEvent event) {
        return intersect(nodeW(), event.getX());
    }

    private boolean isTopResizeZone(MouseEvent event) {
        return intersect(nodeY(), event.getY());
    }

    private boolean isBottomResizeZone(MouseEvent event) {
        return intersect(nodeH(), event.getY());
    }

    private boolean intersect(double side, double point) {
        return side + _m > point && side - _m < point;
    }

    /*private double parentX(double localX) {
        return nodeX() + localX;
    }

    private double parentY(double localY) {
        return nodeY() + localY;
    }*/

    private double nodeX() {
        return ((Rectangle) _shape).getX();
    }

    private double nodeY() {
        return ((Rectangle) _shape).getY();
    }

    private double nodeW() {
        return ((Rectangle) _shape).getX() + ((Rectangle) _shape).getWidth();
    }

    private double nodeH() {
        return ((Rectangle) _shape).getY() + ((Rectangle) _shape).getHeight();
    }

    private Cursor currentMouseState(MouseEvent event) {
        Cursor c = Cursor.DEFAULT;
        boolean left = isLeftResizeZone(event);
        boolean right = isRightResizeZone(event);
        boolean top = isTopResizeZone(event);
        boolean bottom = isBottomResizeZone(event);

        if (left && top) c = Cursor.NW_RESIZE;
        else if (left && bottom) c = Cursor.SW_RESIZE;
        else if (right && top) c = Cursor.NE_RESIZE;
        else if (right && bottom) c = Cursor.SE_RESIZE;
        else if (right) c = Cursor.E_RESIZE;
        else if (left) c = Cursor.W_RESIZE;
        else if (top) c = Cursor.N_RESIZE;
        else if (bottom) c = Cursor.S_RESIZE;

        return c;
    }


    //Save / load stuff
    @XmlElement
    private double _saveShapeX;
    @XmlElement
    private double _saveShapeY;
    @XmlElement
    private double _saveShapeWidth;
    @XmlElement
    private double _saveShapeHeight;

    public double get_saveShapeX() {
        return _saveShapeX;
    }
    public double get_saveShapeY() {
        return _saveShapeY;
    }
    public double get_saveShapeWidth() {
        return _saveShapeWidth;
    }
    public double get_saveShapeHeight() {
        return _saveShapeHeight;
    }

    public void saveShape() {
        Rectangle tmp_shape = (Rectangle)_shape;
        _saveShapeX = tmp_shape.getX();
        _saveShapeY = tmp_shape.getY();
        _saveShapeWidth = tmp_shape.getWidth();
        _saveShapeHeight = tmp_shape.getHeight();
    }

    public void loadShape() {

        _color = Color.rgb(220, 53, 34, 0.8);
        _shape = new Rectangle(_saveShapeX, _saveShapeY, _saveShapeWidth, _saveShapeHeight);
        _shape.setFill(_color);
        _shape.setStroke(Color.rgb(42, 43, 44));
        _shape.setStrokeWidth(3);
        _shape.setStrokeType(INSIDE);

        _shape.setOnMouseDragged(mouseHandler);
        _shape.setOnMousePressed(mouseHandler);
        _shape.setOnMouseReleased(mouseHandler);
        _shape.setOnMouseEntered(mouseHandler);
        _shape.setOnMouseMoved(mouseHandler);
        _shape.setOnMouseExited(mouseHandler);

        _tmpRect = new Rectangle();

        mouseHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (e.getEventType() == MouseEvent.MOUSE_ENTERED) {
                    hoverEnterEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_MOVED){
                    hoverMoveEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_EXITED) {
                    hoverExitEvent(e);
                }

                if (e.getEventType() == MouseEvent.MOUSE_PRESSED) {
                    select(e);
                    clickEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_DRAGGED) {
                    dragEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_RELEASED) {
                    releaseEvent(e);
                }
            }
        };
    }
}

