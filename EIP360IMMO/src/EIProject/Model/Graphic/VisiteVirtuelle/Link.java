package EIProject.Model.Graphic.VisiteVirtuelle;

import EIProject.Controller.InterfaceMain;
import EIProject.Model.FXTools.Triangle;
import EIProject.Model.Graphic.AGraphic;
import EIProject.Model.Graphic.Numerisation.Symbol.ASymbol;
import EIProject.Model.Graphic.Numerisation.Symbol.Stairs;
import EIProject.Model.Level;
import EIProject.Model.Plan;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javax.xml.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 31/01/2017.
 *
 * @author Nicolas Legendre
 * @version 1.0
 *          Cette représente (visuelement) un lien entre deux photos
 */
@XmlRootElement(name = "Link")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Link extends AGraphic{

    /**
     * Cet enum permet de savoir la direction du lien.
     * MULTI : On peut passer par ce lien depuis les deux Photos.
     * ATOB : le link permet le passage depuis la Photo 1 vers la 2.
     * BTOA : inverse de ATOB
     */
    public enum Dir {MULTI, ATOB, BTOA};

    private Photo[] _l_photos;

    private ASymbol _symbol;

    private ASymbol _symbol2;

    private Line _line2;

    @XmlElement(name = "linkDirection")
    private Dir _direction;
    private Triangle[] _triangles = {null, null};

    public Link() {

    }

    public Link(Photo photo1, Photo photo2) {
        _id = _s_countId;
        _s_countId++;

        _l_photos = new Photo[2];
        _l_photos[0] = photo1;
        _l_photos[1] = photo2;

        photo1.addLink(this);
        photo2.addLink(this);

        Line line = new Line(_l_photos[0].get_x(), _l_photos[0].get_y(), _l_photos[1].get_x(), _l_photos[1].get_y());
        _color = Color.rgb(42, 43, 44);
        line.setFill(_color);
        line.setStroke(Color.rgb(42, 43, 44));
        line.setStrokeWidth(3);

        // Creation des triangle de directions
        _direction = Dir.MULTI;
        _triangles[0] = new Triangle(line, _direction);
        _triangles[0].setEvent(mouseHandler);
        _triangles[0].setFill(_color);

        _shape = line;

        InterfaceMain.instance.get_drawingPane().getChildren().add(_shape);
        InterfaceMain.instance.get_drawingPane().getChildren().add(_triangles[0]);

        _l_photos[0].toFront();
        _l_photos[1].toFront();

        _line2 = null;

        setEvent();
    }

    public Link(Photo photo1, Photo photo2, ASymbol symbol) {
        _id = _s_countId;
        _s_countId++;

        _l_photos = new Photo[2];
        _l_photos[0] = photo1;
        _l_photos[1] = photo2;
        _symbol = symbol;

        _symbol.addLink(this);

        photo1.addLink(this);
        photo2.addLink(this);

        _color = Color.rgb(42, 43, 44);

        Line line = new Line(_l_photos[0].get_x(), _l_photos[0].get_y(), _symbol.getCenterX(), _symbol.getCenterY());
        line.setFill(_color);
        line.setStroke(Color.rgb(42, 43, 44));
        line.setStrokeWidth(3);
        _shape = line;
        InterfaceMain.instance.get_drawingPane().getChildren().add(_shape);

        _line2 = new Line(_symbol.getCenterX(), _symbol.getCenterY(), _l_photos[1].get_x(), _l_photos[1].get_y());
        _line2.setFill(_color);
        _line2.setStroke(Color.rgb(42, 43, 44));
        _line2.setStrokeWidth(3);
        InterfaceMain.instance.get_drawingPane().getChildren().add(_line2);

        _direction = Dir.MULTI;
        _triangles[0] = new Triangle(line, _direction);
        _triangles[0].setEvent(mouseHandler);
        _triangles[0].setFill(_color);
        InterfaceMain.instance.get_drawingPane().getChildren().add(_triangles[0]);
        _triangles[1] = new Triangle(_line2, _direction);
        _triangles[1].setEvent(mouseHandler);
        _triangles[1].setFill(_color);
        InterfaceMain.instance.get_drawingPane().getChildren().add(_triangles[1]);

        _l_photos[0].toFront();
        _l_photos[1].toFront();
        _symbol.toFront();

        setEvent();

        _line2.setOnMousePressed(mouseHandler);
        _line2.setOnMouseEntered(mouseHandler);
        _line2.setOnMouseExited(mouseHandler);
    }

    public Link(Photo photo1, Photo photo2, ASymbol symbol, ASymbol symbol2) {
        _id = _s_countId;
        _s_countId++;

        _l_photos = new Photo[2];
        _l_photos[0] = photo1;
        _l_photos[1] = photo2;
        _symbol = symbol;
        _symbol2 =  symbol2;

        _symbol.addLink(this);
        _symbol2.addLink(this);

        photo1.addLink(this);
        photo2.addLink(this);

        _color = Color.rgb(42, 43, 44);

        Line line = new Line(_l_photos[0].get_x(), _l_photos[0].get_y(), _symbol.getCenterX(), _symbol.getCenterY());
        line.setFill(_color);
        line.setStroke(Color.rgb(42, 43, 44));
        line.setStrokeWidth(3);
        _shape = line;
        InterfaceMain.instance.get_drawingPane().getChildren().add(_shape);

        _line2 = new Line(_symbol2.getCenterX(), _symbol2.getCenterY(), _l_photos[1].get_x(), _l_photos[1].get_y());
        _line2.setFill(_color);
        _line2.setStroke(Color.rgb(42, 43, 44));
        _line2.setStrokeWidth(3);
        InterfaceMain.instance.get_drawingPane().getChildren().add(_line2);

        _l_photos[0].toFront();
        _l_photos[1].toFront();
        _symbol.toFront();

        setEvent();

        _line2.setOnMousePressed(mouseHandler);
        _line2.setOnMouseEntered(mouseHandler);
        _line2.setOnMouseExited(mouseHandler);
    }

    public static Link newLink(Photo p1, Photo p2, ASymbol symbol, ASymbol symbol2) {
        ArrayList<Link> l_link = Plan.shortListByClass(InterfaceMain.instance.get_plan().get_currLevel().get_l_item(), new Link(), null); // Changer cette methode pour recupéré les links

        if (l_link != null) {
            for (Link l : l_link) {
                if (l.isSame(p1, p2))
                    return null;
            }
        }

        Link res;
        if (symbol == null) {
            res =  new Link(p1, p2);
        } else if (symbol2 == null){
            res =  new Link(p1, p2, symbol);
        } else {
            res =  new Link(p1, p2, symbol, symbol2);
        }

        return res;
    }

    protected void clickEvent(MouseEvent e) {
        if (e.getClickCount() == 2) {
            if (_direction == Dir.MULTI)
                _direction = Dir.ATOB;
            else if (_direction == Dir.ATOB)
                _direction = Dir.BTOA;
            else
                _direction = Dir.MULTI;
            _triangles[0].updateDir(_direction);
            if (_triangles[1] != null)
                _triangles[1].updateDir(_direction);

        }
    }

    protected void dragEvent(MouseEvent e) {

    }

    protected void releaseEvent(MouseEvent e) {

    }

    void update() {
        Line l = (Line) _shape;

        if (_l_photos[1] != null) {
            if (_line2 == null) {
                l.setStartX(_l_photos[0].get_x());
                l.setStartY(_l_photos[0].get_y());
                l.setEndX(_l_photos[1].get_x());
                l.setEndY(_l_photos[1].get_y());
            } else {
                if (_symbol2 != null) {
                    l.setStartX(_l_photos[0].get_x());
                    l.setStartY(_l_photos[0].get_y());
                    l.setEndX(_symbol.getCenterX());
                    l.setEndY(_symbol.getCenterY());

                    _line2.setStartX(_symbol2.getCenterX());
                    _line2.setStartY(_symbol2.getCenterY());
                    _line2.setEndX(_l_photos[1].get_x());
                    _line2.setEndY(_l_photos[1].get_y());
                } else {
                    l.setStartX(_l_photos[0].get_x());
                    l.setStartY(_l_photos[0].get_y());
                    l.setEndX(_symbol.getCenterX());
                    l.setEndY(_symbol.getCenterY());

                    _line2.setStartX(_symbol.getCenterX());
                    _line2.setStartY(_symbol.getCenterY());
                    _line2.setEndX(_l_photos[1].get_x());
                    _line2.setEndY(_l_photos[1].get_y());
                }
            }

            _triangles[0].compute_transform(l);
            if (_triangles[1] != null)
                _triangles[1].compute_transform(_line2);

            if (_l_photos != null && _l_photos[0] != null && _l_photos[1] != null) {
                _l_photos[0].toFront();
                _l_photos[1].toFront();
            }
        }
    }

    public double get_orientation() {
        Point2D D, N = new Point2D(0,1);
        Line line = (Line) _shape;

        D = new Point2D(line.getEndX() - line.getStartX(), line.getEndY() - line.getStartY());

        double angle = N.angle(D);
        if (D.getX() > 0)
            angle *= -1;

        return angle;
    }

    public void draw() {
        if (_symbol != null && _symbol instanceof Stairs) {
            if (InterfaceMain.instance.get_drawingPane().getChildren().contains(_symbol.get_shape())) {
                if (!InterfaceMain.instance.get_drawingPane().getChildren().contains(_shape))
                    InterfaceMain.instance.get_drawingPane().getChildren().add(_shape);
            } else if (InterfaceMain.instance.get_drawingPane().getChildren().contains(_symbol2.get_shape())) {
                if (!InterfaceMain.instance.get_drawingPane().getChildren().contains(_line2))
                    InterfaceMain.instance.get_drawingPane().getChildren().add(_line2);
            }
        } else {
            super.draw();
            if (_line2 != null) {
                if (!InterfaceMain.instance.get_drawingPane().getChildren().contains(_line2))
                    InterfaceMain.instance.get_drawingPane().getChildren().add(_line2);
            }
        }
    }

    public void remove() {
        super.remove();
        if (_symbol2 != null && ((Stairs)_symbol2).get_level().get_l_item().contains(this)) {
            ((Stairs)_symbol2).get_level().get_l_item().remove(this);
        }
        _l_photos[0].removeLink(this);
        _l_photos[1].removeLink(this);
    }

    public void clean() {
        super.clean();
        if (_line2 != null) {
            if (InterfaceMain.instance.get_drawingPane().getChildren().contains(_line2))
                InterfaceMain.instance.get_drawingPane().getChildren().remove(_line2);
        }
        if (InterfaceMain.instance.get_drawingPane().getChildren().contains(_triangles[0]))
            InterfaceMain.instance.get_drawingPane().getChildren().remove(_triangles[0]);
        if (_triangles[1] != null) {
            if (InterfaceMain.instance.get_drawingPane().getChildren().contains(_triangles[1]))
                InterfaceMain.instance.get_drawingPane().getChildren().remove(_triangles[1]);
        }
    }

    private boolean isSame(Photo p1, Photo p2) {
        if (p1 == _l_photos[0] && p2 == _l_photos[1]
                || p1 == _l_photos[1] && p2 == _l_photos[0])
            return true;
        return false;
    }

    protected void select(MouseEvent e) {
        super.select(e);
        if (_line2 != null && e.getButton() == MouseButton.PRIMARY) {
            _line2.setStrokeWidth(5);
        }
    }

    public void unselect() {
        super.unselect();
        if (_line2 != null)
            _line2.setStrokeWidth(3);
    }

    protected void hoverEnterEvent(MouseEvent e) {
        _shape.setEffect(new DropShadow(20, _color));
        if (_line2 != null)
            _line2.setEffect(new DropShadow(20, _color));
    }

    protected void hoverExitEvent(MouseEvent e) {
        super.hoverExitEvent(e);
        if (_line2 != null)
            _line2.setEffect(null);
    }

    public void toFront() {
        super.toFront();
        if (_line2 != null) {
            _line2.toFront();
            _symbol.toFront();
        }
        if (_l_photos != null && _l_photos[0] != null)
            _l_photos[0].toFront();
        if (_l_photos != null && _l_photos[1] != null)
            _l_photos[1].toFront();
    }



    //Save / load stuff
    @XmlElement
    private double _saveShapeX1;
    @XmlElement
    private double _saveShapeY1;
    @XmlElement
    private double _saveShapeX2;
    @XmlElement
    private double _saveShapeY2;

    public double get_saveShapeX1() {
        return _saveShapeX1;
    }
    public double get_saveShapeY1() {
        return _saveShapeY1;
    }
    public double get_saveShapeX2() {
        return _saveShapeX2;
    }
    public double get_saveShapeY2() {
        return _saveShapeY2;
    }

    @XmlElement
    private boolean _isShape2;

    @XmlElement
    private double _saveShape2X1;
    @XmlElement
    private double _saveShape2Y1;
    @XmlElement
    private double _saveShape2X2;
    @XmlElement
    private double _saveShape2Y2;

    public double get_saveShape2X1() {
        return _saveShape2X1;
    }
    public double get_saveShape2Y1() {
        return _saveShape2Y1;
    }
    public double get_saveShape2X2() {
        return _saveShape2X2;
    }
    public double get_saveShape2Y2() {
        return _saveShape2Y2;
    }

    @XmlElement
    private int _savePhoto1;
    @XmlElement
    private int _savePhoto2;

    public int get_savePhoto1() {
        return _savePhoto1;
    }
    public int get_savePhoto2() {
        return _savePhoto2;
    }

    @XmlElement
    private int _saveSymbol1;
    @XmlElement
    private int _saveSymbol2;

    public int get_saveSymbol1() {
        return _saveSymbol1;
    }
    public int get_saveSymbol2() {
        return _saveSymbol2;
    }

    public void saveShape() {
        Line tmp_shape = (Line)_shape;
        _saveShapeX1 = tmp_shape.getStartX();
        _saveShapeY1 = tmp_shape.getStartY();
        _saveShapeX2 = tmp_shape.getEndX();
        _saveShapeY2 = tmp_shape.getEndY();

        if (_line2 == null) {
            _isShape2 = false;
        } else {
            _isShape2 = true;
            _saveShape2X1 = _line2.getStartX();
            _saveShape2Y1 = _line2.getStartY();
            _saveShape2X2 = _line2.getEndX();
            _saveShape2Y2 = _line2.getEndY();
        }

        _savePhoto1 = _l_photos[0].get_id();
        _savePhoto2 = _l_photos[1].get_id();

        if (_symbol != null)
            _saveSymbol1 = _symbol.get_id();
        else
            _saveSymbol1 = -1;
        if (_symbol2 != null)
            _saveSymbol2 = _symbol2.get_id();
        else
            _saveSymbol2 = -1;
    }

    public void loadShape(HashMap<Integer, Level> items, Level lvl) {
        mouseHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (e.getEventType() == MouseEvent.MOUSE_ENTERED) {
                    hoverEnterEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_MOVED){
                    hoverMoveEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_EXITED) {
                    hoverExitEvent(e);
                }

                if (e.getEventType() == MouseEvent.MOUSE_PRESSED) {
                    select(e);
                    clickEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_DRAGGED) {
                    dragEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_RELEASED) {
                    releaseEvent(e);
                }
            }
        };

        _shape = new Line(_saveShapeX1, _saveShapeY1, _saveShapeX2, _saveShapeY2);
        _color = Color.rgb(42, 43, 44);
        _shape.setFill(_color);
        _shape.setStroke(Color.rgb(42, 43, 44));
        _shape.setStrokeWidth(3);
        //InterfaceMain.instance.get_drawingPane().getChildren().add(_shape);

        _shape.setOnMouseDragged(mouseHandler);
        _shape.setOnMousePressed(mouseHandler);
        _shape.setOnMouseReleased(mouseHandler);
        _shape.setOnMouseEntered(mouseHandler);
        _shape.setOnMouseMoved(mouseHandler);
        _shape.setOnMouseExited(mouseHandler);

        _triangles[0] = new Triangle((Line)_shape, _direction);
        _triangles[0].setEvent(mouseHandler);
        _triangles[0].setFill(_color);
        //InterfaceMain.instance.get_drawingPane().getChildren().add(_triangles[0]);

        if (_isShape2) {
            _line2 = new Line(_saveShape2X1, _saveShape2Y1, _saveShape2X2, _saveShape2Y2);
            _line2.setFill(_color);
            _line2.setStroke(Color.rgb(42, 43, 44));
            _line2.setStrokeWidth(3);
            //InterfaceMain.instance.get_drawingPane().getChildren().add(_line2);

            _triangles[1] = new Triangle(_line2, _direction);
            _triangles[1].setEvent(mouseHandler);
            _triangles[1].setFill(_color);
            //InterfaceMain.instance.get_drawingPane().getChildren().add(_triangles[1]);

            _line2.setOnMousePressed(mouseHandler);
            _line2.setOnMouseEntered(mouseHandler);
            _line2.setOnMouseExited(mouseHandler);
        }

        _l_photos = new Photo[2];

        Integer count = 0;
        Level tmp_level = items.get(count);
        while (tmp_level != null) {
            for (AGraphic lp : tmp_level.get_l_item()) {
                if (lp instanceof Photo) {
                    if (((Photo) lp).get_id() == _savePhoto1) {
                        _l_photos[0] = (Photo) lp;
                        ArrayList<Link> edit = ((Photo) lp).get_l_link();
                        if (edit == null)
                            edit = new ArrayList<>();
                        edit.add(this);
                        ((Photo) lp).set_l_link(edit);
                    } else if (((Photo) lp).get_id() == _savePhoto2) {
                        _l_photos[1] = (Photo) lp;
                        ArrayList<Link> edit = ((Photo) lp).get_l_link();
                        if (edit == null)
                            edit = new ArrayList<>();
                        edit.add(this);
                        ((Photo) lp).set_l_link(edit);
                    }
                }
                if (lp instanceof ASymbol) {
                    if (((ASymbol) lp).get_id() == _saveSymbol1) {
                        _symbol = (ASymbol) lp;
                    } else if (((ASymbol) lp).get_id() == _saveSymbol2) {
                        _symbol2 = (ASymbol) lp;
                    }
                }
            }
            count += 1;
            tmp_level = items.get(count);
        }
    }

    @XmlTransient
    public Photo[] get_l_photos() {
        return _l_photos;
    }

    public void set_l_photos(Photo[] _l_photos) {
        this._l_photos = _l_photos;
    }

    public Dir get_direction() {
        return _direction;
    }

}
