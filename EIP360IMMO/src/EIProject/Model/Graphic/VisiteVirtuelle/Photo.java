package EIProject.Model.Graphic.VisiteVirtuelle;

import EIProject.Controller.InterfaceMain;
import EIProject.Model.FXTools.PhotoPreview;
import EIProject.Model.Graphic.AGraphic;
import EIProject.Model.Graphic.Numerisation.Room.ARoom;
import EIProject.Model.Graphic.Numerisation.Room.RoomPolygon;
import EIProject.Model.Graphic.Numerisation.Room.RoomRectangle;
import EIProject.Model.Plan;
import EIProject.Model.Preview.IPreview;
import EIProject.Model.Preview.LinkPreview;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.control.Cell;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Rotate;

import javax.xml.bind.annotation.*;

import java.io.File;
import java.util.ArrayList;

/**
 * 25/01/2017.
 *
 * @author Nicolas Legendre
 * @version 1.0
 *          Cette class représente un emplacement de photo 360°
 */
@XmlRootElement(name = "Photo")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class  Photo extends AGraphic{
    @XmlElement(name = "staticPhotoID")
    static private int _s_photoId = 0;

    private ARoom _room;

    @XmlElement(name = "photoID")
    private int _photoId;

    @XmlElement(name = "photoPath")
    private String _pathPhoto;
    @XmlElement(name = "photoName")
    private String _namePhoto;
    @XmlElement(name = "photoDescription")
    private String _descriptionPhoto;
    @XmlElement(name = "photoAutoRotation")
    private Boolean _autoRotPhoto;

    private Text _text;

    @XmlElement(name = "photoMeX")
    private double _me_x;
    @XmlElement(name = "photoMeY")
    private double _me_y;

    private ArrayList<Link> _l_link;
    private ArrayList<ToolTip> _l_tools;
    /**
     * Permet de connaitre l'orientation de la photo 0° = Nord
     */
    @XmlElement(name = "photoRotation")
    private Double _photoRotation = 0.0;
    private Line _orientationIndicator;


    @XmlElement(name = "pathPhotoDropped")
    private String pathPhotoDropped;

    private EventHandler<DragEvent> dragHandler = new EventHandler<DragEvent>() {
        public void handle(DragEvent event) {

            if (event.getEventType() == DragEvent.DRAG_ENTERED) {
                _shape.setFill(Color.rgb(42, 43, 44));
                _text.setFill(Color.rgb(255, 255, 255));
                _shape.setCursor(Cursor.CLOSED_HAND);
                event.acceptTransferModes(TransferMode.COPY);
            } else if (event.getEventType() == DragEvent.DRAG_EXITED) {
                _shape.setFill(_color);
                _text.setFill(Color.rgb(42, 43, 44));
            } else if (event.getEventType() == DragEvent.DRAG_DROPPED) {
                /* data dropped */
                /* if there is a string data on dragboard, stock it in string */
                boolean success = false;
                if (event.getDragboard().hasString()) {
                    pathPhotoDropped = event.getDragboard().getString();
                    _pathPhoto = pathPhotoDropped;
                    success = true;
                    _color = Color.rgb(4, 207, 0);
                    _shape.setFill(_color);
                    _text.setFill(Color.rgb(255, 255, 255));
                }
                /* let the source know whether the string was successfully
                 * transferred and used */
                event.setDropCompleted(success);
            } else if (event.getEventType() == DragEvent.DRAG_DONE) {
                /* the drag and drop gesture ended */
                /* if the data was successfully moved, clear it */
                if (event.getTransferMode() == TransferMode.COPY) {
                }
            } else if (event.getEventType() == DragEvent.DRAG_OVER) {
                event.acceptTransferModes(TransferMode.COPY);
            }
            event.consume();
        }
    };

    public Photo() {

    }

    public Photo(ARoom room, Circle c) {
        _pathPhoto = null;

        _id = _s_countId;
        _s_countId++;

        _s_photoId++;
        _photoId = _s_photoId;

        _namePhoto = "Photo_" + String.format("%03d", _photoId);
        _room = room;
        _autoRotPhoto = true;

        // Init Javafx Circle
        Circle tmp = new Circle();
        tmp.setCenterX(c.getCenterX());
        tmp.setCenterY(c.getCenterY());
        tmp.setRadius(10);
        _color = Color.rgb(217,203,158);
        tmp.setFill(_color);
        tmp.setStroke(Color.rgb(42, 43, 44));
        tmp.setStrokeWidth(3);

        // Init photo Id Display
        _text = new Text(c.getCenterX() - 6, c.getCenterY() + 5, _photoId + "");
        _text.setFill(Color.rgb(42, 43, 44));
        _text.setFont(Font.font("Verdana", 12));
        _text.setTextAlignment(TextAlignment.CENTER);
        _text.setDisable(true);

        //Init orientation line
        _orientationIndicator = new Line(c.getCenterX(), c.getCenterY(), c.getCenterX(), c.getCenterY() - 15);
        _orientationIndicator.setFill(Color.rgb(42, 43, 44));
        _orientationIndicator.getTransforms().add(0, new Rotate(_photoRotation, _orientationIndicator.getStartX(), _orientationIndicator.getStartY()));
        _orientationIndicator.setStrokeWidth(3);

        _shape = tmp;

        _l_link = new ArrayList<>();
        _l_tools = new ArrayList<>();

        // Add Shape to display list
        InterfaceMain.instance.get_drawingPane().getChildren().add(_orientationIndicator);
        InterfaceMain.instance.get_drawingPane().getChildren().add(_shape);
        InterfaceMain.instance.get_drawingPane().getChildren().add(_text);
        setEvent();

        //Init event
        _shape.setOnDragEntered(dragHandler);
        _shape.setOnDragExited(dragHandler);
        _shape.setOnDragDropped(dragHandler);
        _shape.setOnDragDone(dragHandler);
        _shape.setOnDragOver(dragHandler);

        if (InterfaceMain.instance.get_plan().get_firstPhoto() == null)
            InterfaceMain.instance.get_plan().set_firstPhoto(this);
    }

    protected void clickEvent(MouseEvent e) {
        _me_x = Plan.getNearest(e.getScreenX());
        _me_y = Plan.getNearest(e.getScreenY());

        if (InterfaceMain.instance.get_curentTool() == InterfaceMain.Tools.LINKMODE) {
            if (InterfaceMain.instance.get_preview() instanceof LinkPreview) {
                ((LinkPreview) InterfaceMain.instance.get_preview()).clickPhoto(this);
            }
        }
    }

    //permet de deplacer le marqueur photo
    private void move(MouseEvent e) {
        double offsetX = Plan.getNearest(e.getScreenX()) - _me_x;
        double offsetY = Plan.getNearest(e.getScreenY()) - _me_y;

        Circle circle = (Circle) _shape;

        circle.setCenterX(circle.getCenterX() + offsetX);
        circle.setCenterY(circle.getCenterY() + offsetY);

        if (!circle.getBoundsInLocal().intersects(_room.get_shape().getBoundsInLocal())) {
            circle.setCenterX(circle.getCenterX() - offsetX);
            circle.setCenterY(circle.getCenterY() - offsetY);
        }

        _orientationIndicator.getTransforms().remove(0);
        _orientationIndicator.setStartX(circle.getCenterX());
        _orientationIndicator.setStartY(circle.getCenterY());
        _orientationIndicator.setEndX(circle.getCenterX());
        _orientationIndicator.setEndY(circle.getCenterY() - 15);
        _orientationIndicator.getTransforms().add(0, new Rotate(_photoRotation, _orientationIndicator.getStartX(), _orientationIndicator.getStartY()));

        _text.setX(circle.getCenterX() - 6);
        _text.setY(circle.getCenterY() + 5);

        for (Link l: _l_link) {
            l.update();
        }

        _me_x = Plan.getNearest(e.getScreenX());
        _me_y = Plan.getNearest(e.getScreenY());
    }

    // Permet de rotater l'orientation de reference de la photo 360°
    public void rotate(MouseEvent e) {
        Circle circle = (Circle)_shape;

        Point2D normal = new Point2D(0.0, -1.0); // Vecteur normal
        Point2D vector = new Point2D(e.getScreenX() - circle.getCenterX(), e.getScreenY() - circle.getCenterY()); // Vecteur centre du cercle - Possition de la souris
        double rotation = normal.angle(vector);
        if (vector.getX() < 0)
            rotation *= -1;
        _photoRotation = rotation; // todo améliorer la rotation
        _orientationIndicator.getTransforms().remove(0);
        _orientationIndicator.getTransforms().add(0, new Rotate(_photoRotation, _orientationIndicator.getStartX(), _orientationIndicator.getStartY()));
    }

    protected void dragEvent(MouseEvent e) {
        if (e.isControlDown())
            rotate(e);
        else
            move(e);
    }

    protected void releaseEvent(MouseEvent e) {

    }

    public void draw() {
        if (!InterfaceMain.instance.get_drawingPane().getChildren().contains(_orientationIndicator))
            InterfaceMain.instance.get_drawingPane().getChildren().add(_orientationIndicator);
        if (!InterfaceMain.instance.get_drawingPane().getChildren().contains(_shape))
            InterfaceMain.instance.get_drawingPane().getChildren().add(_shape);
        if (!InterfaceMain.instance.get_drawingPane().getChildren().contains(_text))
            InterfaceMain.instance.get_drawingPane().getChildren().add(_text);
        if (_l_link == null) {
            _l_link = new ArrayList<>();
        }
        for (Link l : _l_link) {
            l.draw();
        }
    }

    public void remove() {
        super.remove();
        if (_room.get_l_photo().contains(this))
            _room.get_l_photo().remove(this);
        ArrayList<Link> tmp = new ArrayList<>();
        tmp.addAll(_l_link);
        for (Link l: tmp) {
            l.remove();
        }

        if (InterfaceMain.instance.get_plan().get_firstPhoto() == this)
            InterfaceMain.instance.get_plan().set_firstPhoto(null);
    }

    public void clean() {
        if (InterfaceMain.instance.get_drawingPane().getChildren().contains(_shape))
            InterfaceMain.instance.get_drawingPane().getChildren().remove(_shape);
        if (InterfaceMain.instance.get_drawingPane().getChildren().contains(_text))
            InterfaceMain.instance.get_drawingPane().getChildren().remove(_text);
        if (InterfaceMain.instance.get_drawingPane().getChildren().contains(_orientationIndicator))
            InterfaceMain.instance.get_drawingPane().getChildren().remove(_orientationIndicator);
        for (Link l: _l_link) {
            l.clean();
        }
    }

    public void move(double offsetX, double offsetY) {
        Circle circle = (Circle) _shape;

        circle.setCenterX(Plan.getNearest(circle.getCenterX() + offsetX));
        circle.setCenterY(Plan.getNearest(circle.getCenterY() + offsetY));

        _orientationIndicator.getTransforms().remove(0);
        _orientationIndicator.setStartX(circle.getCenterX());
        _orientationIndicator.setStartY(circle.getCenterY());
        _orientationIndicator.setEndX(circle.getCenterX());
        _orientationIndicator.setEndY(circle.getCenterY() - 15);
        _orientationIndicator.getTransforms().add(0, new Rotate(_photoRotation, _orientationIndicator.getStartX(), _orientationIndicator.getStartY()));

        _text.setX(_text.getX() + offsetX);
        _text.setY(_text.getY() + offsetY);

        for (Link l: _l_link) {
            l.update();
        }
    }

    void addLink(Link l) {
        if (!_l_link.contains(l))
            _l_link.add(l);
    }

    void removeLink(Link l) {
        if (_l_link.contains(l))
            _l_link.remove(l);
    }

    public ToolTip add_tools(Rotate rx, Rotate ry) {
        ToolTip t = new ToolTip(rx, ry);
        _l_tools.add(t);
        return t;
    }

    public void remove_tool(ToolTip todel) {
        if (_l_tools.contains(todel))
            _l_tools.remove(todel);
    }

    public ArrayList<ToolTip> get_l_tools() {
        return _l_tools;
    }

    public int get_pId() {
        return _photoId;
    }

    public void set_pId(int pId) {
        this._photoId = pId;
    }

    public double get_x() {
        Circle c = (Circle) _shape;
        return c.getCenterX();
    }

    public double get_y() {
        Circle c = (Circle) _shape;
        return c.getCenterY();
    }

    public void toFront() {
        _shape.toFront();
        _text.toFront();
    }

    static public void resetPhotoId() { _s_photoId = 0;}

    public static void set_s_photoId(int _s_photoId) {
        Photo._s_photoId = _s_photoId;
    }

    public String get_name() {
        return _namePhoto;
    }

    public void set_name(String _name) {
        this._namePhoto = _name;
    }

    public String get_path() {
        return _pathPhoto;
    }

    public void set_path(String path) {
        this._pathPhoto = path;
        _color = Color.rgb(4, 207, 0);
        _shape.setFill(_color);
        _text.setFill(Color.rgb(255, 255, 255));
    }

    public String get_description() {
        return _descriptionPhoto;
    }

    public void set_description(String _description) {
        this._descriptionPhoto = _description;
    }

    public ARoom get_room() {
        return _room;
    }

    public Boolean get_autoRot() {
        return _autoRotPhoto;
    }

    public void set_autoRot(Boolean _autoRot) {
        this._autoRotPhoto = _autoRot;
    }

    @XmlTransient
    public ArrayList<Link> get_l_link() {
        return _l_link;
    }

    public void set_l_link(ArrayList<Link> _l_link) {
        this._l_link = _l_link;
    }

    public Double get_photoRotation() {
        return _photoRotation;
    }

    //Save / load stuff
    @XmlElement
    private double _circleCenterX;
    @XmlElement
    private double _circleCenterY;

    public double get_circleCenterX() {
        return _circleCenterX;
    }
    public double get_cirleCenterY() {
        return _circleCenterY;
    }

    @XmlElement
    private int _saveRoom;
    public int get_saveRoom() {
        return _saveRoom;
    }

    public void saveShape() {
        Circle tmp_shape = (Circle)_shape;
        _circleCenterX = tmp_shape.getCenterX();
        _circleCenterY = tmp_shape.getCenterY();

        _saveRoom = _room.get_id();
    }

    public void loadShape(ArrayList<AGraphic> items) {
        /*mouseHandler = new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (e.getEventType() == MouseEvent.MOUSE_ENTERED) {
                    hoverEnterEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_MOVED){
                    hoverMoveEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_EXITED) {
                    hoverExitEvent(e);
                }

                if (e.getEventType() == MouseEvent.MOUSE_PRESSED) {
                    select(e);
                    clickEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_DRAGGED) {
                    dragEvent(e);
                } else if (e.getEventType() == MouseEvent.MOUSE_RELEASED) {
                    releaseEvent(e);
                }
            }
        };*/

        dragHandler = new EventHandler<DragEvent>() {
            public void handle(DragEvent event) {

                if (event.getEventType() == DragEvent.DRAG_ENTERED) {
                    _shape.setFill(Color.rgb(42, 43, 44));
                    _text.setFill(Color.rgb(255, 255, 255));
                    _shape.setCursor(Cursor.CLOSED_HAND);
                    event.acceptTransferModes(TransferMode.COPY);
                } else if (event.getEventType() == DragEvent.DRAG_EXITED) {
                    _shape.setFill(_color);
                    _text.setFill(Color.rgb(42, 43, 44));
                } else if (event.getEventType() == DragEvent.DRAG_DROPPED) {
                /* data dropped */
                /* if there is a string data on dragboard, stock it in string */
                    boolean success = false;
                    if (event.getDragboard().hasString()) {
                        pathPhotoDropped = event.getDragboard().getString();
                        _pathPhoto = pathPhotoDropped;
                        success = true;
                        _color = Color.rgb(4, 207, 0);
                        _shape.setFill(_color);
                        _text.setFill(Color.rgb(255, 255, 255));
                    }
                /* let the source know whether the string was successfully
                 * transferred and used */
                    event.setDropCompleted(success);
                } else if (event.getEventType() == DragEvent.DRAG_DONE) {
                /* the drag and drop gesture ended */
                /* if the data was successfully moved, clear it */
                    if (event.getTransferMode() == TransferMode.COPY) {
                    }
                } else if (event.getEventType() == DragEvent.DRAG_OVER) {
                    event.acceptTransferModes(TransferMode.COPY);
                }
                event.consume();
            }
        };

        Circle tmp = new Circle();
        tmp.setCenterX(_circleCenterX);
        tmp.setCenterY(_circleCenterY);
        tmp.setRadius(10);

        _orientationIndicator = new Line(tmp.getCenterX(), tmp.getCenterY(), tmp.getCenterX(), tmp.getCenterY() - 15);
        _orientationIndicator.setFill(Color.rgb(42, 43, 44));
        _orientationIndicator.getTransforms().add(0, new Rotate(_photoRotation, _orientationIndicator.getStartX(), _orientationIndicator.getStartY()));
        _orientationIndicator.setStrokeWidth(3);

        if (_pathPhoto != null)
            _color = Color.rgb(4, 207, 0);
        else
            _color = Color.rgb(217,203,158);
        tmp.setFill(_color);
        tmp.setStroke(Color.rgb(42, 43, 44));
        tmp.setStrokeWidth(3);
        _shape = tmp;

        _text = new Text(_circleCenterX - 6, _circleCenterY + 5, _photoId + "");
        _text.setFill(Color.rgb(42, 43, 44));
        _text.setFont(Font.font("Verdana", 12));
        _text.setTextAlignment(TextAlignment.CENTER);
        _text.setDisable(true);

        _shape.setOnMouseDragged(mouseHandler);
        _shape.setOnMousePressed(mouseHandler);
        _shape.setOnMouseReleased(mouseHandler);
        _shape.setOnMouseEntered(mouseHandler);
        _shape.setOnMouseMoved(mouseHandler);
        _shape.setOnMouseExited(mouseHandler);

        _shape.setOnDragEntered(dragHandler);
        _shape.setOnDragExited(dragHandler);
        _shape.setOnDragDropped(dragHandler);
        _shape.setOnDragDone(dragHandler);
        _shape.setOnDragOver(dragHandler);

        for (AGraphic lp : items) {
            if (lp instanceof RoomRectangle || lp instanceof RoomPolygon) {
                if (lp.get_id() == _saveRoom) {
                    ArrayList<Photo> edit = ((ARoom)lp).get_l_photo();
                    if (edit == null) {
                        edit = new ArrayList<>();
                    }
                    edit.add(this);
                    ((ARoom)lp).set_l_photo(edit);
                    _room = (ARoom)lp;
                }
            }
        }

        _l_link = new ArrayList<>();
    }



    private void changePhoto (){
        if (PhotoPreview.instancePhoto != null)
            PhotoPreview.instancePhoto.changePhoto(this);
    }


    public EventHandler<MouseEvent> mouseHandler = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent e) {
            if (e.getEventType() == MouseEvent.MOUSE_CLICKED) {
                changePhoto();
            }
        }
    };

}
