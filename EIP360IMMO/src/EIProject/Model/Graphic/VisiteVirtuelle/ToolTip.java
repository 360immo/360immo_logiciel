package EIProject.Model.Graphic.VisiteVirtuelle;

import EIProject.Model.Graphic.AGraphic;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;

public class ToolTip extends AGraphic {

    protected static int _s_cId = 0;

    private int _tId;

    private Photo _photos;

    private Rotate _rX;

    private Rotate _rY;

    private String _name;

    private String _description;

    private BorderPane _borderpane;
    private Text _textName;
    private Text _textDescription;

    public ToolTip() {

    }

    public ToolTip(Rotate x, Rotate y) {
        _rX = x;
        _rY = y;
        _tId = _s_cId;
        _s_cId++;
        _name = "ToolTips_" + _tId;
        _description = "Here the tooltip description";

        _textName = new Text();
        _textName.setText(_name);
        _textName.setStyle("-fx-font-size: 20;");
        _textName.setCache(true);

        _textDescription = new Text();
        _textDescription.setText(_description);
        _textDescription.setStyle("-fx-font-size: 15;");
        _textDescription.setCache(true);

        _borderpane = new BorderPane();
        _borderpane.setStyle("-fx-border-color: black;-fx-background-color: #ff7776;");
        _borderpane.setTop(_textName);
        _borderpane.setCenter(_textDescription);
        _borderpane.setVisible(false);

        _borderpane.getTransforms().addAll(_rX, _rY);
        _borderpane.getTransforms().add(new Translate(0,0,450));
    }

    @Override
    protected void clickEvent(MouseEvent e) {

    }

    @Override
    protected void dragEvent(MouseEvent e) {

    }

    @Override
    protected void releaseEvent(MouseEvent e) {

    }

    public void updateRotates(Rotate x, Rotate y) {
        _rX = x;
        _rY = y;
    }

    public String get_description() {
        return _description;
    }

    public void set_description(String _description) {
        this._description = _description;
        this._textDescription.setText(_description);
    }

    public Photo get_photos() {
        return _photos;
    }

    public Rotate get_rX() {
        return _rX;
    }

    public Rotate get_rY() {
        return _rY;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
        this._textName.setText(_name);
    }

    public BorderPane get_borderpane() {
        return _borderpane;
    }
}
