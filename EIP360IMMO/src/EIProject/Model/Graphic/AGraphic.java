package EIProject.Model.Graphic;

import EIProject.Controller.InspectorController;
import EIProject.Controller.InterfaceMain;
import EIProject.Model.Graphic.Numerisation.Symbol.ASymbol;
import EIProject.Model.Graphic.Numerisation.Symbol.Stairs;
import javafx.event.EventHandler;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Shape;
import javax.xml.bind.annotation.*;

/**
 * 01/02/2017.
 *
 * @author Nicolas Legendre
 * @version 1.0
 *          Class abstraite des éléments dessinés.
 *          Elle permet une gestion generique des input utilisateur sur le Shape
 *          Du dessin et de la supression d'éléments.
 */

//@XmlRootElement(name = "Plan")
//@XmlAccessorType(XmlAccessType.FIELD)
@XmlTransient
//@XmlSeeAlso({ARoom.class, ASymbol.class})
public abstract class AGraphic {
    @XmlElement (name = "countId")
    protected static int _s_countId = 0;

    public static void reset_s_countId() {
        _s_countId = 0;
    }

    @XmlElement (name = "id")
    protected int _id;

    protected Color _color;

    protected Shape _shape;

    protected EventHandler<MouseEvent> mouseHandler = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent e) {
            if (e.getEventType() == MouseEvent.MOUSE_ENTERED) {
                hoverEnterEvent(e);
            } else if (e.getEventType() == MouseEvent.MOUSE_MOVED){
                hoverMoveEvent(e);
            } else if (e.getEventType() == MouseEvent.MOUSE_EXITED) {
                hoverExitEvent(e);
            }

            if (e.getEventType() == MouseEvent.MOUSE_PRESSED) {
                select(e);
                clickEvent(e);
            } else if (e.getEventType() == MouseEvent.MOUSE_DRAGGED) {
                dragEvent(e);
            } else if (e.getEventType() == MouseEvent.MOUSE_RELEASED) {
                releaseEvent(e);
            }
        }
    };

    protected void setEvent() {
        _shape.setOnMouseDragged(mouseHandler);
        _shape.setOnMousePressed(mouseHandler);
        _shape.setOnMouseReleased(mouseHandler);
        _shape.setOnMouseEntered(mouseHandler);
        _shape.setOnMouseMoved(mouseHandler);
        _shape.setOnMouseExited(mouseHandler);
    }

    protected void select(MouseEvent e) {
        if (!e.isControlDown())
            InterfaceMain.instance.resetSelect();
        if (e.getButton() == MouseButton.PRIMARY) {
            InterfaceMain.instance.addToSelect(this);
            _shape.setFill(Color.WHITE);
            _shape.setStrokeWidth(3);
            InspectorController.instance.setInspecteurTarget(this);
        }
    }

    public void unselect() {
        _shape.setFill(_color);
        _shape.setStrokeWidth(3);
        if (this instanceof Stairs) {
            Image stairs = new Image("EIProject/view/Styles/ressources/StairIconSmall.png");
            _shape.setFill(new ImagePattern(stairs, 0, 0, 1, 1, true));
        }
        InspectorController.instance.close();
    }

    protected void hoverEnterEvent(MouseEvent e) {
       // _shape.setEffect(new DropShadow(20, _color));
        _shape.setEffect(new InnerShadow(20, _color));
    }

    protected void hoverMoveEvent(MouseEvent e) {

    }

    protected void hoverExitEvent(MouseEvent e) {
        _shape.setEffect(null);
    }

    protected abstract void clickEvent(MouseEvent e);
    protected abstract void dragEvent(MouseEvent e);
    protected abstract void releaseEvent(MouseEvent e);

    public void draw() {
        if (!InterfaceMain.instance.get_drawingPane().getChildren().contains(_shape) && _shape != null)
            InterfaceMain.instance.get_drawingPane().getChildren().add(_shape);
    }

    public void drawMatthieuTest() {
        if (!InterfaceMain.instance.getRoot().getChildren().contains(_shape) && _shape != null)
            InterfaceMain.instance.getRoot().getChildren().add(_shape);
    }


    public void remove() {
        if (InterfaceMain.instance.get_plan().get_currLevel().get_l_item().contains(this)) {
            clean();
            InterfaceMain.instance.get_plan().get_currLevel().get_l_item().remove(this);
        }
    }

    public void clean() {
        if (InterfaceMain.instance.get_drawingPane().getChildren().contains(_shape))
            InterfaceMain.instance.get_drawingPane().getChildren().remove(_shape);
    }

    public static int get_s_countId() {
        return _s_countId;
    }

    public static void set_s_countId(int _s_countId) {
        AGraphic._s_countId = _s_countId;
    }

    public int get_id() {
        return _id;
    }

    @XmlTransient
    public Shape get_shape() {
        return _shape;
    }

    @XmlTransient
    public Color get_color() {return _color; }

    public void set_color(Color newColor) {
        _color = new Color(newColor.getRed(), newColor.getGreen(), newColor.getBlue(), 0.8);
        _shape.setFill(_color);
    }

    public void set_shape(Shape _shape) {
        this._shape = _shape;
    }

    public void toFront() {
        _shape.toFront();
    }
}
