package EIProject.Model.Dao;

import EIProject.Model.SQL.User;
import EIProject.Model.SQL.Visit;
import javafx.collections.ObservableList;

public interface VisitDao {
    void create(Visit visit);
    Visit read(int id_Users, String name);
    void update(Visit visit, Visit existingVisit);
    boolean delete(Visit visit);
    ObservableList<Visit> readAll(User user);
    boolean updateVisible(Visit visit);
}
