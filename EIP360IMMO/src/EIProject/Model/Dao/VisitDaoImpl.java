package EIProject.Model.Dao;

import EIProject.Main;
import EIProject.Model.SQL.User;
import EIProject.Model.SQL.Visit;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;
import java.util.Calendar;

public class VisitDaoImpl implements VisitDao {
    @Override
    public void create(Visit visit) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = getConnexion();
            preparedStatement = connection.prepareStatement("INSERT INTO Visit (id_Users, Name, File, lastEdit) VALUES (?, ?, ?, ?)");
            preparedStatement.setInt(1, visit.getId_Users());
            preparedStatement.setString(2, visit.getName());
            preparedStatement.setString(3, visit.getFile());
            preparedStatement.setDate(4, new java.sql.Date(Calendar.getInstance().getTime().getTime()));
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            {
                try { if (preparedStatement != null) preparedStatement.close(); } catch (SQLException ignore) {}
                try { if (connection != null) connection.close(); } catch (SQLException ignore) {}
            }
        }

    }

    @Override
    public Visit read(int id_Users, String name) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Visit visit = null;
        try {
            connection = getConnexion();
            preparedStatement = connection.prepareStatement("SELECT id,Visible,View,File,lastEdit FROM Visit WHERE id_Users = ? AND Name = ?");
            preparedStatement.setInt(1, id_Users);
            preparedStatement.setString(2, name);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                visit = new Visit();
                visit.setid(resultSet.getInt(1));
                visit.setId_Users(id_Users);
                visit.setVisible(resultSet.getBoolean(2));
                visit.setView(resultSet.getInt(3));
                visit.setName(name);
                visit.setFile(resultSet.getString(4));
                visit.setLastEdit(resultSet.getDate(5));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            {
                try { if (resultSet != null) resultSet.close(); } catch (SQLException ignore) {}
                try { if (preparedStatement != null) preparedStatement.close(); } catch (SQLException ignore) {}
                try { if (connection != null) connection.close(); } catch (SQLException ignore) {}
            }
        }
        return visit;
    }

    @Override
    public void update(Visit visit, Visit existingVisit) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = getConnexion();
            preparedStatement = connection.prepareStatement("UPDATE Visit SET Name = ?, File = ?, lastEdit = ? WHERE id = ?");
            preparedStatement.setString(1, visit.getName());
            preparedStatement.setString(2, visit.getFile());
            preparedStatement.setDate(3, new java.sql.Date(Calendar.getInstance().getTime().getTime()));
            preparedStatement.setInt(4, existingVisit.getid());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            {
                try { if (preparedStatement != null) preparedStatement.close(); } catch (SQLException ignore) {}
                try { if (connection != null) connection.close(); } catch (SQLException ignore) {}
            }
        }
    }

    @Override
    public boolean delete(Visit visit) {
        boolean error = false;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = getConnexion();
            preparedStatement = connection.prepareStatement("DELETE FROM Visit WHERE id = ?");
            preparedStatement.setInt(1, visit.getid());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            error = true;
            e.printStackTrace();
        } finally {
            {
                try { if (preparedStatement != null) preparedStatement.close(); } catch (SQLException ignore) {}
                try { if (connection != null) connection.close(); } catch (SQLException ignore) {}
            }
        }
        return error;
    }

    @Override
    public ObservableList<Visit> readAll(User user) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ObservableList<Visit> visits = FXCollections.observableArrayList();
        try {
            connection = getConnexion();
            preparedStatement = connection.prepareStatement("SELECT id,id_Users,Visible,View,Name,File,lastEdit FROM Visit WHERE id_Users = ?");
            preparedStatement.setInt(1, user.get_id());
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Visit visit = new Visit();
                visit.setid(resultSet.getInt(1));
                visit.setId_Users(resultSet.getInt(2));
                visit.setVisible(resultSet.getBoolean(3));
                visit.setView(resultSet.getInt(4));
                visit.setName(resultSet.getString(5));
                visit.setFile(resultSet.getString(6));
                visit.setLastEdit(resultSet.getDate(7));
                visits.add(visit);
            }
        } catch (Exception e) {
            e.printStackTrace();
            visits = null;
        } finally {
            {
                try { if (resultSet != null) resultSet.close(); } catch (SQLException ignore) {}
                try { if (preparedStatement != null) preparedStatement.close(); } catch (SQLException ignore) {}
                try { if (connection != null) connection.close(); } catch (SQLException ignore) {}
            }
        }
        return visits;
    }

    @Override
    public boolean updateVisible(Visit visit) {
        boolean error = false;
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = getConnexion();
            preparedStatement = connection.prepareStatement("UPDATE Visit SET Visible = ?, lastEdit = ? WHERE id = ?");
            preparedStatement.setBoolean(1, !visit.getVisible());
            preparedStatement.setDate(2, new java.sql.Date(Calendar.getInstance().getTime().getTime()));
            preparedStatement.setInt(3, visit.getid());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            error = true;
            e.printStackTrace();
        } finally {
            {
                try { if (preparedStatement != null) preparedStatement.close(); } catch (SQLException ignore) {}
                try { if (connection != null) connection.close(); } catch (SQLException ignore) {}
            }
        }
        return error;
    }

    private Connection getConnexion() throws Exception {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String _urldb = "jdbc:mysql://" + Main.SQL_ADDR + ":" + Main.SQL_PORT + "/" + Main.SQL_DB;
        return DriverManager.getConnection(_urldb, Main.SQL_LOGIN, Main.SQL_PASS);
    }

}
