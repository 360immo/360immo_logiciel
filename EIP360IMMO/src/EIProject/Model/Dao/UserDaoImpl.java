package EIProject.Model.Dao;

import EIProject.Main;
import EIProject.Model.SQL.User;
import org.jasypt.util.text.StrongTextEncryptor;

import java.io.*;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class UserDaoImpl implements UserDao {
    @Override
    public boolean create(User user) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        boolean error = false;
        try {
            connection = getConnexion();
            preparedStatement = connection.prepareStatement("INSERT INTO Users (email,password,companyName,license,signUp,salt) VALUES (?, MD5(SHA1(MD5(?))), ?, ? , ?, ?)");
            preparedStatement.setString(1, user.get_email());
            preparedStatement.setString(2, user.get_password());
            preparedStatement.setString(3, user.get_companyName());
            preparedStatement.setDate(4, user.get_license());
            preparedStatement.setDate(5, new java.sql.Date(Calendar.getInstance().getTime().getTime()));
            preparedStatement.setString(6, "test");
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
            error = true;
        } finally {
                {
                    try { if (preparedStatement != null) preparedStatement.close(); } catch (SQLException ignore) {}
                    try { if (connection != null) connection.close(); } catch (SQLException ignore) {}
                }
        }
        return error;
    }

    @Override
    public boolean readUser(User user) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        boolean error = false;
        try {
            connection = getConnexion();
            preparedStatement = connection.prepareStatement("SELECT * FROM Users WHERE email = ?");
            preparedStatement.setString(1, user.get_email());
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                error = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            {
                try { if (resultSet != null) resultSet.close(); } catch (SQLException ignore) {}
                try { if (preparedStatement != null) preparedStatement.close(); } catch (SQLException ignore) {}
                try { if (connection != null) connection.close(); } catch (SQLException ignore) {}
            }
        }
        return error;
    }

    @Override
    public User login(String email, String password, boolean autoLogin) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        User user = null;
        try {
            connection = getConnexion();
            preparedStatement = connection.prepareStatement("SELECT id,email,password,companyName,license FROM Users WHERE email = ? AND password = MD5(SHA1(MD5(?)))");
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = new User();
                user.set_id(resultSet.getInt(1));
                user.set_email(resultSet.getString(2));
                user.set_password(resultSet.getString(3));
                user.set_companyName(resultSet.getString(4));
                user.set_license(resultSet.getDate(5));
                EncryptDataToFile(user.get_license(), user.get_email(), user.get_password(), autoLogin);
                user.set_date(DecryptDataToFile());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            {
                try { if (resultSet != null) resultSet.close(); } catch (SQLException ignore) {}
                try { if (preparedStatement != null) preparedStatement.close(); } catch (SQLException ignore) {}
                try { if (connection != null) connection.close(); } catch (SQLException ignore) {}
            }
        }
        return user;
    }

    @Override
    public User autoLogin(String email, String password) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        User user = null;
        try {
            connection = getConnexion();
            preparedStatement = connection.prepareStatement("SELECT id,email,password,companyName,license FROM Users WHERE email = ? AND password = ?");
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = new User();
                user.set_id(resultSet.getInt(1));
                user.set_email(resultSet.getString(2));
                user.set_password(resultSet.getString(3));
                user.set_companyName(resultSet.getString(4));
                user.set_license(resultSet.getDate(5));
                EncryptDataToFile(user.get_license(), user.get_email(), user.get_password(), true);
                user.set_date(DecryptDataToFile());
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            {
                try { if (resultSet != null) resultSet.close(); } catch (SQLException ignore) {}
                try { if (preparedStatement != null) preparedStatement.close(); } catch (SQLException ignore) {}
                try { if (connection != null) connection.close(); } catch (SQLException ignore) {}
            }
        }
        return user;
    }


    private Connection getConnexion() throws Exception {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String _urldb = "jdbc:mysql://" + Main.SQL_ADDR + ":" + Main.SQL_PORT + "/" + Main.SQL_DB;
        return DriverManager.getConnection(_urldb, Main.SQL_LOGIN, Main.SQL_PASS);
    }

    private void EncryptDataToFile(Date _date, String email, String password, boolean autoLogin) {
        try {
            DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(
                    new FileOutputStream(new File("license.lic" ))));
            StrongTextEncryptor encryptorlic = new StrongTextEncryptor();
            encryptorlic.setPassword("123");
            dos.writeUTF(encryptorlic.encrypt(_date.toString() + "@@@" + email + "@_@" + password + "@-@" + autoLogin));
            dos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private java.util.Date DecryptDataToFile() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date date = null;
        try {
            DataInputStream dis = new DataInputStream(new BufferedInputStream(
                    new FileInputStream(new File("License.lic"))));
            StrongTextEncryptor encryptorlic = new StrongTextEncryptor();
            encryptorlic.setPassword("123");
            String string = encryptorlic.decrypt(dis.readUTF());
            date = formatter.parse(string.substring(0, string.indexOf("@@@")));
            dis.close();
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return date;
    }
}
