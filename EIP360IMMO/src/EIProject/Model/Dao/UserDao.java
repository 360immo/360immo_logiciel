package EIProject.Model.Dao;

import EIProject.Model.SQL.User;

public interface UserDao {
    boolean create(User user);
    boolean readUser(User user);
    User login(String email, String password, boolean autoLogin);
    User autoLogin(String email, String password);
}
