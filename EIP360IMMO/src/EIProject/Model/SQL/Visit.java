package EIProject.Model.SQL;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class Visit {
    private int id;
    private int id_Users;
    private boolean Visible;
    private int View;
    private String Name;
    private String File;
    private Date lastEdit;

    public int getId_Users() {
        return id_Users;
    }

    public int getid() {
        return id;
    }

    public void setid(int id) {
        this.id = id;
    }

    public void setId_Users(int id_Users) {
        this.id_Users = id_Users;
    }

    public boolean getVisible() {
        return Visible;
    }

    public void setVisible(boolean visible) {
        Visible = visible;
    }

    public int getView() {
        return View;
    }

    public void setView(int view) {
        View = view;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getFile() {
        return File;
    }

    public void setFile(String file) {
        File = file;
    }

    public String getLastEdit() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        return formatter.format(lastEdit);
    }

    public void setLastEdit(Date lastEdit) {
        this.lastEdit = lastEdit;
    }
}
