package EIProject.Model.SQL;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

public class User {
    private int _id;
    private String _email;
    private String _password;
    private String _companyName;
    private Date _license;
    private Date _signUp;
    private java.util.Date _date;

    public User(String _email, String _password, String _companyName) {
        this._email = _email;
        this._password = _password;
        this._companyName = _companyName;
        this._license = java.sql.Date.valueOf(LocalDate.now().plusMonths(1).plusDays(1));
        this._signUp = java.sql.Date.valueOf(LocalDate.now());
    }

    public User() {
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_email() {
        return _email;
    }

    public void set_email(String _email) {
        this._email = _email;
    }

    public String get_password() {
        return _password;
    }

    public void set_password(String _password) {
        this._password = _password;
    }

    public String get_companyName() {
        return _companyName;
    }

    public void set_companyName(String _companyName) {
        this._companyName = _companyName;
    }

    public Date get_license() {
        return _license;
    }

    public void set_license(Date _license) {
        this._license = _license;
    }

    public Date get_signUp() {
        return _signUp;
    }

    public void set_signUp(Date _signUp) {
        this._signUp = _signUp;
    }

    public String get_date() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        return formatter.format(_date);
    }

    public void set_date(java.util.Date _date) {
        this._date = _date;
    }
}
