package EIProject.Model.Preview;

import EIProject.Controller.InterfaceMain;
import EIProject.Model.Level;
import EIProject.Model.Plan;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

import java.util.ArrayList;

/**
 * 27/03/2017.
 *
 * @author Nicolas Legendre
 * @version 1.0
 *          Cette class permets la création de salle Polygonale.
 *          Elle set les mousses event sur le _node passer en param.
 *          Elle gere la previsualisation et l'anulation de la création de salle.
 */
public class RoomPolyPreview implements IPreview {

    private Node _node;
    private ArrayList<Line> _l_line;
    private Line _previewLine;

    public RoomPolyPreview(Node node) {
        _node = node;

        _l_line = new ArrayList<>();
    }

    private void handleDrawMode(MouseEvent e) {
        double x = Plan.getNearest(e.getX());
        double y = Plan.getNearest(e.getY());

        if (e.getEventType() == MouseEvent.MOUSE_PRESSED) {
            if (e.getButton() == MouseButton.PRIMARY) {
                if (_previewLine == null) {
                    initPreview(x, y);
                } else {
                    if (!collide())
                        addLine();
                }

                if (_l_line.size() > 2) {
                    Line first = _l_line.get(0);
                    if (first.getStartX() == x && first.getStartY() == y) {
                        Level level = InterfaceMain.instance.get_plan().get_currLevel();
                        level.createRoom(_l_line);
                        clearLine();
                        cleanPreview();
                    }
                }

            } else if (e.getButton() == MouseButton.SECONDARY) {
                clearLine();
                cleanPreview();
            }
        } else if (e.getEventType() == MouseEvent.MOUSE_MOVED) {
            if (_previewLine != null) {
                _previewLine.setEndX(x);
                _previewLine.setEndY(y);
                if (collide()) {
                    _previewLine.setStroke(Color.web("red", 0.5));
                } else {
                    _previewLine.setStroke(Color.rgb(150, 150, 150, 1));
                }

            }

        }
    }

    private EventHandler<MouseEvent> mouseHandler = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent e) {
            if (e.getEventType() != MouseEvent.MOUSE_MOVED)
                InterfaceMain.instance.resetSelect();
            if (InterfaceMain.instance.get_curentTool() == InterfaceMain.Tools.DRAWPOLYMODE)
                handleDrawMode(e);
        }
    };

    private Boolean collide() {
        Boolean res = false;

        ArrayList<Node> _l_n = new ArrayList<>(InterfaceMain.instance.get_drawingPane().getChildren());

        for (Node n : _l_n) {
            if (!res && (_previewLine != n)
                    && (n.getClass() == Rectangle.class || n.getClass() == Polygon.class)
                    && n.getAccessibleText() != "Symbol") {
                Shape intersect = Shape.intersect((Shape) n, _previewLine);
                if (intersect.getBoundsInLocal().getWidth() > 15 && intersect.getBoundsInLocal().getHeight() > 15) {
                    res = true;
                }
            }
        }

        return res;
    }

    private void initPreview(double startX, double startY) {
        _previewLine = new Line(startX, startY, startX, startY);
        _previewLine.setFill(Color.rgb(42, 43, 44, 0.5));
        _previewLine.setStroke(Color.rgb(42, 43, 44, 0.5));
        _previewLine.setStrokeWidth(5);
        _previewLine.setDisable(true);

        if (!InterfaceMain.instance.get_drawingPane().getChildren().contains(_previewLine))
            InterfaceMain.instance.get_drawingPane().getChildren().add(_previewLine);
    }

    private void cleanPreview() {
        if (InterfaceMain.instance.get_drawingPane().getChildren().contains(_previewLine))
            InterfaceMain.instance.get_drawingPane().getChildren().remove(_previewLine);

        _previewLine = null;
    }

    private void addLine() {
        Line l = new Line(_previewLine.getStartX(), _previewLine.getStartY(),
                _previewLine.getEndX(), _previewLine.getEndY());
        l.setFill(Color.rgb(42, 43, 44, 0.5));
        l.setStroke(Color.rgb(150, 150, 150, 1));
        l.setStrokeWidth(5);

        l.setDisable(true);

        InterfaceMain.instance.get_drawingPane().getChildren().add(l);
        _l_line.add(l);

        _previewLine.setStartX(_previewLine.getEndX());
        _previewLine.setStartY(_previewLine.getEndY());
    }

    private void clearLine() {
        InterfaceMain.instance.get_drawingPane().getChildren().removeAll(_l_line);
        _l_line.clear();
    }

    public void launch() {
        _node.setOnMousePressed(mouseHandler);
        _node.setOnMouseMoved(mouseHandler);
    }

    public void stop() {
        _node.setOnMousePressed(null);
        _node.setOnMouseMoved(null);
        cleanPreview();
        clearLine();
    }
}
