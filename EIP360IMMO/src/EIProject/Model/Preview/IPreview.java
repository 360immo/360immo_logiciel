package EIProject.Model.Preview;

import javafx.scene.Node;

/**
 * 27/03/2017.
 *
 * @author Nicolas Legendre
 * @version 1.0
 *          Interface pour les classes de création / prévisualisation
 */
public interface IPreview {

    void launch();
    void stop();
}
