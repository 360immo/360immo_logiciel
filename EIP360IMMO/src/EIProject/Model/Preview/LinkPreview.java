package EIProject.Model.Preview;

import EIProject.Controller.InterfaceLevels;
import EIProject.Controller.InterfaceMain;
import EIProject.Model.Graphic.Numerisation.Symbol.ASymbol;
import EIProject.Model.Graphic.Numerisation.Symbol.Stairs;
import EIProject.Model.Graphic.VisiteVirtuelle.Link;
import EIProject.Model.Graphic.VisiteVirtuelle.Photo;
import EIProject.Model.Level;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;

/**
 * Created by nicol on 18/04/2017.
 * Permet de prévisualiser la création des liens
 */
public class  LinkPreview implements IPreview {
    private Node _node;

    // For preview drag link creation
    private Photo photo;
    private ASymbol symbol;
    private Line[] line;

    public Stairs getLinkedStair() {
        return linkedStair;
    }

    private Stairs linkedStair;

    public LinkPreview(Node node) {
        _node = node;
    }

    private void handleLinkMode(MouseEvent e) {
        if (photo != null) {
            if (e.getEventType() == MouseEvent.MOUSE_CLICKED && e.getButton() == MouseButton.SECONDARY) {
                cancel();
            }
            if (e.getEventType() == MouseEvent.MOUSE_MOVED || e.getEventType() == MouseEvent.MOUSE_DRAGGED) {
                if (symbol == null) {
                    line[0].setStartX(photo.get_x());
                    line[0].setStartY(photo.get_y());
                    line[0].setEndX(e.getX());
                    line[0].setEndY(e.getY());
                } else {
                    int lineId = 1;
                    if (linkedStair != null)
                        lineId = 2;
                    line[lineId].setEndX(e.getX());
                    line[lineId].setEndY(e.getY());
                }

            }
        }
    }

    private EventHandler<MouseEvent> mouseHandler = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent e) {
            if (InterfaceMain.instance.get_curentTool() == InterfaceMain.Tools.LINKMODE)
                handleLinkMode(e);
        }
    };

    public void launch() {
        photo = null;
        symbol = null;
        linkedStair = null;

        // init les line
        line = new Line[3];
        line[0] = new Line();
        line[0].setFill(Color.rgb(42, 43, 44, 0.5));
        line[0].setStroke(Color.rgb(42, 43, 44, 0.5));
        line[0].setStrokeWidth(3);
        line[0].setVisible(false);
        line[0].setDisable(true);

        line[1] = new Line();
        line[1].setFill(Color.rgb(42, 43, 44, 0.5));
        line[1].setStroke(Color.rgb(42, 43, 44, 0.5));
        line[1].setStrokeWidth(3);
        line[1].setVisible(false);
        line[1].setDisable(true);

        line[2] = new Line();
        line[2].setFill(Color.rgb(42, 43, 44, 0.5));
        line[2].setStroke(Color.rgb(42, 43, 44, 0.5));
        line[2].setStrokeWidth(3);
        line[2].setVisible(false);
        line[2].setDisable(true);

        if (!InterfaceMain.instance.get_drawingPane().getChildren().contains(line[0]))
            InterfaceMain.instance.get_drawingPane().getChildren().add(line[0]);
        if (!InterfaceMain.instance.get_drawingPane().getChildren().contains(line[1]))
            InterfaceMain.instance.get_drawingPane().getChildren().add(line[1]);

        _node.setOnMouseClicked(mouseHandler);
        _node.setOnMouseMoved(mouseHandler);
        _node.setOnMouseDragged(mouseHandler);
    }

    public void stop() {
        removeStairLine();
        photo = null;
        symbol = null;
        linkedStair = null;

        _node.setOnMouseClicked(null);
        _node.setOnMouseMoved(null);
        _node.setOnMouseDragged(null);

        if (InterfaceMain.instance.get_drawingPane().getChildren().contains(line[0]))
            InterfaceMain.instance.get_drawingPane().getChildren().remove(line[0]);
        if (InterfaceMain.instance.get_drawingPane().getChildren().contains(line[1]))
            InterfaceMain.instance.get_drawingPane().getChildren().remove(line[1]);
    }

    private void cancel() {
        removeStairLine();
        photo = null;
        symbol = null;
        linkedStair = null;
        line[0].setVisible(false);
        line[1].setVisible(false);
    }

    public void clickPhoto(Photo hit) {
        if (photo == null) {
            photo = hit;
            line[0].setStartX(hit.get_x());
            line[0].setStartY(hit.get_y());
            line[0].setEndX(hit.get_x());
            line[0].setEndY(hit.get_y());
            line[0].setVisible(true);
        } else {
            Link l;
            if (symbol instanceof Stairs) {
                l = Link.newLink(photo, hit, symbol, ((Stairs) symbol).get_linkedStair());
                InterfaceMain.instance.get_plan().get_currLevel().get_l_item().add(l);
                ((Stairs) symbol).get_linkedStair().get_level().get_l_item().add(l);
            } else {
                l = Link.newLink(photo, hit, symbol, null);
                if (l != null)
                    InterfaceMain.instance.get_plan().get_currLevel().get_l_item().add(l);
            }

            cancel();
        }
    }

    public void clickDoor(ASymbol hit) {
        if (photo != null) {
            Rectangle rect = (Rectangle) hit.get_shape();

            symbol = hit;

            line[0].setEndX(rect.getX() + rect.getWidth() / 2.0f);
            line[0].setEndY(rect.getY() + rect.getHeight() / 2.0f);

            line[1].setStartX(rect.getX() + rect.getWidth() / 2.0f);
            line[1].setStartY(rect.getY() + rect.getHeight() / 2.0f);
            line[1].setEndX(rect.getX());
            line[1].setEndY(rect.getY());
            line[1].setVisible(true);
        }
    }

    public void clickStair(Stairs hit) {
        if (photo != null && hit.get_linkedStair() != null) {
            symbol = hit;

            linkedStair = hit.get_linkedStair(); // Get linked Stair

            Rectangle rect = (Rectangle) linkedStair.get_shape();

            line[2].setStartX(rect.getX() + rect.getWidth() / 2.0f); // Draw Line
            line[2].setStartY(rect.getY() + rect.getHeight() / 2.0f);
            line[2].setEndX(rect.getX());
            line[2].setEndY(rect.getY());
            line[2].setVisible(true);

            InterfaceLevels.instance.changeLevel(linkedStair.get_level().id);
            if (!InterfaceMain.instance.get_drawingPane().getChildren().contains(line[2]))
                InterfaceMain.instance.get_drawingPane().getChildren().add(line[2]);
        }
    }

    private void removeStairLine() {
        if (linkedStair != null) {
            if (symbol instanceof Stairs) {
                if (InterfaceMain.instance.get_drawingPane().getChildren().contains(line[2]))
                    InterfaceMain.instance.get_drawingPane().getChildren().remove(line[2]);
                InterfaceLevels.instance.changeLevel(((Stairs)symbol).get_level().id);
                line[2].setVisible(false);
            }
        }
    }
}
