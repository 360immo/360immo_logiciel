package EIProject.Model.Preview;

import EIProject.Controller.InterfaceMain;
import EIProject.Model.Graphic.Numerisation.Room.ARoom;
import EIProject.Model.Level;
import EIProject.Model.Plan;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import org.w3c.dom.css.Rect;

import java.util.ArrayList;

/**
 * 27/03/2017.
 * @author Nicolas Legendre
 * @version 1.0
 * Cette class permets la création de salle Rectangulaire.
 * Elle set les mousses event sur le _node passer en param.
 * Elle gere la previsualisation et l'anulation de la création de salle.
 */
public class RoomRectPreview implements IPreview {

    private Node _node;
    private Rectangle _rect;
    private boolean _isColide;

    private double _startX;
    private double _startY;

    public RoomRectPreview(Node node) {
        _node = node;
    }

    private void handleDrawMode(MouseEvent e) {
        double e_x = Plan.getNearest(e.getX());
        double e_y = Plan.getNearest(e.getY());
        if (e.getEventType() == MouseEvent.MOUSE_PRESSED) {
            if (e.getButton() == MouseButton.PRIMARY) {
                _rect.setVisible(true);
                _rect.setX(e_x);
                _rect.setY(e_y);
                _rect.setWidth(0);
                _rect.setHeight(0);
                _isColide = false;
                _startX = e_x;
                _startY = e_y;
            } else if (e.getButton() == MouseButton.SECONDARY) {
                _rect.setWidth(0);
                _rect.setHeight(0);
                e.consume();
                _rect.setVisible(false);
            }
        } else if (e.getEventType() == MouseEvent.MOUSE_DRAGGED) {
            double w = e_x - _startX;
            double h = e_y - _startY;

            _rect.setWidth(Math.abs(w));
            _rect.setHeight(Math.abs(h));
            if (w <= 0) {
                _rect.setX(e_x);
            } else {
                _rect.setX(_startX);
            }
            if (h <= 0) {
                _rect.setY(e_y);
            } else {
                _rect.setY(_startY);
            }

            _isColide = collide();

            if (_isColide)
                _rect.setFill(Color.web("red", 0.5));
            else
                _rect.setFill(Color.rgb(42, 43, 44, 0.5));
        } else if (e.getEventType() == MouseEvent.MOUSE_RELEASED) {
            if (!_isColide && _rect.getWidth() > 24 && _rect.getHeight() > 24) {
                Level level = InterfaceMain.instance.get_plan().get_currLevel();
                level.createRoom(_rect);
                _rect.setWidth(0);
                _rect.setHeight(0);
            }
            _rect.setVisible(false);
        }
    }

    private EventHandler<MouseEvent> mouseHandler = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent e) {
            InterfaceMain.instance.resetSelect();
            if (InterfaceMain.instance.get_curentTool() == InterfaceMain.Tools.DRAWMODE)
                handleDrawMode(e);
        }
    };

    public void launch() {
        _rect = new Rectangle();
        _rect.setFill(Color.rgb(42, 43, 44, 0.5));
        _rect.setStroke(Color.rgb(42, 43, 44, 0.5));
        _rect.setStrokeWidth(3);
        if (!InterfaceMain.instance.get_drawingPane().getChildren().contains(_rect))
            InterfaceMain.instance.get_drawingPane().getChildren().add(_rect);
        _rect.setVisible(false);

        _node.setOnMouseDragged(mouseHandler);
        _node.setOnMousePressed(mouseHandler);
        _node.setOnMouseReleased(mouseHandler);
    }

    public void stop() {
        if (InterfaceMain.instance.get_drawingPane().getChildren().contains(_rect))
            InterfaceMain.instance.get_drawingPane().getChildren().remove(_rect);
        _node.setOnMouseDragged(null);
        _node.setOnMousePressed(null);
        _node.setOnMouseReleased(null);
    }

    private Boolean collide() {
        Boolean res = false;
        Level level = InterfaceMain.instance.get_plan().get_currLevel();
        ArrayList<Node> l_shape = new ArrayList(InterfaceMain.instance.get_drawingPane().getChildren());

        if (_rect.getX() < 0 || (_rect.getX() + _rect.getWidth()) > level.get_sizeX()
                || _rect.getY() < 0 || (_rect.getY() + _rect.getHeight()) > level.get_sizeY()) {
            res = true;
        } else {
            for (Node n : l_shape) {
                if ((_rect != n)
                        && (n.getClass() == Rectangle.class || n.getClass() == Polygon.class)
                        && n.getAccessibleText() != "Symbol") {
                    Shape intersect = Shape.intersect(_rect, (Shape) n);
                    if (intersect.getBoundsInLocal().getWidth() > 15 && intersect.getBoundsInLocal().getHeight() > 15) {
                        res = true;
                    }
                }
            }
        }
        return res;
    }
}
