package EIProject.Model.FXTools;

import EIProject.Model.Graphic.VisiteVirtuelle.Link;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polygon;

import static java.lang.Math.abs;

/**
 * Created by nicolas on 17/08/2017.
 * Class qui permet de dessiner un triangle (bout de fléche) dans un environement 2D
 */
public class Triangle extends Polygon {

    final Double[][] coordinates = {{-10.0,5.0,0.0, -6.0,10.0,5.0 },
                                   {-10.0,-6.0,0.0, 5.0,10.0,-6.0}};

    private Link.Dir _dir;

    // Position dans le monde 2D du triangle
    private Point2D _position;
    // Rotation dans le monde 2D du triangle
    private Double _rotation;

    public Triangle(Line ref, Link.Dir dir) {
        _dir = dir;
        drawDir();
        compute_transform(ref);
        apply_transform();
    }

    private void apply_transform() {
        // Positionement dans le monde
        setTranslateX(_position.getX());
        setTranslateY(_position.getY());

        // Rotation de la fleche dans le même sens que la ligne
        setRotate(_rotation);
    }

    // Set la position et la rotation du triangle au milieu de la ligne ref
    public void compute_transform(Line ref) {
        Point2D start = new Point2D(ref.getStartX(), ref.getStartY());
        Point2D end = new Point2D(ref.getEndX(), ref.getEndY());

        _position = new Point2D((abs(start.getX() + end.getX())) / 2.0, (abs(start.getY() + end.getY())) / 2.0 );

        // Calcule la rotation de du triangle
        Point2D normal = new Point2D(0.0, -1.0); // Vecteur normal
        Point2D vector = new Point2D(end.getX() - start.getX(), end.getY() - start.getY()); // Vector AB
        _rotation = normal.angle(vector);
        if (vector.getX() < 0)
            _rotation *= -1;

        apply_transform();
    }

    private void drawDir() {
        getPoints().clear();
        setVisible(true);
        if (_dir == Link.Dir.ATOB)
            getPoints().addAll(coordinates[0]);
        else if (_dir == Link.Dir.BTOA)
            getPoints().addAll(coordinates[1]);
        else
            setVisible(false);
    }

    public void setEvent(EventHandler<MouseEvent> event) {
        setOnMousePressed(event);
        setOnMouseEntered(event);
        setOnMouseExited(event);
    }

    public void updateDir(Link.Dir dir) {
        _dir = dir;
        drawDir();
    }
}
