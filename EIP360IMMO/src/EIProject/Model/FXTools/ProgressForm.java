package EIProject.Model.FXTools;

import javafx.concurrent.Task;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Created by nicol on 31/07/2017.
 */
public class ProgressForm {
    private final Stage dialogStage;
    private final ProgressBar pb = new ProgressBar();
    private final Text text = new Text();

    public ProgressForm() {
        dialogStage = new Stage();
        dialogStage.initStyle(StageStyle.UNDECORATED);
        dialogStage.setResizable(false);
        dialogStage.initModality(Modality.APPLICATION_MODAL);


        // PROGRESS BAR
        final Label label = new Label();
        label.setText("alerto");

        pb.setProgress(-1F);

        final VBox hb = new VBox();
        hb.getStyleClass().add("progress-hb");
        hb.setSpacing(5);
        hb.setAlignment(Pos.CENTER);
        hb.getChildren().addAll(text, pb);

        text.setFill(Color.WHITE);

        Scene scene = new Scene(hb);
        String cssPath = getClass().getResource("/EIProject/view/Styles/DarkTheme.css").toExternalForm();
        scene.getStylesheets().add(cssPath);
        dialogStage.setScene(scene);
    }

    public void activateProgressBar(final Task<?> task, String str)  {
        pb.progressProperty().bind(task.progressProperty());
        text.setText(str);
        dialogStage.show();
    }

    public Stage getDialogStage() {
        return dialogStage;
    }
}
