package EIProject.Model.FXTools;

import EIProject.Model.Graphic.VisiteVirtuelle.Link;
import EIProject.Model.Graphic.VisiteVirtuelle.Photo;
import EIProject.Model.Graphic.VisiteVirtuelle.ToolTip;
import javafx.animation.Interpolator;
import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.geometry.Point3D;
import javafx.geometry.Rectangle2D;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Circle;
import javafx.scene.shape.CullFace;
import javafx.scene.shape.Line;
import javafx.scene.shape.Sphere;
import javafx.scene.text.Text;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Translate;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

//Todo Ajout des metadata
//Todo support des link avec des symboles

/**
 * @author Legendre Nicolas
 * Inspiré du code de mathieux cherfils
 * Permet de visualisé une photo 360° dans le panel inspecteur
 */
public class PhotoPreview {
    static public PhotoPreview instancePhoto;

    public Stage get_photoStage() {
        return _photoStage;
    }

    private final Stage _photoStage;

    // display items
    private Photo _photo;
    private PhongMaterial _material;
    private Sphere _sphere;
    private PerspectiveCamera _camera;
    private Group _root3D;

    //Camera rotate settings
    private Rotate _rotateX = new Rotate(0, Rotate.X_AXIS);
    private Rotate _rotateY = new Rotate(0, Rotate.Y_AXIS);

    private double _baseX;
    private double _baseY;

    private Boolean _toolTips;
    private Circle _pToolTips;
    private Rotate _rToolX = new Rotate(0, Rotate.X_AXIS);
    private Rotate _rToolY = new Rotate(0, Rotate.Y_AXIS);
    private Translate _tTool = new Translate(0,0, 450);

    private boolean pressed = false;

    //Map des marqueurs
    private HashMap<Circle, Photo> _m_links;
    //Map des tooltips
    private HashMap<Circle, ToolTip> _m_tools;

    /**
     * @param photo la premiere photo afficher de la photo 360°
     */
    public PhotoPreview(Photo photo) {
        // Initialise une new fenetre

        instancePhoto = this;
        _photoStage = new Stage();


        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
        _photoStage.setX((primScreenBounds.getMinX() + primScreenBounds.getWidth()) - 1920);
        _photoStage.setY((primScreenBounds.getMinY() + primScreenBounds.getHeight()) - 600);
        _photoStage.setWidth(1120);
        _photoStage.setHeight(600);


        _photoStage.initStyle(StageStyle.UTILITY);
        _photoStage.setResizable(true);
        _photoStage.initModality(Modality.APPLICATION_MODAL);
        _photoStage.setTitle("Preview");

        // crée une nouvelle camera et la positione en (0,0,0)
        _camera = new PerspectiveCamera(true);
        _camera.setNearClip(0.1);
        _camera.setFarClip(10000.0);
        _camera.setFieldOfView(90);
        _camera.getTransforms().addAll(_rotateY, _rotateX);

        //Cree la sphere sur laquel sera display la photo
        _sphere = new Sphere(500);
        _sphere.setCullFace(CullFace.FRONT);
        _sphere.setScaleX(-1);

        // Crée le material de la sphere, qui prendra pour texture la photo
        _material = new PhongMaterial();

        // Crée un group d'objet FX 3D
        _root3D = new Group(_camera, _sphere, new AmbientLight(Color.WHITE));

        // Crée la scene qui sera ajouter a la fenettre
        Scene scene = new Scene(_root3D, 1280, 720, true, SceneAntialiasing.BALANCED);
        scene.setCamera(_camera);

        //put the 360 IMMO logo on top and bottom of the scene
        displayLogos();

        //Init map
        _m_links = new HashMap<>();
        _m_tools = new HashMap<>();

        //Load First room
        changePhoto(photo);

        //Load scene style
        String cssPath = getClass().getResource("/EIProject/view/Styles/DarkTheme.css").toExternalForm();
        scene.getStylesheets().add(cssPath);

        //Configure mouse and keyboard event
        scene.setOnKeyPressed(keyPressed);
        scene.setOnMouseDragged(mouseHandler);
        scene.setOnMousePressed(mouseHandler);
        scene.setOnMouseMoved(mouseHandler);

        //Load the scene in the stage
        _photoStage.setScene(scene);
        //Show stage in a new windows
        _photoStage.show();

        /*
        Setup tooltips data & preview system
         */
        _toolTips = false;
        _pToolTips = new Circle(15);
        _pToolTips.setFill(new Color(1.0,0,0,0.3));
        _pToolTips.setStroke(new Color(0,0,0,0.3));
        _pToolTips.setStrokeWidth(3);
        _pToolTips.setVisible(false);

        _pToolTips.getTransforms().addAll(_rToolX, _rToolY);
        _pToolTips.getTransforms().add(_tTool);

        _root3D.getChildren().add(_pToolTips);
    }

    private void displayLogos() {
        ImageView logoTop = new ImageView(new Image("file:src/360IMMO.png"));
        logoTop.setFitWidth(100);
        logoTop.setPreserveRatio(true);
        logoTop.setSmooth(true);
        logoTop.setCache(true);
        logoTop.setVisible(true);
        logoTop.getTransforms().addAll(new Rotate(90.f, new Point3D(1,0,0)), new Translate(-50, -50, 200));
        _root3D.getChildren().add(logoTop);
        ImageView logoBot = new ImageView(new Image("file:src/360IMMO.png"));
        logoBot.setFitWidth(100);
        logoBot.setPreserveRatio(true);
        logoBot.setSmooth(true);
        logoBot.setCache(true);
        logoBot.setVisible(true);
        logoBot.getTransforms().addAll(new Rotate(90.f, new Point3D(1,0,0)), new Translate(-50, -50, -200));
        _root3D.getChildren().add(logoBot);
    }

    private void setDifuseMap() {
        // update le material de la sphere, qui prendra pour texture la photo
        File img ;
        if (_photo.get_path() != null && !_photo.get_path().isEmpty()) {
            img = new File(_photo.get_path());
        } else {
            img = new File("src/photoError.jpg");
        }

        _material.setDiffuseMap(new Image(img.toURI().toString()));
        _sphere.setMaterial(_material);
        _sphere.setRotationAxis(new Point3D(0,1,0));
        _sphere.setRotate(_photo.get_photoRotation());

        //Reset la camera
        _camera.setRotate(0);
    }

    private Circle createMarqueur(Link link) {
        // Init le visuel du cercle
        Circle c = new Circle(15);
        c.setFill(Color.GRAY);
        c.setStroke(Color.BLACK);
        c.setStrokeWidth(3);

        //Calcule la position du cercle - dot product entre la Normal N et le vecteur Direction D
        Line line = (Line) link.get_shape();
        Point2D D, N = new Point2D(0,1);

        if (link.get_l_photos()[0] != _photo) {
            _m_links.put(c, link.get_l_photos()[0]);
            D = new Point2D(line.getEndX() - line.getStartX(), line.getEndY() - line.getStartY());
        }
        else {
            _m_links.put(c, link.get_l_photos()[1]);
            D = new Point2D(line.getStartX() - line.getEndX(), line.getStartY() - line.getEndY());
        }

        double angle = N.angle(D);
        if (D.getX() > 0)
            angle *= -1;
        //applique les transform
        c.getTransforms().add(new Rotate(angle, new Point3D(0,1,0)));
        c.getTransforms().add(new Translate(0,0, 450));

        //Set les mouse event du circle
        c.setOnMouseClicked(linkMouseEvent);
        c.setOnMouseEntered(linkMouseEvent);
        c.setOnMouseExited(linkMouseEvent);

        return c;
    }

    private void setMarqueur() {
        _root3D.getChildren().removeAll(_m_links.keySet());
        _m_links.clear();

        if (_photo.get_l_link() != null) {
            for (Link l : _photo.get_l_link()) {
                Circle c = createMarqueur(l);
                _root3D.getChildren().add(c);
            }
        }
    }

    public void changePhoto(Photo photo) {
        if (photo != null) {
            _photo = photo;

            setDifuseMap();
            setMarqueur();
            setToolTips();

            _rotateX.setAngle(0);
            _rotateY.setAngle(0);
        }
    }

    private void rotateCamera(double rX, double rY) {
//        _cameraXForm.setRotate(rX, rY, 0);
            _rotateX.setAngle(_rotateX.getAngle() + rX);
            _rotateY.setAngle(_rotateY.getAngle() + rY);
    }

    private void setToolTips() {
        for (ToolTip t: _m_tools.values()) {
            _root3D.getChildren().remove(t.get_borderpane());
        }
        _root3D.getChildren().removeAll(_m_tools.keySet());
        _m_tools.clear();

        ArrayList<ToolTip> tools = _photo.get_l_tools();
        if (tools != null) {
            for (ToolTip t : tools) {
                addToolToWorldSpace(t);
            }
        }
    }

    private void addToolToWorldSpace(ToolTip tool) {
        Circle c = new Circle(15);
        c.setFill(new Color(1.0,0,0,1));
        c.setStroke(new Color(0,0,0,1));
        c.setStrokeWidth(3);

        c.getTransforms().addAll(tool.get_rX(), tool.get_rY());
        c.getTransforms().add(new Translate(0,0,450));

        c.setOnMouseEntered(toolsMouseEvent);
        c.setOnMouseExited(toolsMouseEvent);
        c.setOnMouseClicked(toolsMouseEvent);

        _root3D.getChildren().add(tool.get_borderpane());
        _root3D.getChildren().add(c);

        _m_tools.put(c, tool);
    }

    private void openEditStage(ToolTip tool) {
        Stage toolStage = new Stage();

        toolStage.initStyle(StageStyle.UTILITY);
        toolStage.setResizable(true);
        toolStage.initModality(Modality.APPLICATION_MODAL);
        toolStage.setTitle(tool.get_name() + " edit");

        BorderPane panel = new BorderPane();
        panel.setStyle("-fx-border-color: black;-fx-background-color: #ff7776;");
        TextArea nameField = new TextArea(tool.get_name());
        TextArea descriptionField = new TextArea(tool.get_description());

        panel.setTop(nameField);
        panel.setCenter(descriptionField);

        nameField.textProperty().addListener((obs, oldText, newText) -> {
            tool.set_name(newText);
        });

        descriptionField.textProperty().addListener((obs, oldText, newText) -> {
            tool.set_description(newText);
        });

        Scene toolScene = new Scene(panel);
        toolStage.setScene(toolScene);

        toolStage.show();
    }

    private EventHandler<MouseEvent> toolsMouseEvent = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            Circle c = (Circle) event.getSource();
            ToolTip t = _m_tools.get(c);

            if (event.getEventType() == MouseEvent.MOUSE_ENTERED) {
                t.get_borderpane().setVisible(true);
            } else if (event.getEventType() == MouseEvent.MOUSE_EXITED) {
                t.get_borderpane().setVisible(false);
            } else if (event.getEventType() == MouseEvent.MOUSE_CLICKED) {
                if (event.getClickCount() == 2) {
                    openEditStage(t);
                }
            }
        }
    };

    //Ensemble d'event clavier
    private EventHandler<MouseEvent> linkMouseEvent = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            if (event.getSource() instanceof Circle) {
                Circle c = (Circle) event.getSource();
                if (event.getEventType() == MouseEvent.MOUSE_CLICKED) {
                    changePhoto(_m_links.get(c));
                } else if (event.getEventType() == MouseEvent.MOUSE_ENTERED) {
                    c.setFill(Color.WHITE);
                } else if (event.getEventType() == MouseEvent.MOUSE_EXITED) {
                    c.setFill(Color.GRAY);
                }
            }
        }
    };

    //Ensemble d'event clavier
    private EventHandler<KeyEvent> keyPressed = new EventHandler<KeyEvent>() {
        @Override
        public void handle(KeyEvent event) {
            if (event.getEventType() == KeyEvent.KEY_PRESSED) {
                int x = 0, y = 0;

                if (event.getCode().equals(KeyCode.UP)) {
                    x += 5.0f;
                }
                if (event.getCode().equals(KeyCode.DOWN)){
                    x -= 5.0f;
                }
                if (event.getCode().equals(KeyCode.RIGHT)) {
                    y += 5.0f;
                }
                if (event.getCode().equals(KeyCode.LEFT)) {
                    y -= 5.0f;
                }
                if (event.getCode().equals(KeyCode.T)) {
                    _toolTips = !_toolTips;
                    _pToolTips.setVisible(_toolTips);
                }
                rotateCamera(x, y);
            }
        }
    };

    private void toolPreview(MouseEvent event) {
        PickResult pr = event.getPickResult();
        Point3D res =pr.getIntersectedPoint();
        res.normalize();

        _tTool.setZ(0);

        double angleX = _rotateX.getAngle();
        double ref = _photoStage.getScene().getHeight() / 2;
        double sceneY = event.getSceneY() - ref;
        double ratio = sceneY / ref;
        ratio *= -1;
        angleX = (angleX + 40 * ratio);
        _rToolX.setAngle(angleX);

        Point2D N = new Point2D(0,1);
        Point2D D = new Point2D(res.getX(),res.getZ());
        D.normalize();
        double angle = N.angle(D);
        if (D.getX() > 0)
            angle *= -1;

        _rToolY.setAngle(angle);

        _tTool.setZ(450);
    }

    private EventHandler<MouseEvent> mouseHandler = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            if (event.getEventType() == MouseEvent.MOUSE_MOVED) {
                if (_toolTips) {
                    toolPreview(event);
                }
            }
            if (event.getEventType()  == MouseEvent.MOUSE_PRESSED) {
                _baseX = event.getSceneX();
                _baseY = event.getSceneY();
                if (_toolTips && event.getClickCount() == 2) {
                   ToolTip t = _photo.add_tools(_rToolX.clone(), _rToolY.clone());
                   addToolToWorldSpace(t);
                }
            } else if (event.getEventType() == MouseEvent.MOUSE_DRAGGED) {
                if (event.isPrimaryButtonDown()) {
                    double dx = (_baseX - event.getSceneX()) ;
                    double dy = (_baseY- event.getSceneY());
                    double powX = (dy / _root3D.getScene().getHeight() * 360) * (Math.PI / 180) * 15;
                    double powY = (dx / _root3D.getScene().getWidth() * -360) * (Math.PI / 180) * 15;
                    _baseX = event.getSceneX();
                    _baseY = event.getSceneY();
                    rotateCamera(powX, powY);
                }
            }
        }
    };


}
