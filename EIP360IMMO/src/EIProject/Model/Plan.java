package EIProject.Model;

import EIProject.Controller.InterfaceMain;
import EIProject.Model.Graphic.AGraphic;
import EIProject.Model.Graphic.Numerisation.Room.ARoom;
import EIProject.Model.Graphic.Numerisation.Room.RoomPolygon;
import EIProject.Model.Graphic.Numerisation.Room.RoomRectangle;
import EIProject.Model.Graphic.VisiteVirtuelle.Photo;
import javafx.collections.ObservableListBase;
import javafx.scene.Node;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Predicate;


/**
 * Created by Lucas on 15/09/2016.
 */

@XmlRootElement(name = "Plan")
@XmlAccessorType(XmlAccessType.FIELD)
public class Plan implements java.io.Serializable {

    @XmlElement(name = "name")
    private String _name;

    @XmlElementWrapper(name = "levels")
    @XmlElement(name = "Level")
    private HashMap<Integer, Level> _m_levels;

    @XmlElement(name = "currentLevel")
    private Level _currLevel;

    @XmlElement(name = "picturesFolder")
    private String _picFolder;

    @XmlElement(name = "firstPhoto")
    private Photo _firstPhoto;

    public Plan() {
        // needed for save/load
    }

    public Plan(String Name, String bg) {
        _m_levels = new HashMap();
        _name = Name;
        Level first = new Level("Level 1", bg);
        _currLevel = first;
        _m_levels.put(0, first);
        _picFolder = "";
        _firstPhoto = null;
    }

    //Levels Functions
    public void add_level(String Name, String bg, Integer index) {
        Level to_add = new Level(Name, bg);
        _m_levels.put(index, to_add);
    }

    public void del_level(int idx) {
        _m_levels.remove(idx);
    }

    public ArrayList<Photo> getAllPlanPhoto() {
        ArrayList<Photo> l_photo = new ArrayList<>();

        for (Level l : _m_levels.values()) {
            l_photo.addAll(Plan.<AGraphic, Photo>shortListByClass(l.get_l_item(), new Photo(), null));
        }
        return l_photo;
    }

    public ArrayList<RoomPolygon> getAllPlanRoomPoly() {
        ArrayList<RoomPolygon> l_RoomPoly = new ArrayList<>();

        for (Level l : _m_levels.values()) {
            l_RoomPoly.addAll(Plan.<AGraphic, RoomPolygon>shortListByClass(l.get_l_item(), new RoomPolygon(), null));
        }
        return l_RoomPoly;
    }

    public ArrayList<RoomRectangle> getAllPlanRoomRect() {
        ArrayList<RoomRectangle> l_RoomRect = new ArrayList<>();

        for (Level l : _m_levels.values()) {
            l_RoomRect.addAll(Plan.<AGraphic, RoomRectangle>shortListByClass(l.get_l_item(), new RoomRectangle(), null));
        }
        return l_RoomRect;
    }

    //static public functions
    static public double getNearest(double a) {
        // get the nearest grid point
        if (a % 25 > 12)
            a = Math.floor(a / 25) * 25 + 25;
        else
            a = Math.floor(a / 25) * 25;
        return (a);
    }

    // Fontion trés utile qui permet de recupéré tout les fils de type C depuis une liste de type pere B.
    static public <B, C extends B> ArrayList<C> shortListByClass(ArrayList<B> _listBase, C shortItem, ArrayList<C> exeption)
    {
        ArrayList<C> _l_res = new ArrayList<>();

        for (B n : _listBase){
            if (n.getClass() == shortItem.getClass() && (exeption == null || !exeption.contains(n)))
                _l_res.add((C) n);
        }
        return _l_res;
    }

    public void moveRoomSelect(double offsetX, double offsetY) {
        for (AGraphic item : InterfaceMain.instance.get_l_select()) {
            if (item.getClass() == RoomPolygon.class || item.getClass() == RoomRectangle.class) {
                ARoom cible = (ARoom) item;
                cible.move(offsetX, offsetY);

                if (ARoom.collide(cible))
                    cible.error();
                else
                    cible.exitError();
            }
        }
    }

    public void collideMoveCheck(double offsetX, double offsetY) {
        boolean error = false;

        for (AGraphic item : InterfaceMain.instance.get_l_select()) {
            if (item.getClass() == RoomPolygon.class || item.getClass() == RoomRectangle.class) {
                ARoom cible = (ARoom) item;

                if (ARoom.collide(cible))
                    error = true;
            }
        }

        if (error) {
            moveRoomSelect(offsetY,offsetX);
        }
    }

    // getters & setters

    public HashMap<Integer, Level> get_m_levels() {
        return _m_levels;
    }

    public Level get_currLevel() { return _currLevel; }

    public void set_currLevel(int index) {
        _currLevel = _m_levels.get(index);
    }

    public String get_picFolder() {
        return _picFolder;
    }

    public void set_picFolder(String _picFolder) {
        this._picFolder = _picFolder;
    }

    public String get_name() { return _name; }

    public Photo get_firstPhoto() {
        return _firstPhoto;
    }

    public void set_firstPhoto(Photo _firstPhoto) {
        this._firstPhoto = _firstPhoto;
    }

    public void set_name(String _name) {
        this._name = _name;
    }
}
