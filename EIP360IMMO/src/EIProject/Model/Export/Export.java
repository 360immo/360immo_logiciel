package EIProject.Model.Export;

import EIProject.Controller.InterfaceLogin;
import EIProject.Controller.InterfaceMain;
import EIProject.Main;
import EIProject.Model.Dao.VisitDao;
import EIProject.Model.Dao.VisitDaoImpl;
import EIProject.Model.Graphic.Numerisation.Room.RoomPolygon;
import EIProject.Model.Graphic.Numerisation.Room.RoomRectangle;
import EIProject.Model.Graphic.VisiteVirtuelle.Link;
import EIProject.Model.Graphic.VisiteVirtuelle.Photo;
import EIProject.Model.Graphic.VisiteVirtuelle.ToolTip;
import EIProject.Model.SQL.User;
import EIProject.Model.SQL.Visit;
import com.google.gson.stream.JsonWriter;
import javafx.geometry.Point2D;
import javafx.scene.shape.Line;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static java.nio.file.Files.copy;

public class Export {

    private String _pathsave;
    private String _name;
    private String _path;
    private Visit _visit = null;
    private User _user = null;
    private List<String> _Uploadpath;
    private JsonWriter _jsonWriter = null;
    private int _error = 0;
    private ArrayList<RoomRectangle> _l_RoomRect = null;
    private ArrayList<RoomPolygon> _l_RoomPoly = null;

    public Export() {
    }

    public int ExportPhotos(ArrayList<Photo> l_photo, boolean isUpdate) throws FileNotFoundException {
        _user = InterfaceLogin.getUser();
        _visit = new Visit();
        _visit.setName(InterfaceMain.instance.get_plan().get_name());
        _visit.setId_Users(_user.get_id());
        _pathsave = String.format("C:\\Users\\%s\\AppData\\Local\\360IMMO", System.getProperty("user.name"));
        _name = String.format("%s.json", _visit.getName());
        _path = String.format("\\%s\\%s", _user.get_companyName(), _visit.getName());
        _Uploadpath = new ArrayList<>();
        _l_RoomRect = InterfaceMain.instance.get_plan().getAllPlanRoomRect();
        _l_RoomPoly = InterfaceMain.instance.get_plan().getAllPlanRoomPoly();
        CheckandCreateDir(_visit.getName());
        CreateJson(l_photo);
        Upload(isUpdate);
        return _error;
    }

    private void CreateJson(ArrayList<Photo> l_photo) {
        try {
            _jsonWriter = new JsonWriter(new FileWriter(_pathsave + "\\" + _name));
            _jsonWriter.beginObject();
            _jsonWriter.name("name").value(_visit.getName());
            _jsonWriter.name("date").value(CurrentDate());
            _jsonWriter.name("auteur").value(System.getProperty("user.name"));
            _jsonWriter.name("nombrePhoto").value(l_photo.size());
            _jsonWriter.name("first").value(InterfaceMain.instance.get_plan().get_firstPhoto().get_pId());
            _jsonWriter.name("Rooms").beginArray();
            for (int y = 0; y < _l_RoomRect.size(); y++)
                jsonRoomRect(_l_RoomRect.get(y));
            for (int z = 0; z < _l_RoomPoly.size(); z++)
                jsonRoomPoly(_l_RoomPoly.get(z));
            _jsonWriter.endArray();
            _jsonWriter.endObject();
            _jsonWriter.close();
        }
        catch (IOException e) {
            _error = 1;
            e.printStackTrace();
        }
    }

    private float compute_orientation(Link link, Photo current) {
//Calcule la position du cercle - dot product entre la Normal N et le vecteur Direction D
        Line line = (Line) link.get_shape();
        Point2D D, N = new Point2D(0,1);

        if (link.get_l_photos()[0] != current) {
            D = new Point2D(line.getEndX() - line.getStartX(), line.getEndY() - line.getStartY());
        }
        else {
            D = new Point2D(line.getStartX() - line.getEndX(), line.getStartY() - line.getEndY());
        }

        float angle = (float) N.angle(D);
        if (D.getX() > 0)
            angle *= -1;

        return angle;
    }

    private void jsonFillRoom(ArrayList<Photo> l_RoomPhoto) throws IOException {
        Photo[] _l_photos;
        int a, b;

        for (int x = 0; x < l_RoomPhoto.size(); x++) {
            String  photoPath = l_RoomPhoto.get(x).get_path();
            _jsonWriter.beginObject();
            _jsonWriter.name("id").value(l_RoomPhoto.get(x).get_pId());
            _jsonWriter.name("name").value(l_RoomPhoto.get(x).get_name());
            _jsonWriter.name("description").value(l_RoomPhoto.get(x).get_description());
            if (photoPath != null)
                _jsonWriter.name("path").value(_path + l_RoomPhoto.get(x).get_path().substring(l_RoomPhoto.get(x).get_path().lastIndexOf("\\")));
            else
                _jsonWriter.name("path").value("null");
            if (photoPath != null)
                _Uploadpath.add(movePhoto(l_RoomPhoto.get(x).get_path(), _pathsave + "\\" + _visit.getName(), l_RoomPhoto.get(x).get_name()));
            _jsonWriter.name("autoRotation").value(l_RoomPhoto.get(x).get_autoRot());
            _jsonWriter.name("direction").value(l_RoomPhoto.get(x).get_photoRotation());
            _jsonWriter.name("Links").beginArray();
            for (int z = 0; z < l_RoomPhoto.get(x).get_l_link().size(); z++) {
                _jsonWriter.beginObject();
                _l_photos = l_RoomPhoto.get(x).get_l_link().get(z).get_l_photos();
                if (_l_photos[0].get_pId() == l_RoomPhoto.get(x).get_pId()) {
                    a = 0; b = 1; }
                else {
                    a = 1; b = 0; }
                _jsonWriter.name("x").value(_l_photos[a].get_x() - _l_photos[b].get_x());
                _jsonWriter.name("y").value(_l_photos[a].get_y() - _l_photos[b].get_y());
                _jsonWriter.name("direction").value(String.valueOf(l_RoomPhoto.get(x).get_l_link().get(z).get_direction()));
                _jsonWriter.name("orientation").value(String.valueOf(compute_orientation(l_RoomPhoto.get(x).get_l_link().get(z), l_RoomPhoto.get(x))));
                _jsonWriter.name("idNextRoom").value(_l_photos[b].get_room().get_id());
                _jsonWriter.name("idNextPhoto").value(_l_photos[b].get_pId());
                if (_l_photos[b].get_path() != null)
                    _jsonWriter.name("pathNextPhoto").value(_path + _l_photos[b].get_path().substring(_l_photos[b].get_path().lastIndexOf("\\")));
                else
                    _jsonWriter.name("pathNextPhoto").value("null");
                _jsonWriter.name("name").value(_l_photos[b].get_name());
                _jsonWriter.endObject();
            }
            _jsonWriter.endArray();
            _jsonWriter.name("Tools").beginArray();
            for (ToolTip t: l_RoomPhoto.get(x).get_l_tools()) {
                _jsonWriter.beginObject();
                _jsonWriter.name("name").value(t.get_name());
                _jsonWriter.name("description").value(t.get_description());
                _jsonWriter.name("rx").value(t.get_rX().getAngle());
                _jsonWriter.name("ry").value(t.get_rY().getAngle());
                _jsonWriter.endObject();
            }
            _jsonWriter.endArray();
            _jsonWriter.endObject();
        }
    }

    private void jsonRoomRect(RoomRectangle roomRectangle) throws IOException {
        _jsonWriter.beginObject();
        _jsonWriter.name("name").value(roomRectangle.get_name());
        _jsonWriter.name("description").value(roomRectangle.get_description());
        _jsonWriter.name("surface").value(roomRectangle.get_surface());
        _jsonWriter.name("color").value(roomRectangle.get_color().toString());
        _jsonWriter.name("nombrePhoto").value(roomRectangle.get_l_photo().size());
        _jsonWriter.name("Photos").beginArray();
        jsonFillRoom(roomRectangle.get_l_photo());
        _jsonWriter.endArray();
        _jsonWriter.endObject();
    }

    private void jsonRoomPoly(RoomPolygon roomPolygon) throws IOException {
        _jsonWriter.beginObject();
        _jsonWriter.name("name").value(roomPolygon.get_name());
        _jsonWriter.name("description").value(roomPolygon.get_description());
        _jsonWriter.name("surface").value(roomPolygon.get_surface());
        _jsonWriter.name("color").value(roomPolygon.get_color().toString());
        _jsonWriter.name("nombrePhoto").value(roomPolygon.get_l_photo().size());
        _jsonWriter.name("Photos").beginArray();
        jsonFillRoom(roomPolygon.get_l_photo());
        _jsonWriter.endArray();
        _jsonWriter.endObject();
    }

    private String movePhoto(String path, String s, String _name) throws IOException {
        File file = new File(path);

        Path source = Paths.get(file.getPath());
        Path destination = Paths.get(s + path.substring(path.lastIndexOf("\\")));
        copy(source, destination, StandardCopyOption.REPLACE_EXISTING);
        return (destination.toString());

    }

    private void CheckandCreateDir(String name) {
        File dir = new File(_pathsave);
        File file = new File(_pathsave + "\\" + name);

        try {
            if (dir.mkdir()) {
            }
            if (file.mkdir()) {
            }
        }
        catch(SecurityException e){
                _error = 1;
                e.printStackTrace();
        }
    }

    private String CurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }


    private void Upload(boolean isUpdate) {
        try {
            byte [] bytesArray = Files.readAllBytes(Paths.get(_pathsave + "\\" + _name));
            _visit.setFile(new String(bytesArray, StandardCharsets.UTF_8));
            VisitDao dao = new VisitDaoImpl();
            Visit existingVisit = dao.read(_visit.getId_Users(), _visit.getName());
            if (!isUpdate && existingVisit != null) {
                _error = 2;
                return;
            }
            if (isUpdate)
                dao.update(_visit, existingVisit);
            else {
                dao.create(_visit);
            }
            FTPUpload();
        } catch (IOException e) {
            _error = 1;
            e.printStackTrace();
        }
    }

    private void FTPUpload() {
        FTPClient ftpClient = new FTPClient();

        try {
            ftpClient.connect(Main.FTP_ADDR, Main.FTP_PORT);
            ftpClient.login(Main.FTP_LOGIN, Main.FTP_PASS);
            ftpClient.changeWorkingDirectory("visit");
            ftpClient.mkd(_user.get_companyName());
            ftpClient.sendSiteCommand("chmod 755 " + _user.get_companyName());
            ftpClient.changeWorkingDirectory(_user.get_companyName());
            ftpClient.mkd(_name.substring(0, _name.lastIndexOf('.')));
            ftpClient.sendSiteCommand("chmod 755 " + _name.substring(0, _name.lastIndexOf('.')));
            ftpClient.changeWorkingDirectory(_name.substring(0, _name.lastIndexOf('.')));
            ftpClient.enterLocalPassiveMode();
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
            for (int u = 0; u < _Uploadpath.size(); u++){
                File file = new File(_Uploadpath.get(u));
                String name = _Uploadpath.get(u).substring(_Uploadpath.get(u).lastIndexOf('\\') + 1, _Uploadpath.get(u).length());
                InputStream inputStream = new FileInputStream(file);
                ftpClient.storeFile(name, inputStream);
                inputStream.close();
                ftpClient.sendSiteCommand("chmod 755 " + name);
            }
        } catch (IOException e) {
            _error = 1;
            e.printStackTrace();
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException e) {
                _error = 1;
                e.printStackTrace();
            }
        }

    }
}