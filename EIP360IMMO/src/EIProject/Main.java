package EIProject;

import EIProject.Controller.InterfaceError;
import EIProject.Model.Settings;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;

public class Main extends Application {

    private static Stage primaryStage;
    private static BorderPane Interface;
    private static BorderPane InterfaceMenu;
    private static Scene MainScene;

    private static Stage secondStage;
    private static Scene secondScene;
    private static Stage errorStage;

    private static Settings softwareSettings;

    public static final String SQL_LOGIN = "UserSoftware";
    public static final String SQL_PASS = "B9tv9Q9cBxMjrUGc";
    public static final String FTP_LOGIN = "ftpuser";
    public static final String FTP_PASS = "727FsMCRWjZzNy5P";
    public static final String SQL_ADDR = "163.172.63.192";
    public static final String SQL_PORT = "3360";
    public static final String SQL_DB = "360IMMO";
    public static final String FTP_ADDR = "163.172.63.192";
    public static final int FTP_PORT = 21;

    @Override
    public void start(Stage primaryStage) {
        loadSettings();
        Main.primaryStage = primaryStage;
        Main.primaryStage.setTitle("360IMMO");
        Main.primaryStage.getIcons().add(new Image("file:src/360immoicone.png"));
        primaryStage.setOnCloseRequest(e -> Platform.exit());
        initInterface();
        //showInterfacePanel();
        showInterfaceAsset();
        showInterfaceLevel();
        showInterfaceInspector();

        String cssPath = getClass().getResource("/EIProject/view/Styles/DarkTheme.css").toExternalForm();

        MainScene.getStylesheets().add(cssPath);
        primaryStage.setMaximized(true);
        showInterfaceLogin();
    }

    // load current settings
    private static void loadSettings() {
        File settingsFile = new File(System.getProperty("user.home") + System.getProperty("file.separator") + "settings.360CONFIG");
        try {
            JAXBContext context = JAXBContext.newInstance(Settings.class);
            Unmarshaller um = context.createUnmarshaller();
            softwareSettings = (Settings) um.unmarshal(new FileReader(settingsFile.getAbsolutePath()));
            checkRecentlyOpened();
        } catch (javax.xml.bind.JAXBException | java.io.FileNotFoundException except) {
            createNewSettingsFile();
        }
    }

    // create a new settings file (on error or if non exists)
    private static void createNewSettingsFile() {
        softwareSettings = new Settings();
        softwareSettings.set_language("en");
        softwareSettings.set_helper(true);
        softwareSettings.set_l_recentlyOpened(new ArrayList<>());
        saveSettings();
    }

    // save settings in a file
    private static void saveSettings() {
        try {
            JAXBContext context = JAXBContext.newInstance(Settings.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            String path = System.getProperty("user.home") + System.getProperty("file.separator") + "settings.360CONFIG";
            m.marshal(softwareSettings, new File(System.getProperty("user.home") + System.getProperty("file.separator") + "settings.360CONFIG"));
        } catch (javax.xml.bind.JAXBException except) {
            // likely not allowed to write
            except.printStackTrace();
        }
    }

    public static ArrayList<String> getRecentFiles() {
        return (softwareSettings.get_l_recentlyOpened());
    }

    // check if all the files in the list exist on disc
    private static void checkRecentlyOpened() {
/*        for (String check : softwareSettings.get_l_recentlyOpened()) {
            File f = new File(check);
            if(!f.exists() || f.isDirectory()) {
                softwareSettings.get_l_recentlyOpened().remove(check);
            }
        }*/
    }

    public static void setLanguage(String new_value) {
        softwareSettings.set_language(new_value);
        saveSettings();
    }

    public static String getLanguage() {
        return (softwareSettings.get_language());
    }

    public static void setHelper(boolean new_value) {
        softwareSettings.set_helper(new_value);
        saveSettings();
    }

    public static boolean getHelper() {
        return (softwareSettings.get_helper());
    }

    // add a file to the recently opened list
    public static void addRecentlyOpened(String path) {
        int check_already = softwareSettings.get_l_recentlyOpened().indexOf(path);

        if (check_already != 0) {
            if (check_already > 0) {
                softwareSettings.get_l_recentlyOpened().remove(path);
            }
            softwareSettings.get_l_recentlyOpened().add(0, path);
            if (softwareSettings.get_l_recentlyOpened().size() > 5) {
                while (softwareSettings.get_l_recentlyOpened().size() > 5) {
                    softwareSettings.get_l_recentlyOpened().remove(5);
                }
            }
            saveSettings();
        }
    }

    public static void initInterface() {
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("UIResources", new Locale(softwareSettings.get_language()));
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("view/Interface.fxml"), bundle);
            AnchorPane InterfacePaneMain = loader.load();
            MainScene = new Scene(InterfacePaneMain);
            Interface = (BorderPane) InterfacePaneMain.getChildren().get(0);
            primaryStage.setScene(MainScene);
            primaryStage.sizeToScene();
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void showInterfacePanel() {
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("UIResources", new Locale(softwareSettings.get_language()));
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("view/InterfacePlan.fxml"), bundle);
            //loader.setLocation);
            ScrollPane InterfacePan = loader.load();
            Interface.setCenter(InterfacePan);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void showInterfaceLevel() {
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("UIResources", new Locale(softwareSettings.get_language()));
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("view/InterfaceLevels.fxml"), bundle);
            AnchorPane InterfacePan = loader.load();
            Interface.setLeft(InterfacePan);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void showInterfaceAsset() {
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("UIResources", new Locale(softwareSettings.get_language()));
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("view/InterfaceAssets.fxml"), bundle);
            AnchorPane InterfacePan = loader.load();
            Interface.setBottom(InterfacePan);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void showInterfaceInspector() {
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("UIResources", new Locale(softwareSettings.get_language()));
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("view/InterfaceInspector.fxml"), bundle);
            AnchorPane InterfacePan = loader.load();
            Interface.setRight(InterfacePan);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void showInterfaceMenu() {
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("UIResources", new Locale(softwareSettings.get_language()));
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("view/InterfaceMenu.fxml"), bundle);
            InterfaceMenu = loader.load();
            Scene sceneMenu = new Scene(InterfaceMenu);

            primaryStage.setScene(sceneMenu);
            primaryStage.sizeToScene();
            primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void showAccountMenu() {
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("UIResources", new Locale(softwareSettings.get_language()));
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("view/AccountPanel.fxml"), bundle);
            AnchorPane InterfacePan = loader.load();
            InterfaceMenu.setCenter(InterfacePan);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void showInterfaceLogin() {
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("UIResources", new Locale(softwareSettings.get_language()));
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("view/InterfaceLogin.fxml"), bundle);
            AnchorPane pane = loader.load();
            secondScene = new Scene(pane);
            secondStage = new Stage();
            secondStage.setTitle("Welcome");
            secondStage.setScene(secondScene);
            secondStage.setAlwaysOnTop(true);
            secondStage.setResizable(false);
            secondStage.initModality(Modality.APPLICATION_MODAL);
            if (!secondStage.isShowing())
                secondStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void showInterfaceError() {
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("UIResources", new Locale(softwareSettings.get_language()));
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("view/InterfaceError.fxml"), bundle);
            AnchorPane pane = loader.load();
            Scene errorScene = new Scene(pane);
            errorStage = new Stage();
            errorStage.setTitle("Error :(");
            errorStage.setScene(errorScene);
            errorStage.setAlwaysOnTop(true);
            errorStage.setResizable(false);
            errorStage.initStyle(StageStyle.UNDECORATED);
            if (!errorStage.isShowing())
                errorStage.show();
            errorStage.focusedProperty().addListener(new ChangeListener<Boolean>()
            {
                @Override
                public void changed(ObservableValue<? extends Boolean> ov, Boolean t, Boolean t1)
                {
                    errorStage.close();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void showInterfaceExport() {
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("UIResources", new Locale(softwareSettings.get_language()));
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("view/InterfaceExport.fxml"), bundle);
            AnchorPane pane = loader.load();
            secondScene = new Scene(pane);
            secondStage = new Stage();
            secondStage.setTitle("Export !");
            secondStage.setScene(secondScene);
            secondStage.setAlwaysOnTop(true);
            secondStage.setResizable(false);
            secondStage.initModality(Modality.APPLICATION_MODAL);
            if (!secondStage.isShowing())
                secondStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void showInterfaceManageVisit() {
        try {
            ResourceBundle bundle = ResourceBundle.getBundle("UIResources", new Locale(softwareSettings.get_language()));
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("view/InterfaceManageVisit.fxml"), bundle);
            AnchorPane pane = loader.load();
            secondScene = new Scene(pane);
            secondStage = new Stage();
            secondStage.setTitle("Manage Visit !");
            secondStage.setScene(secondScene);
            secondStage.setAlwaysOnTop(true);
            secondStage.setResizable(false);
            secondStage.initModality(Modality.APPLICATION_MODAL);
            if (!secondStage.isShowing())
                secondStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void closeInterfaceExport() {
        secondStage.close();
    }

    public static void myClosed(){
        primaryStage.close();
    }

   /* public BorderPane getInterfaceMenu() { return InterfaceMenu;}

    public Stage getPrimaryStage() {
        return primaryStage;
    }*/

    public static void main(String[] args) {
        launch(args);
    }

    public static Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void licenseExpired() {
        Interface.setDisable(true);
    }
}