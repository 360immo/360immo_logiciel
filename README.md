# README #

### Presentation du projet ###

Dans le but de mettre en valeur un bien immobilier, nous souhaitons proposer une solution logicielle intuitive et ergonomique.
Nous proposerons cette solution aux agences immobilières indépendantes, qui seront donc notre cœur de cible (les grands groupes possédant déjà des logiciels développés par leurs employés).
Cette solution permettra de créer et éditer des visites virtuelles composées de photos à 360°. Nous nous imposons comme objectif d’avoir le moins possible d’interaction entre l’agent immobilier et le logiciel, de viser une automatisation maximale ; cependant, si le logiciel commet des erreurs d’analyse, la visite virtuelle devra pouvoir être aisément modifiée par l’agent immobilier.

### Description du repository ###

Ce repository est la partie logiciel de la solution 360IMMO. Ce logiciel permet la création d'une visite virtuelle à partir d'un plan 2D et d'images 360° préalablement prise par l'utilisateur

Le projet est développé en java 8 (jdk1.8.0_121)